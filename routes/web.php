<?php
Route::get("test", function () {
    return view("text");
});



Route::get('/', "IndexController@index");

//需要登入後才行的
Route::group(['prefix' => 'user','middleware'=>'guest'], function () {
    Route::get('news', "UserNewsController@index");
    Route::get('news/{id}', 'UserNewsController@view');
    Route::get('news/type/{id}', 'UserNewsController@index');

    Route::get('edit', "MemberController@EditPage");
    Route::post('edit', "MemberController@EditMember");
    Route::get('password', "MemberController@EditPasswordPage");
    Route::post('password', "MemberController@EditPassword");
    Route::get('event/list', "MemberController@EventPage");
    Route::get('dog/{id}', 'MemberController@DogPage');
});

//驗證碼
Route::get('captcha', "CaptchaController@create");

Route::get('/about/master', "AboutController@master");
Route::get('/about/dogs', "AboutController@dogs");
Route::get('/about/dogs/{id}', "AboutController@dogsView");
Route::get('/about/constitution', "AboutController@constitutions");
Route::get('/about/structure', "AboutController@structure");
Route::get('/about/commit', "AboutController@commit");
Route::get('/about/research', "AboutController@research");
Route::get('/news/report', "AboutController@report");
Route::get('/service', "AboutController@service");

//登入
Route::get('login', "MemberController@LoginPage");
Route::post('login', "MemberController@Login");
Route::get('logout', "MemberController@Logout");


// 20170604 孝先提 轉到後台去處理
//註冊
//Route::get('user/sign_up',"MemberController@SignUpPage");
//Route::post('user/sign_up',"MemberController@SignUp");
//Route::get('user/sign_up/welcome',"MemberController@welcome");
Route::get('user/reset/password', "MemberController@ResetPasswordPage");
Route::post('user/reset/password', "MemberController@ResetPassword");



Route::get('how', 'IndexController@how');
Route::get('how/download', "IndexController@howDownload");

//最新消息
Route::get('news', 'NewsController@index');
Route::get('news/{id}', 'NewsController@view');
Route::get('news/type/{id}', 'NewsController@index');

Route::get('donate', "DonateController@index");
Route::get('donate/{type}', "DonateController@index");


Route::get('album', "AlbumController@index");
Route::get('album/{id}', "AlbumController@view");



Route::get('events', "EventsController@index");
Route::get('events/calendar', "EventsController@calendar");
Route::get('events/{id}', "EventsController@view");
Route::get('events/type/{id}', 'EventsController@index');


Route::get('file/{yvtset}/{column}/{id}', "DownloadFileController@download");

//Route::get('admin/news','Admin\NewsController@index');



Route::get("admin/login", 'Admin\LoginController@show');
Route::post("admin/login", 'Admin\LoginController@login');
Route::post("admin/logout", 'Admin\LoginController@logout');



Route::group(['prefix' => 'admin','middleware'=>'admin.check'], function () {
    Route::resource("dogAlbum", 'Admin\DogAlbumController');


//    Route::resource("user",'Admin\MemberController');
//    Route::post("user/{id}/check",'Admin\MemberController@checkPay');
//    Route::post("/user/search",'UserController@search');


//    Route::resource("master/{user_id}/dogs/{id}",'Admin\DogController');

    //event
    Route::resource("event", 'Admin\EventController');
    Route::delete("master/{user_id}/event_list/{id}", 'Admin\EventController@deleteList');
    Route::delete("master/{dog_id}/event_dog_list/{id}", 'Admin\EventController@deleteDogList');
    Route::get("event/{id}/list", 'Admin\EventController@listPage');

    Route::resource("constitution", 'Admin\ConstitutionController');

    Route::resource("structure", 'Admin\StructureController');
    Route::resource("commit", 'Admin\CommitController');


    Route::resource("master", 'Admin\MasterController');
    Route::post("master/{id}/check", 'Admin\MasterController@checkPay');
    Route::get("master/search", 'Admin\MasterController@search');

    //add event_list for master api
    Route::post("eventList/save", 'Admin\EventController@saveEventList');
    Route::post("eventDogList/save", 'Admin\EventController@saveEventDogList');

    //編輯修改狗
    Route::get("master/{id}/dogs/create", 'Admin\DogController@create');
    Route::post("master/{id}/dogs/create", 'Admin\DogController@store');
    Route::get('master/{user_id}/dogs/{id}', 'Admin\DogController@show');
    Route::put('master/{user_id}/dogs/{id}', 'Admin\DogController@update');
    Route::delete('master/{user_id}/dogs/{id}', 'Admin\DogController@destroy');

    Route::post('yvtset/{yvtset?}/{id}/copy', 'Admin\YvtController@copy');

    Route::get('yvtset/{yvtset?}', 'Admin\YvtController@index');
    Route::get('yvtset/{yvtset?}/create', 'Admin\YvtController@create');
    Route::post('yvtset/{yvtset?}', 'Admin\YvtController@store');

    Route::get('yvtset/{yvtset?}/{id}', 'Admin\YvtController@show');
    Route::put('yvtset/{yvtset?}/{id}', 'Admin\YvtController@update');
    Route::delete('yvtset/{yvtset?}/{id}', 'Admin\YvtController@delete');
});

//Route::resource('admin/yvtset/{yvtset?}','Admin\YvtController');

//Route::get("api/{yvtset}",'Api\YvtApiController@yvt_list');
Route::get("api/{yvtset}/{id}", 'Api\YvtApiController@yvt_detail');



//關聯orders
Route::get("ajax/yvtset/{yvtset}", 'Admin\YvtController@getColumn');
Route::post('ajax/yvtset/{yvtset}', 'Admin\YvtController@relStore');
Route::post("ajax/yvtset/{yvtset}/order", 'Admin\YvtController@updateOrder');
Route::get("ajax/yvtset/{yvtset}/{id}", 'Admin\YvtController@getIdColumn');
