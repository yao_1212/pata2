<?php

return [
    'case_sensitive' => true,
    'width' => 135,
    'height' => 50,
    'length' => 4,
    'quality' => 100,
    'case' => 'mixed',
    'letters' => false,
    'numbers' => true,
    'angle' => 10,
    'separation' => 30,
    'background' => [250, 250, 250],

    'h_lines' => 5,
    'v_lines' => 20,
    'line_color' => [220, 220, 220],

    //Colors
    'colors' => [
        [167, 149, 122],
        [117, 101, 78],
        [119, 101, 75]
    ],

    //Font
    'font' => 'Prototype',
    'size' => 30,
    'fonts_path' => 'fonts',
];