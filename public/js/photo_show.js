$(function(){


    var $overlay = $('<div id="overlay"></div>');
    var $image = $("<img>");

    $overlay.append($image);

    $("body").append($overlay);

    $('.photo-gallery').on('click', function(event){
        event.preventDefault();
        var imageLocation = $(this).find('.img-wrap').attr("src");
        $image.attr("src", imageLocation);

        $overlay.show();
    });


    $("#overlay").click(function() {
        $( "#overlay" ).hide();
    });
})