$(document).ready(function() {
    // Hides all paragraphs
    // $("p").hide();
    // Optional for showing the first paragraph. For animation use .slideDown(200) instead of .show()
    $("p").show();

    $("h2").click(function()
    {
        // Toggles the paragraph under the header that is clicked. .slideToggle(200) can be changed to .slideDown(200) to make sure one paragraph is shown at all times.
        $(this).next("p").slideToggle(200);
        // Makes other pararaphes that is not under the current clicked heading dissapear
        // $(this).siblings().next("p").slideUp(200);
    });
});
