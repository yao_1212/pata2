<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('event', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('title');
//            $table->string('type');
//            $table->string('startdate');//活動日期
//            $table->string('comedate');//報到
//            $table->string('outdate');//截止日期
//            $table->string('master');//主辦
//            $table->string('addr');
//            $table->string('admin');
//            $table->string('phone');
//            $table->string('score');
//            $table->text('content');
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
