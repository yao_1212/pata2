<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dogs', function (Blueprint $table) {
            $table->increments('dog_id');
            $table->integer('user_id');
            $table->string("name")->comment('名字');
            $table->string("type")->comment('品種')->nullable();
            $table->string("sex")->comment('性別')->nullable();
            $table->timestamp("birth")->comment('生日')->nullable();
            $table->string("number")->comment('晶片號')->nullable();
            $table->string("first_ok")->comment('首次合格日期')->nullable();
            $table->timestamp("start_date")->comment('效期開始')->nullable();
            $table->timestamp("end_date")->comment('效期結束')->nullable();
            $table->string("img")->comment('照片')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dogs');
    }
}
