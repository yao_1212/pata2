<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDogAlbumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dog_albums', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name")->comment('名字');
            $table->string("type")->comment('品種')->nullable();
            $table->string("sex")->comment('性別')->nullable();
            $table->timestamp("birth")->comment('生日')->nullable();
            $table->string("personality")->comment('個性')->nullable();
            $table->string("like")->comment('喜歡事物')->nullable();
            $table->string("treat")->comment('對待')->nullable();
            $table->string("img")->comment('照片')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dog_albums');
    }
}
