<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('constitutions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('total')->comment('總則');
            $table->text('member')->comment('會員');
            $table->text('power')->comment('組織及職權');
            $table->text('meeting')->comment('會議');
            $table->text('money')->comment('經費');
            $table->text('sub')->cooment('附則');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constitutions');
    }
}
