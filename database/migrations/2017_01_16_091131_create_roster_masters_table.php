<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRosterMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('roster_master', function (Blueprint $table) {
//            $table->increments('m_id');
//            $table->integer('ros_id');
//            $table->string("name")->nullable();
//            $table->string("service")->nullable();
//            $table->string("task")->nullable();
//            $table->string("img")->nullable();
//            $table->integer("m_order");
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roster_masters');
    }
}
