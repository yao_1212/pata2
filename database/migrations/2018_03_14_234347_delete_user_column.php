<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteUserColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // ALTER TABLE `users`
        // DROP `nickname`,
        // DROP `type`;
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('type'); //要放到活動
            $table->dropColumn('nickname'); // 要放到治療犬上
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
