<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('password', 60);
            $table->string('tw_name');
            $table->string('en_name');
            $table->integer('type');
            $table->dateTime('payTime');
            $table->integer('gender');
            $table->date('birth');
            $table->string('twID');
            $table->integer('degree')->nullable();
            $table->string('school')->nullable();
            $table->string('office')->nullable();
            $table->string('ophone')->nullable();
//            $table->string('zipcode1');//永久區號
//            $table->string('zipcode2');//通訊區號
            $table->string('addr1')->nullable();//永久地址
//            $table->string('addr2')->nullable();//通訊地址
            $table->string('phone1')->nullable();
            $table->string('mphone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
