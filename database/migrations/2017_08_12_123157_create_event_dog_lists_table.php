<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventDogListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_dog_lists',function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->integer('events_id')->unsigned();
            $table->integer('dog_id')->unsigned();
            $table->boolean("isCheck")->default(0);
            $table->boolean('isPay');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_dog_lists');
    }
}
