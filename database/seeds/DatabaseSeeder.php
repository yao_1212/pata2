<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i=0;$i<10;$i++){
            DB::table("event")->insert([
                'title'=> $faker->realText($maxNbChars = 10, $indexSize = 2),
                "type" => random_int(1,3),
                'startdate'=> $faker->date("Y-m-d"),
                'comedate'=> $faker->date("Y-m-d"),
                'outdate'=> $faker->date("Y-m-d"),
                'master'=> "台灣動物輔助治療專業發展協會",
                'addr'=> "台灣動物輔助治療專業發展協會",
                'admin'=> "姚",
                'phone'=> "091234567",
                'score'=> 3,
                'content'=> $faker->realText($maxNbChars = 10, $indexSize = 5),
                "created_at"=>$faker->date("Y-m-d")
            ]);
        }



        ///最新消息////
//        $news_arr = array("1"=>"最新消息",
//            "2"=>"學術研討會",
//            "3"=>"好康報報",
//            "4"=>"徵才訊息");
//
//        for($i=0;$i<10;$i++){
//            DB::table("news")->insert([
//                "title"=> $faker->realText($maxNbChars = 10, $indexSize = 2),
//                "type" => $news_arr[random_int(1,4)],
//                "content"=>$faker->realText($maxNbChars = 50, $indexSize = 2),
//                "created_at"=>$faker->date("Y-m-d")
//            ]);
//        }



        //////捐款芳名錄//////
//        foreach (range(1,10) as $index){
//            $uuid = $faker->date("Y")-1911;
//            $uuid = $uuid.date("md")."h".randomString(4);
//
//            DB::table("donate")->insert([
//                'id' => $uuid,
//                'name' => $faker->name,
//                'donate_time'=> $faker->date("Y-m-d"),
//                'money' => random_int(200,1000),
//                'created_at' => date("Y-m-d")
//            ]);
//        }
    }
}
