<?php

use Illuminate\Database\Seeder;
use App\Reports;

class ReportSeeder extends Seeder
{
    /**
     * Run the datebase seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
			['date'=>'2018-02-02','title'=>'花蓮第一批治療犬誕生！', 'link'=>'https://www.facebook.com/1375489219431949/photos/a.1376650695982468.1073741828.1375489219431949/1908233816157484/?type=3&theater'],
			['date'=>'2018-03-08','title'=>'挺過犬瘟熱當治療犬　陪長者復健讓自閉兒開口', 'link'=>'https://tw.appledaily.com/new/realtime/20180308/1310833/'],
			['date'=>'2018-03-08','title'=>'狗醫師大使 伴埔里長者', 'link'=>'https://udn.com/news/story/11322/3020733'],
			['date'=>'2018-03-08','title'=>'動物輔療 狗醫師加入埔基伴老行列', 'link'=>'https://health.udn.com/health/story/6631/3020332'],
			['date'=>'2018-03-09','title'=>'狗兒熬過犬瘟熱 當「治療師」逗長輩', 'link'=>'https://tw.news.appledaily.com/headline/daily/20180309/37952390'],
			['date'=>'2018-03-09','title'=>'挺過犬瘟熱當治療犬　陪長者復健讓自閉兒開口', 'link'=>'http://sharpdaily.tw/mobiles/article/389136'],
			['date'=>'2018-03-09','title'=>'埔基長照引進動物輔療 社區推陪伴犬訓練計畫', 'link'=>'https://pulilife.com/2018/03/08/%E5%9F%94%E5%9F%BA%E9%95%B7%E7%85%A7%E5%BC%95%E9%80%B2%E5%8B%95%E7%89%A9%E8%BC%94%E7%99%82-%E7%A4%BE%E5%8D%80%E6%8E%A8%E9%99%AA%E4%BC%B4%E7%8A%AC%E8%A8%93%E7%B7%B4%E8%A8%88%E7%95%AB/'],
			['date'=>'2018-03-09','title'=>'狗醫師伴老 長者笑呵呵', 'link'=>'http://merit-times.net/2018/03/09/%E7%8B%97%E9%86%AB%E5%B8%AB%E4%BC%B4%E8%80%81-%E9%95%B7%E8%80%85%E7%AC%91%E5%91%B5%E5%91%B5/'],
			['date'=>'2018-03-10','title'=>'埔基長照中心 治療犬陪復健', 'link'=>'http://www.chinatimes.com/newspapers/20180310000599-260107'],
			['date'=>'2018-03-10','title'=>'埔基長照中心 治療犬陪復健', 'link'=>'https://tw.news.yahoo.com/%E5%9F%94%E5%9F%BA%E9%95%B7%E7%85%A7%E4%B8%AD%E5%BF%83-%E6%B2%BB%E7%99%82%E7%8A%AC%E9%99%AA%E5%BE%A9%E5%81%A5-215006132.html'],
			['date'=>'2018-03-10','title'=>'「魯比」挺過犬瘟熱 當陪伴犬逗樂銀髮族', 'link'=>'https://news.ftv.com.tw/news/detail/2018311C03M1'],
			['date'=>'2018-03-12','title'=>'暨南大學與長照服務單位聯手 動物加入伴老行列', 'link'=>'http://www.cna.com.tw/postwrite/Detail/229993.aspx#.Wu___KSFOUm'],
			['date'=>'2018-03-14','title'=>'暨大聯手長照單位 推動物伴老', 'link'=>'http://www.tanews.org.tw/info/14345'],
			['date'=>'2018-04-05','title'=>'【埔基動物輔療伴老計畫】狗醫師培訓　長照服務新利器', 'link'=>'https://www.ct.org.tw/1321444'],
			['date'=>'2018-04-10','title'=>'【汪汪小百科】治療犬是什麼?我家狗狗可以成為治療犬嗎?', 'link'=>'http://maoup.com.tw/?p=14973'],
			['date'=>'2018-05-01','title'=>'【「台灣動物輔助治療之母」－葉明理博士 專訪】', 'link'=>'http://www.needsradio.org.tw/joomla2/index.php?option=com_content&view=article&id=3631%3A2017-09-18&catid=241%3A11000000&Itemid=96'],
			['date'=>'2018-05-01','title'=>'《動物輔療》溫馨互動 失智路上有牠陪伴', 'link'=>'https://tw.news.yahoo.com/%E5%8B%95%E7%89%A9%E8%BC%94%E7%99%82-%E6%BA%AB%E9%A6%A8%E4%BA%92%E5%8B%95-%E5%A4%B1%E6%99%BA%E8%B7%AF%E4%B8%8A%E6%9C%89%E7%89%A0%E9%99%AA%E4%BC%B4-073800751.html'],
			['date'=>'2018-06-01','title'=>'狗狗也能當醫生 治療犬與動物輔助治療', 'link'=>'http://www.tanews.org.tw/info/14876'],
			['date'=>'2018-07-05','title'=>'全台46隊治療犬鑑定考 花蓮2隻通過進駐', 'link'=>'https://www.youtube.com/watch?feature=share&v=OMZxv19E8Os&app=desktop'],
			['date'=>'2018-07-06','title'=>'治療犬首度進駐花蓮！考試項目有這些', 'link'=>'https://tw.news.yahoo.com/%E6%B2%BB%E7%99%82%E7%8A%AC%E9%A6%96%E5%BA%A6%E9%80%B2%E9%A7%90%E8%8A%B1%E8%93%AE-%E8%80%83%E8%A9%A6%E9%A0%85%E7%9B%AE%E6%9C%89%E9%80%99%E4%BA%9B-005012588.html'],
			['date'=>'2018-08-06','title'=>'【Summer FUN Camp 治療犬訓練師】', 'link'=>'https://www.facebook.com/101kidzclub/videos/1899413533448564/'],
			['date'=>'2018-08-22','title'=>'療癒萌萌達的狗醫生❤❤', 'link'=>'https://www.facebook.com/%E6%96%B0%E5%8C%97%E5%B8%82%E8%B2%A2%E5%AF%AE%E7%A6%8F%E9%9A%86%E5%85%AC%E5%85%B1%E6%89%98%E8%80%81%E4%B8%AD%E5%BF%83-%E6%97%A5%E9%96%93%E6%89%98%E8%80%81%E6%9A%A8%E9%8A%80%E9%AB%AE%E4%BF%B1%E6%A8%82%E9%83%A8-618258668372003/?__tn__=K-R&eid=ARDYV48OIEOqAfGwMH86sNbQAT-Jm3-nQpRxxpj5VZSZAg-Hi-JvM7sePiP0mG2IpM9MIwWaBfTwA9JS&fref=mentions'],
			['date'=>'2018-09-02','title'=>'【全職媽媽遛小孩-366】動物輔助治療犬的認識與體驗', 'link'=>'http://plumtywewe.pixnet.net/blog/post/67162842-%E3%80%90%E5%85%A8%E8%81%B7%E5%AA%BD%E5%AA%BD%E9%81%9B%E5%B0%8F%E5%AD%A9-366%E3%80%91%E5%8B%95%E7%89%A9%E8%BC%94%E5%8A%A9%E6%B2%BB%E7%99%82%E7%8A%AC%E7%9A%84%E8%AA%8D%E8%AD%98%E8%88%87%E9%AB%94%E9%A9%97?fbclid=IwAR3xnW0vnpXzdRodr94yFOvVSH9_85KDEOzmuh5b0bqq2GJKySbKL1VQXes'],
			['date'=>'2018-10-08','title'=>'「2017變臉台灣 第5集」用心陪伴，與病友一起走過', 'link'=>'https://youtu.be/hdg98EenpqU'],
			['date'=>'2018-10-11','title'=>'讓動保融入學科 老師也能輕鬆上動保', 'link'=>'https://shar.es/a1wTRG'],
			['date'=>'2018-11-09','title'=>'當生物課遇上治療犬', 'link'=>'http://ndsunnyfield.pixnet.net/blog/post/349865684-%E7%95%B6%E7%94%9F%E7%89%A9%E8%AA%B2%E9%81%87%E4%B8%8A%E6%B2%BB%E7%99%82%E7%8A%AC'],
			['date'=>'2018-11-10','title'=>'終結孤獨「心處方」！動物輔助治療+幸福養心運動', 'link'=>'https://tw.news.yahoo.com/%E7%B5%82%E7%B5%90%E5%AD%A4%E7%8D%A8%E3%80%8C%E5%BF%83%E8%99%95%E6%96%B9%E3%80%8D%EF%BC%81%E5%8B%95%E7%89%A9%E8%BC%94%E5%8A%A9%E6%B2%BB%E7%99%82%E5%B9%B8%E7%A6%8F%E9%A4%8A%E5%BF%83%E9%81%8B%E5%8B%95-145619906.html?fbclid=IwAR0nSdsBin6vm9eEAiSqoQzTA18Msx99KAmH_mAAA83KVpS7tF4gUGkyhMs'],
			['date'=>'2018-11-10','title'=>'國北護舉辦「心理健康BMI幸福養心暨動物輔助治療」推廣活動', 'link'=>'https://www.ydn.com.tw/News/312414?fbclid=IwAR1GyHP93SuoNXypa3tp1_PxzpsJUOQXZW3ujBOcl0fkTr9RBYOI1CtImuI'],
			['date'=>'2018-11-12','title'=>'超萌治療犬輔助　療癒失智長者身心靈', 'link'=>'https://www.top1health.com/Article/253/69072?page=2&fbclid=IwAR2XEVy2xVve-0X6wYGR34GdMEDRY1wDRH1CEML0uVWp1jeg1sVlTsvUkbM'],
			['date'=>'2018-11-12','title'=>'超萌治療犬陪伴失智長者 互動療癒穩定情緒', 'link'=>'https://tw.news.yahoo.com/%E8%B6%85%E8%90%8C%E6%B2%BB%E7%99%82%E7%8A%AC%E9%99%AA%E4%BC%B4%E5%A4%B1%E6%99%BA%E9%95%B7%E8%80%85-%E4%BA%92%E5%8B%95%E7%99%82%E7%99%92%E7%A9%A9%E5%AE%9A%E6%83%85%E7%B7%92-064242783.html?fbclid=IwAR3WsMiMB2ijlRECwUwcG0Y1glBFB2IO2WJi0Iel_87EBFjpF-cChxIDsUA'],
			['date'=>'2018-11-12','title'=>'超萌「治療犬」發功！ 減少寂寞無望感延緩失智', 'link'=>'https://cnews.com.tw/003181112a03/?fbclid=IwAR11fSYik0vkCtYizdshWbvItJ68op5RLqe30em8otgIeK8lqlKheo0IVuQ'],
			['date'=>'2018-11-12','title'=>'超萌「治療犬」發功！ 減少寂寞無望感延緩失智', 'link'=>'https://n.yam.com/Article/20181112702734'],
			['date'=>'2018-11-12','title'=>'超萌治療犬的陪伴，增加失智長者互動', 'link'=>'https://health.gvm.com.tw/webonly_content_19335.html'],
			['date'=>'2018-11-12','title'=>'超萌治療犬輔助　療癒失智長者身心靈', 'link'=>'http://eladies.sina.com.tw/getnews.php?newsid=217502'],
			['date'=>'2018-11-13','title'=>'超萌「治療犬」發功！失智者12週後竟出現「4大改變」', 'link'=>'https://health.ettoday.net/news/1304540'],
			['date'=>'2018-11-14','title'=>'共照牽手，失智據點活力秀，超萌治療犬的陪伴，增加失智長者互動，延緩失智，生活功能促進', 'link'=>'https://www.peopo.org/news/384443?fbclid=IwAR0D8AIP56Sj7qAvkvc1G9caP0q-u4AKhENRVMB4V2_WYAWghUKQAJ6LEJU'],
			['date'=>'2018-11-14','title'=>'頂番國小生命教育體驗課程回饋貼文', 'link'=>'https://www.facebook.com/profile.php?id=1247709620&lst=1359082572%3A1247709620%3A1542338117&__tn__=-UK-R'],
			['date'=>'2018-12-08','title'=>'標準型貴賓犬 經訓練成療癒狗醫師', 'link'=>'https://www.facebook.com/tvbs56TVIEWPOINT/videos/1990383831040110/'],
			['date'=>'2019-01-03','title'=>'不只是毛「小孩」 動物也能守護人類', 'link'=>'https://vita.tw/%E4%B8%8D%E5%8F%AA%E6%98%AF%E6%AF%9B-%E5%B0%8F%E5%AD%A9-%E5%8B%95%E7%89%A9%E4%B9%9F%E8%83%BD%E5%AE%88%E8%AD%B7%E4%BA%BA%E9%A1%9E-4639f53b3553?fbclid=IwAR0WOCsBUMF0PsbpMboQxhNYplLeyZyBjqjVde-5M3Cf9vLcYkO7Oi-B--4'],
			['date'=>'2019-03-16','title'=>'全新喘息家庭旅遊', 'link'=>'https://www.facebook.com/dreamvok/videos/2263151497287846/'],
			['date'=>'2019-03-27','title'=>'動物輔助治療課', 'link'=>'https://www.facebook.com/sfang.fans/videos/417369069027530/'],
		];
		foreach($data as $report){
			Reports::create($report);
		}
    }
}
