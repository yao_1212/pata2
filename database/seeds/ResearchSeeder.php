<?php

use Illuminate\Database\Seeder;
use App\Researches;

class ResearchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
			['date'=> '2001-01-01','title'=>'社區老人擁有寵物對其健康狀況及生活品質影響之相關性探討The Impacts of Pet Ownership on the Health Status and the Quality of Life of Community Elderly in Taiwan', 'link' => 'https://hdl.handle.net/11296/7wry65'],
			['date'=> '2008-01-01','title'=>'社區中老年人與同伴動物鍵結歷程之初探The exploration of the human-animal bonding processbetween elders and companion animals in the community', 'link' => 'https://hdl.handle.net/11296/22qhf6'],
			['date'=> '2009-01-01','title'=>'"與生命的互動，從「在乎」開始—動物輔助治療與自閉症孩子的交會Life Interaction Based on “Caring "": The Bond between Animal-Assisted Therapy and Children with Autism"', 'link' => 'https://hdl.handle.net/11296/2j54de'],
			['date'=> '2009-01-01','title'=>'中壯年腦傷案主在動物輔助治療中的經驗與改變The Experiences and Changes of the Brain-injured Adults from Animal-Assisted Therapy', 'link' => 'https://hdl.handle.net/11296/n29kqx'],
			['date'=> '2011-01-01','title'=>'動物輔助治療取向教學對國中中重度智能障礙學生社交技巧之影響The Effect of Teaching Social Skills by Animal Assisted Therapy on Junior High School Students with Moderate or Severe Intellectual and Developmental Disabilities', 'link' => 'https://hdl.handle.net/11296/8ss936'],
			['date'=> '2012-01-01','title'=>'牠的一生，我的一段：成年早期男性人貓關係歷程中經驗之探索An Exploratory Study On The Affective Experience Of The Relationship Between Early Adult Male And Pet Cat', 'link' => 'https://hdl.handle.net/11296/79zu27'],
			['date'=> '2012-01-01','title'=>'正向情緒擴建理論取向之動物輔助治療對老人幸福感之影響Effects of The Broaden-and-Build Theory of Positive Emotion Approach with Animal-Assisted Therapy on Well-being of Elderly', 'link' => 'https://hdl.handle.net/11296/8y65ad'],
			['date'=> '2013-01-01','title'=>'"飼主寵物依附關係、情感狀態和 利動物行為之相關研究－以飼養犬、貓為例A Correlational Study of Pet Attachment, Affectivity and Pro-animal Behavior among Owners - A Case of Dogs and Cats"', 'link' => 'https://hdl.handle.net/11296/hasqjb'],
			['date'=> '2013-01-01','title'=>'運用動物失落儀式協助校犬志工因應校犬死亡之研究Saying Goodbye to School Dog：Animal Lost Activity and Its Implications', 'link' => 'https://hdl.handle.net/11296/67x3w9'],
			['date'=> '2014-01-01','title'=>'不同型態「治療犬」方案對機構失智症住民照護之成效探討The Effect of Different Therapeutic Dog Programs on Institutionalized Residents with Dementia', 'link' => 'https://hdl.handle.net/11296/32bwbp'],
			['date'=> '2014-01-01','title'=>'國小低年級動物輔助課程發展及其對學童生活能力之影響研究The Study on Developing An Animal-Assisted Education Course for 7-8 Years Old Children and Evaluating Its Effectiveness on Their Living-Abilities', 'link' => 'https://hdl.handle.net/11296/zx2p7g'],
			['date'=> '2015-01-01','title'=>'同伴動物對高齡者主觀幸福感經驗初探The subjective wellbeing experience of elders from being with companion animal', 'link' => 'https://hdl.handle.net/11296/7q72yn'],
			['date'=> '2015-01-01','title'=>'動物輔助治療方案運用於學齡前多重障礙兒童服務之探討-以愛智發展中心為例A Process Evaluation of a Animal-Assisted Therapy Program on the Preschool Multiple Disabilities-Taking ”Ai-Chi Development Center of the First Social Welfare Foundation”', 'link' => 'https://hdl.handle.net/11296/t8s9cs'],
			['date'=> '2015-01-01','title'=>'動物輔助治療團體對高關懷青少年心理適應之影響The influence of animal-assisted group counseling on high-risk young adolescents’psychological adjustment', 'link' => 'https://hdl.handle.net/11296/4ymn98'],
			['date'=> '2015-01-01','title'=>'動物輔助治療方案影響失智老人身心健康之研究Effects of Animal-Assisted Therapy on the Physical and Psychological Health for the Elderly with Dementia', 'link' => 'https://hdl.handle.net/11296/8jn4mt'],
			['date'=> '2016-01-01','title'=>'飼主遭遇同伴動物非預期死亡之歷程探討: 以四位兒童期受忽略成人為例', 'link' => 'https://hdl.handle.net/11296/k6rc38'],
			['date'=> '2017-01-01','title'=>'動物輔助活動對憂鬱傾向青少年之影響Influence of the Animal Assisted Activity on the Young Adolescents with Depression Inclination', 'link' => 'https://hdl.handle.net/11296/px4ae4'],
			['date'=> '2017-01-01','title'=>'動物輔助活動方案對自閉症兒童人際互動改變之個案研究The Effects of Animal-Assited Activities Program on Interaction among Children with Autism：A Case Study', 'link' => 'https://hdl.handle.net/11296/fs4t7c'],
			['date'=> '2017-01-01','title'=>'動物輔助治療團體對注意力不足過動症兒童社交技巧之輔導效果The Effect of Animal-Assisted Group Therapy on Social Skills of Children with Attention Deficit Hyperactivity Disorder', 'link' => 'https://hdl.handle.net/11296/72xqpg'],
			['date'=> '2017-01-01','title'=>'發展動物輔助治療於一位自閉症兒童的治療歷程：客體關係分析式觀點', 'link' => 'https://hdl.handle.net/11296/v24w49'],
			['date'=> '2017-01-01','title'=>'治療犬介入對護理之家老年住民幸福感、孤寂感成效的探討The Effects of A Therapeutic Dog Interventions on Happiness and Loneliness among Elderly Residents in Nursing Home', 'link' => 'https://hdl.handle.net/11296/5sbx42'],
		];
		foreach($data as $research){
			Researches::create($research);
		}
    }
}
