<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */

    "accepted"         => "必須接受 <b class='error-column'>:attribute</b>",
    "active_url"       => "<b class='error-column'>:attribute</b> 必須是合法的URL地址",
    "after"            => "<b class='error-column'>:attribute</b> 必須是在 :date 之後的日期",
    "alpha"            => "<b class='error-column'>:attribute</b> 只能包含英文字母",
    "alpha_dash"       => "<b class='error-column'>:attribute</b> 只能包含英文字母,數字和減號",
    "alpha_num"        => "<b class='error-column'>:attribute</b> 只能包含英文字母和數字",
    "before"           => "<b class='error-column'>:attribute</b> 必須是在 :date. 之前的日期",
    "between"          => array(
        "numeric" => "<b class='error-column'>:attribute</b> 必須在:min - :max之間",
        "file"    => "<b class='error-column'>:attribute</b> 大小必須在:min kb - :max kb 之間",
        "string"  => "<b class='error-column'>:attribute</b> 長度必須在:min - :max 之間",
    ),
    "confirmed"        => "<b class='error-column'>:attribute</b> 必須一致",
    "date"             => "<b class='error-column'>:attribute</b> 必須是合法的日期",
    "date_format"      => "<b class='error-column'>:attribute</b> 必須符合格式 :format",
    "different"        => "<b class='error-column'>:attribute</b> 必須和 :other 不同",
    "digits"           => "<b class='error-column'>:attribute</b> 位數必須是 :digits",
    "digits_between"   => "<b class='error-column'>:attribute</b> 位數必須在 :min 到 :max 之間",
    "email"            => "<b class='error-column'>:attribute</b> 必須是合法的Email格式",
    "exists"           => "<b class='error-column'>:attribute</b> 不存在",
    "image"            => "<b class='error-column'>:attribute</b> 必須是圖片",
    "in"               => "<b class='error-column'>:attribute</b> 不合法",
    "integer"          => "<b class='error-column'>:attribute</b> 必須是整數",
    "ip"               => "<b class='error-column'>:attribute</b> 必須是合法的IP地址",
    "max"              => array(
        "numeric" => "<b class='error-column'>:attribute</b> 不能大於 :max",
        "file"    => "<b class='error-column'>:attribute</b> 文件大小不能大於 :max kb",
        "string"  => "<b class='error-column'>:attribute</b> 長度不能大於 :max",
    ),
    "mimes"            => "<b class='error-column'>:attribute</b> 文件格式必須是 :values 其中之一",
    "min"              => array(
        "numeric" => "<b class='error-column'>:attribute</b> 不能小於 :min",
        "file"    => "<b class='error-column'>:attribute</b> 文件大小不能小於 :min kb",
        "string"  => "<b class='error-column'>:attribute</b> 長度不能小於 :min",
    ),
    "not_in"           => "<b class='error-column'>:attribute</b> 不合法",
    "numeric"          => "<b class='error-column'>:attribute</b> 必須是數字",
    "regex"            => "<b class='error-column'>:attribute</b> 不合法",
    "required"         => "<b class='error-column'>:attribute</b> 必須填寫",
    "required_if"      => ":other 為 :value 的時候必須填寫 <b class='error-column'>:attribute</b>",
    "required_with"    => ":values 填寫了的時候也必須填寫 <b class='error-column'>:attribute</b>",
    "required_without" => ":values 不填寫的時候必須填寫 <b class='error-column'>:attribute</b>",
    "same"             => "<b class='error-column'>:attribute</b> 必須和 :other 相同",
    "size"             => array(
        "numeric" => "<b class='error-column'>:attribute</b> 位數必須是 :size",
        "file"    => "<b class='error-column'>:attribute</b> 大小必須是 :size kb",
        "string"  => "<b class='error-column'>:attribute</b> 長度必須為 :size characters",
    ),
    "unique"           => "<b class='error-column'>:attribute</b> 已經存在了",
    "url"              => "<b class='error-column'>:attribute</b> 格式不正確",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => array(),

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => array(
        "tw_name" => "中文姓名",
        "en_name" => "英文姓名",
        "gender"  => "性別",
        "birth"   => "生日",
        "twID"    => "身分證字號",
        "email"   => "信箱",
        "degree"  => "學歷",
        "school"  => "畢業學校",
        "office"  => "服務單位",
        "ophone"  => "單位電話",
        "fax"     => "傳真",
        "addr1"   => "永久地址",
        "home"    => "居住地址",
        "phone1"  => "住家電話",
        "mphone"  => "行動電話",
        "password"=> "密碼",
        "password_confirmation" => "確認密碼",
        "nickname"=> "綽號",
        "dog_name"=> "治療犬名稱",
        "dog_sex"=> "性別",
        "dog_id" => "晶片號",
        "dog_type"=> "品種",
        "dog_birth"=> "出生年",
        "dog_first_ok"=> "首次合格日期",
        "dog_start_date"=> "效期開始",
        "dog_end_date" => "效期結束",
        "dog_img" => "寵物照片"

    ),

);
