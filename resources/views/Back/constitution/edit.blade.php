@extends('back.layout.master')
@section('content')

    <div class="container panel flex-3 bg-coffee">

        <h3 class="div_relative">
            協會章程-修改
            <div class="inline btn-right">
                <a href="{{URL("admin/structure")}}" class="btn btn-info">
                    <i class="fa fa-info-o">回到首頁</i>
                </a>
            </div>
        </h3>
        <hr>

        <div class="invoice" style="min-height: 600px">
            <form action="{{url('admin/constitution')}}" method="POST">
                {{ csrf_field() }}

                <table class="table">
                    <tr>
                        <td class="section">
                            <h3>總則</h3>
                            <a class="btn btn-info add_btn" data="total">新增</a>

                            <div>條號</div>
                            @foreach($total as $value)


                            <div class="input-group-lg">
                                <div class="col-md-3">
                                    <input disabled type="text"  name="total_key[]" id="total_key[]" class="form-control" value="{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}"/>
                                </div>
                                <div class="col-md-7">
                                    <textarea required name="total_value[]" class="form-control" id="total_value[]" cols="50" rows="5">{{$value->value}}</textarea>
                                </div>
                                <div class="col-md-2">
                                    <a class="del btn btn-danger">刪除</a>
                                </div>
                            </div>

                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input type="submit" value="確定修改" class="btn btn-success btn-lg"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="section">
                            <h3>會員</h3>
                            <a class="btn btn-info add_btn" data="member">新增</a>

                            <div>條號</div>
                            @foreach($member as $value)
                                <div class="input-group-lg">
                                    <div class="col-md-3">
                                        <input disabled type="text"  name="member_key[]" id="member_key[]" class="form-control" value="{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}"/>
                                    </div>
                                    <div class="col-md-7">
                                        <textarea required name="member_value[]" class="form-control" id="member_value[]" cols="50" rows="5">{{$value->value}}</textarea>
                                    </div>
                                    <div class="col-md-2">
                                        <a class="del btn btn-danger">刪除</a>
                                    </div>
                                </div>

                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input type="submit" value="確定修改" class="btn btn-success btn-lg"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="section">
                            <h3>組織及職權</h3>
                            <a class="btn btn-info add_btn" data="power">新增</a>

                            <div>條號</div>
                            @foreach($power as $value)
                                <div class="input-group-lg">
                                    <div class="col-md-3">
                                        <input disabled type="text"  name="power_key[]" id="power_key[]" class="form-control" value="{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}"/>
                                    </div>
                                    <div class="col-md-7">
                                        <textarea required name="power_value[]" class="form-control" id="power_value[]" cols="50" rows="5">{{$value->value}}</textarea>
                                    </div>
                                    <div class="col-md-2">
                                        <a class="del btn btn-danger">刪除</a>
                                    </div>
                                </div>

                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input type="submit" value="確定修改" class="btn btn-success btn-lg"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="section">
                            <h3>會議</h3>
                            <a class="btn btn-info add_btn" data="meeting">新增</a>

                            <div>條號</div>
                            @foreach($meeting as $value)
                                <div class="input-group-lg">
                                    <div class="col-md-3">
                                        <input disabled type="text"  name="meeting_key[]" id="meeting_key[]" class="form-control" value="{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}"/>
                                    </div>
                                    <div class="col-md-7">
                                        <textarea required name="meeting_value[]" class="form-control" id="meeting_value[]" cols="50" rows="5">{{$value->value}}</textarea>
                                    </div>
                                    <div class="col-md-2">
                                        <a class="del btn btn-danger">刪除</a>
                                    </div>
                                </div>

                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input type="submit" value="確定修改" class="btn btn-success btn-lg"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="section">
                            <h3>經費及會計</h3>
                            <a class="btn btn-info add_btn" data="money">新增</a>

                            <div>條號</div>
                            @foreach($money as $value)
                                <div class="input-group-lg">
                                    <div class="col-md-3">
                                        <input disabled type="text"  name="money_key[]" id="money_key[]" class="form-control" value="{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}"/>
                                    </div>
                                    <div class="col-md-7">
                                        <textarea required name="money_value[]" class="form-control" id="money_value[]" cols="50" rows="5">{{$value->value}}</textarea>
                                    </div>
                                    <div class="col-md-2">
                                        <a class="del btn btn-danger">刪除</a>
                                    </div>
                                </div>

                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input type="submit" value="確定修改" class="btn btn-success btn-lg"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="section">
                            <h3>附則</h3>
                            <a class="btn btn-info add_btn" data="sub">新增</a>

                            <div>條號</div>
                            @foreach($sub as $value)
                                <div class="input-group-lg">
                                    <div class="col-md-3">
                                        <input disabled type="text"  name="sub_key[]" id="sub_key[]" class="form-control" value="{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}"/>
                                    </div>
                                    <div class="col-md-7">
                                        <textarea required name="sub_value[]" class="form-control" id="sub_value[]" cols="50" rows="5">{{$value->value}}</textarea>
                                    </div>
                                    <div class="col-md-2">
                                        <a class="del btn btn-danger">刪除</a>
                                    </div>
                                </div>

                            @endforeach
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" align="center">
                            <input type="submit" value="確定修改" class="btn btn-success btn-lg"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>

        <script>
            $(function() {

                $(document).on("click", ".del", function () {
                    $(this).parents('.input-group-lg').remove();  // jQuery 1.7+
                });
                $(".add_btn").click(function () {
                    var $data = $(this).attr('data');
                    if(typeof($data)== 'undefined'){
                        console.warn('not data');
                    }else{
                        $(this).parent('.section').append(
                            '<div class="input-group-lg" style="padding-top:10px;">'+
                            '<div class="col-md-3">'+
                            '<input disabled type="text"  name="'+$data+'_key[]" id="'+$data+'_key[]" class="form-control" value=""/>'+
                            '</div>'+
                            '<div class="col-md-7">'+
                            '<textarea name="'+$data+'_value[]" class="form-control" id="'+$data+'_value[]" cols="50" rows="5"></textarea>'+
                            '</div>'+
                            '<div class="col-md-2">'+
                            '<a class="del btn btn-danger">刪除</a>'+
                            '</div>'+
                            '</div>'
                        );
                    }

                });
            });
        </script>


    </div>
@endsection

