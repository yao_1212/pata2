<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->

            <li class="treeview {{  (Request::is('admin/structure') || Request::is('admin/commit')) ? ' active' : ''}}">

                <a href="#"><span>協會簡介管理</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is('admin/structure') ? ' active' : ''}}">
                        <a href="{{URL("admin/structure")}}"><span>理監事名單管理</span></a>
                    </li>
                    <li class="{{ Request::is('admin/commit') ? ' active' : ''}}">
                        <a href="{{URL("admin/commit")}}"><span>委員會名單管理</span></a>
                    </li>

                </ul>
            </li>
            <li class="{{ Request::is('admin/constitution/edit') ? ' active' : ''}}">
                <a href="{{URL("admin/constitution/edit")}}"><span>協會章程管理</span></a>
            </li>
            <li class="{{ Request::is('admin/yvtset/user_news') ? ' active' : ''}}">
                <a href="{{URL("admin/yvtset/user_news/")}}"><span>會員專屬公告管理</span></a>
            </li>
            <li class="{{ Request::is('admin/yvtset/news') ? ' active' : ''}}">
                <a href="{{URL("admin/yvtset/news/")}}"><span>最新消息管理</span></a>
            </li>
            <li class="{{ Request::is('admin/master') ? ' active' : ''}}">
                <a href="{{URL("admin/master/")}}"><span>會員管理</span></a>
            </li>

            <li class="{{ Request::is('admin/event') ? ' active' : ''}}">
                <a href="{{URL("admin/event/")}}"><span>動輔活動管理</span></a>
            </li>
            <li class="{{ Request::is('admin/yvtset/master') ? ' active' : ''}}">
                <a href="{{URL("admin/yvtset/master/")}}"><span>動輔師管理</span></a>
            </li>

{{--            <li class="{{ Request::is('admin/yvtset/dog') ? ' active' : ''}}">
    <a href="{{URL("admin/yvtset/dog/")}}"><span>治療犬管理</span></a>
</li>--}}
            

            

            <li class="{{ Request::is('admin/dogAlbum') ? ' active' : ''}}">
                <a href="{{URL("admin/dogAlbum")}}"><span>治療犬介紹</span></a>
            </li>
            <li class="{{ Request::is('admin/yvtset/album') ? ' active' : ''}}">
                <a href="{{URL("admin/yvtset/album/")}}"><span>活動花絮管理</span></a>
            </li>
            <li class="{{ Request::is('admin/yvtset/donate') ? ' active' : ''}}">
                <a href="{{URL("admin/yvtset/donate/")}}"><span>捐款芳名錄管理</span></a>
            </li>

            <li class="{{ Request::is('admin/yvtset/services') ? ' active' : ''}}">
                <a href="{{URL("admin/yvtset/services/")}}"><span>服務實績管理</span></a>
            </li>
            <li class="{{ Request::is('admin/yvtset/reports') ? ' active' : ''}}">
                <a href="{{URL("admin/yvtset/reports/")}}"><span>媒體報導管理</span></a>
            </li>
            <li class="{{ Request::is('admin/yvtset/research') ? ' active' : ''}}">
                <a href="{{URL("admin/yvtset/researches/")}}"><span>動輔相關研究⽂獻管理</span></a>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>