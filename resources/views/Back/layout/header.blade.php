<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">台灣動物輔助治療專業發展協會</a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <form action="{{ url("admin/logout") }}" method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="hidden-xs" style="color: #fff;
                                                                    margin-right: 20px;
                                                                    height: 50px;
                                                                    font-size: 20px;
                                                                    border: none;
                                                                    background: none;">
                            登出
                        </button>
                    </form>
                </li>
            </ul>
        </div>
    </nav>
</header>