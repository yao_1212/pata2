@extends('back.layout.master')
@section('content')
    <div class="container panel flex-3 bg-coffee">
        <h3 class="div_relative">
            {{$yvtset->labelname}}管理
            <div class="inline btn-right">
                <a href="{{URL("admin/yvtset/".$yvtset->getTable()."/create")}}" class="btn btn-success">
                    <i class="fa glyphicon-plus">新增{{$yvtset->labelname}}</i>
                </a>
            </div>
        </h3>
        <hr>
            <table class="table table-striped table-hover index-datatable">
                <thead>
                @foreach($yvtset->columns as $key => $yvt_val)
                    @if($yvt_val["listshow"])
                        <th>{{ $yvt_val["label"] }}</th>
                    @endif
                @endforeach
                </thead>

                <tbody>
                @foreach($yvtsets as $key => $value)
                    <tr>
                    @foreach($yvtset->columns as $key => $yvt_val)
                        @if($yvt_val["listshow"])
                            @if($yvt_val["listshow"]==="url_photo")
                                <td class="img-wrap"><img  class="img" src="{{ URL( "upload/".$yvtset->getTable()."/".$value->$key)}}"></td>
                            @else
                                <td>{{ $value->$key }}</td>
                            @endif
                        @endif
                    @endforeach
                        <?php $id = $value->getKeyName()?>
                        <td>
                            <a href="{{URL("admin/yvtset/".$yvtset->getTable()."/".$value->$id)}}" class="btn btn-info"><i class="glyphicon glyphicon-pencil"></i></a>
                            @if(isset($yvtset->delete) && $yvtset->delete == true)
                            <form  onsubmit="return confirm('確定要刪除嗎？資料將無法恢復。');" style="display:inline-block;" action="{{URL("admin/yvtset/".$yvtset->getTable()."/".$value->$id)}}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-info"><i class="glyphicon glyphicon-trash"></i></button>
                            </form>
                            @endif
                        </td>
                        
                    </tr>
                @endforeach
                </tbody>
            </table>
        {{ $yvtsets->links() }}
    </div>
    <script>
        $(function(){
            deleteConfirm(){
                confirm('確定要刪除？');
                return false;
            }
        })
    </script>
@endsection

