<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{asset("css/back.css")}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<style>
    .ui-state-highlight { min-height: 120px; line-height: 1.2em; }
</style>
<script>
    $(function(){
        $(".save-btn").click(function(){
            var order_header  = $(this).parents(".content-wrap-header");
            var order_content = $(this).parents(".order-content");

            var drag          = order_content.children(".drag");
            var enable_btn    = order_content.find(".order-enable-btn");
            var notify_btn    = order_content.find(".notify-btn");

            drag.sortable({disabled:true});

            drag.removeClass("drag-enable");

            notify_btn.removeClass("hide");
            enable_btn.addClass("hide");
            $(this).addClass("hide");

        });

        //按下編輯順序
        $(".notify-btn").click(function(){
            var order_header  = $(this).parents(".content-wrap-header");
            var order_content = $(this).parents(".order-content");

            var drag          = order_content.children(".drag");
            var enable_btn    = order_header.find(".order-enable-btn");
            var save_btn      = order_content.find(".save-btn");


            save_btn.removeClass("hide");
            enable_btn.removeClass("hide");

            $(this).addClass("hide");
            drag.addClass("drag-enable");

            drag.sortable({
                disabled: false,
                scroll: true,
                revert: true,
                cursor: "move",
                placeholder: "order-selected",
                activate:function( event, ui ) {
                    ui.item.addClass("num-selected-border");
                    ui.item.find(".num-wrap").addClass("num-selected");
                },
                beforeStop: function( event, ui ) {
                    ui.item.removeClass("num-selected-border");
                    ui.item.find(".num-wrap").removeClass("num-selected");

                }
            });

            $( "#sortable" ).disableSelection();

        });



        //按鈕提示
        $(".notify-btn").hover(function(e){
            var notify = $(this).parent().find(".notify-sm");
            notify.removeClass("hide");

        },function(){
            var parent = $(this).parent();
            var notify = $(this).parent().find(".notify-sm");
            notify.addClass("hide");
        })



    })
</script>
<div class="body-wrap">

    {{-- order-content div start --}}
    <div class="order-content">
        <div class="content-wrap-header flex end-right">
            <div class="notify">
                <button class="btn edit-bg notify-btn">
                    <div class="notify-sm edit-bd edit-color hide">
                        更動呈現於頁面的順序
                    </div>
                    <i class="fa fa-pencil-square-o edit-icon" aria-hidden="true"></i>
                    更動順序
                </button>
            </div>
            <button class="btn order-temp-color white-bg order-temp-border order-enable-btn hide">
                更動順序中
            </button>
        </div>

        {{--drag--}}
        <div class="drag" id="sortable">


            <div class="order-item">
                <div class="num-wrap num-bg center middle ">
                    <div class="num">
                        1
                    </div>
                </div>
                <div class="img-wrap">
                    <img class="img" src="https://lh5.googleusercontent.com/ADP4oye-S_nEieHCA4DPTdFzTGdRcjDXES_JYBlQ5uBDrzOLvdBDUPHgkwZpJpG8DTBH6OEvjX64Wj1FPDS-PV2rCsEQXTczcclrQm0vwNn3i4sz95oNHNukB-VjXPoKo-pgz5XZ" alt="">
                </div>
                <div class="content-wrap center middle">
                    <div class="content-wrap-title">
                        <div class="title">
                            yoyoyoyoyo
                        </div>
                    </div>
                    <div class="edit">
                        <div class="circle-wrap">
                            <div class="circle" align="center">
                                <i class="fa fa-pencil-square-o fa-2x edit-icon" aria-hidden="true"></i>
                                <span class="edit-text">編輯</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="order-item">
                <div class="num-wrap center middle">
                    <div class="num">
                        2
                    </div>
                </div>
                <div class="img-wrap">
                    <img class="img" src="http://img.gq.com.tw/userfiles/sm/sm645_images_A1/30569/2016122265580237.jpg" alt="">
                </div>
                <div class="content-wrap center middle">
                    <div class="content-wrap-title">
                        <div class="title">
                            新垣結衣新垣結衣新垣結衣新垣結衣新垣結衣新垣結衣
                        </div>
                    </div>
                    <div class="edit">
                        <div class="circle-wrap">
                            <div class="circle" align="center">
                                <i class="fa fa-pencil-square-o fa-2x edit-icon" aria-hidden="true"></i>
                                <span class="edit-text">編輯</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
        {{--end drag--}}
        <div class="content-wrap-footer flex end-right">
            <button class="btn order-temp-bg btn-md save-btn hide">儲存</button>
        </div>
    </div>
    {{--end order div --}}
</div>
</body>
</html>