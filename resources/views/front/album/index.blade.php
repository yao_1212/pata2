@extends('front.layout.master2')

@section('content')
    <link rel="stylesheet" href="{{asset("css/album.css")}}">
    <div class="body_bg bg-coffee">
        <div class="content" style="background-color:#fff;padding:30px;">
            <h1>活動花絮</h1>
            <hr><br>

            <div class="flex-box flex-row ">
                @foreach($albums as $album)
                    <div style="padding: 10px;display: inline-block">
                        <a href="{{url("album/".$album->album_id)}}">
                            <div class="img-wrap"  src="{{asset("upload/album/".$album->cover)}}" style="background-image: url('{{asset("upload/album/".$album->cover)}}')">
                                <div class="album-title">
                                    <div class="album-content">
                                        {{$album->title}}
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach

            </div>
            <div align="center">
                {{ $albums->links() }}
            </div>
            {{--<div class="photo-list">--}}
                {{--<div class="col-3">--}}
                    {{--<div class="img-wrap">--}}
                        {{--<img class="" src="http://cw1.tw/CW/images/article/C1462256735570.jpg" alt="">--}}
                        {{--<div class="album-title">--}}
                            {{--<div class="album-content">--}}
                                {{--<i class="fa fa-arrow-right" aria-hidden="true"></i>--}}
                                {{--第四屆第一次會員大會--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-3">--}}
                    {{--<div class="img-wrap">--}}
                        {{--<img class="" src="http://cw1.tw/CW/images/article/C1348198443709.jpg" alt="">--}}
                        {{--<div class="album-title">--}}
                            {{--<div class="album-content">--}}
                                {{--<i class="fa fa-arrow-right" aria-hidden="true"></i>--}}
                                {{--第四屆第四次理監事會議--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-3">--}}
                    {{--<div class="img-wrap">--}}
                        {{--<img class="" src="http://www.cnjlzj.com/upLoad/album/month_1503/201503071652021804.jpg" alt="">--}}
                        {{--<div class="album-title">--}}
                            {{--<div class="album-content">--}}
                                {{--<i class="fa fa-arrow-right" aria-hidden="true"></i>--}}
                                {{--資訊需求分析與應用研討會--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}


            {{--</div>--}}
            {{--<div class="photo-list">--}}
                {{--<div class="col-3">--}}
                    {{--<div class="img-wrap">--}}
                        {{--<img class="" src="http://fun.nmns.edu.tw/files/c/photo_m2000338.jpg" alt="">--}}
                        {{--<div class="album-title">--}}
                            {{--<div class="album-content">--}}
                                {{--<i class="fa fa-arrow-right" aria-hidden="true"></i>--}}
                                {{--第四屆會員大會--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-3">--}}
                    {{--<div class="img-wrap">--}}
                        {{--<img class="" src="http://farm3.staticflickr.com/2507/3902693560_3e5d04d27e_z.jpg" alt="">--}}
                        {{--<div class="album-title">--}}
                            {{--<div class="album-content">--}}
                                {{--<i class="fa fa-arrow-right" aria-hidden="true"></i>--}}
                                {{--理監事會議--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}




            {{--</div>--}}
            {{----}}
            {{----}}
        </div>
    </div>
@endsection