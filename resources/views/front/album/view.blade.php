@extends('front.layout.master2')

@section('content')
    <script src="{{asset("js/photo_show.js")}}"></script>
    <link rel="stylesheet" href="{{asset("css/album.css")}}">
    <div class="body_bg">
        <div class="content">
            <h1>活動花絮 - {{$albums->title}}</h1>
            <hr><br>

            <div class="flex-box flex-row ">
                @foreach($photos as $photo)
                    <div style="padding:10px;" class="photo-gallery">
                        <div class="img-wrap" src="{{asset("upload/album_photo/".$photo->img)}}" style="background-image: url('{{asset("upload/album_photo/".$photo->img)}}')"></div>
                    </div>
                @endforeach

            </div>

        </div>
    </div>
    <style>
        #overlay {
            background: rgba(0,0,0, .8);
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            display: none;
            text-align: center;
        }

        #overlay img {
            margin: 10% auto 0;
            width: 550px;
            border-radius: 5px;
        }
    </style>
    <script>


    </script>
@endsection