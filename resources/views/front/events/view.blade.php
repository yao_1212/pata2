@extends('front.layout.master2')
@section('content')

    <link rel="stylesheet" href="{{asset("css/front.css")}}">
    <link rel="stylesheet" href="{{asset("css/album.css")}}">

    <div class="container flex-3" style="min-height:600px">
        <h1>{{$events->title}}</h1>
        <hr>

        <div class="text-box">
            <div class="table-section">
                <table class="table-border-left">
                    <tr>
                        <td class="table-title-right">活動日期</td>
                        <td>{{ $events->comedate }}</td>
                    </tr>
                    <tr>
                        <td class="table-title-right">報名日期</td>
                        <td>{{ $events->startdate }}</td>
                    </tr>
                    <tr>
                        <td class="table-title-right">報名截止日期</td>
                        <td>{{ $events->outdate }}</td>
                    </tr>
                    <tr>
                        <td class="table-title-right">主辦單位</td>
                        <td>{{ $events->master }}</td>
                    </tr>
                    <tr>
                        <td class="table-title-right">活動地點</td>
                        <td>{{ $events->addr }}</td>
                    </tr>
                    <tr>
                        <td class="table-title-right">聯絡人</td>
                        <td>{{ $events->admin }}</td>
                    </tr>
                    <tr>
                        <td class="table-title-right">聯絡電話</td>
                        <td>{{ $events->phone }}</td>
                    </tr>
                    <tr>
                        <td class="table-title-right"></td>
                        <td>{!! $events->content !!}</td>
                    </tr>
                </table>
            </div>
        </div>


    </div>
@endsection