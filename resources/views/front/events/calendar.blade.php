@extends('front.layout.master2')
@section('content')
    <div class="container flex-3 bg-coffee">
        <h1>活動月曆</h1>
        <hr>
        <div class="text-box">
            <iframe src="https://calendar.google.com/calendar/embed?src=pata.org.tw%40gmail.com&ctz=Asia/Taipei" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
        </div>

    </div>
@endsection