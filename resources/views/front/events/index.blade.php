@extends('front.layout.master2')
@section('content')
    <div class="container flex-3" style="min-height:600px">
        <h1>活動列表</h1>
        <hr>
        <div class="text-box">
            <table class="table">
                <thead>
                <tr>
                    <th width="120">活動日期</th>
                    <th width="120">類別</th>
                    <th width="500">活動名稱</th>
                    <th width="180">截止報名時間</th>
                </tr>
                </thead>
                <tbody>
                @foreach($events as $event)
                    <tr>
                        <td>{{$event->comedate}}</td>
                        <td><b>{{$event->getType()}}</b></td>
                        <td><a href="{{url("events/".$event->id)}}">{{$event->title}}</a></td>
                        <td>{{$event->outdate}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div align="center">
                {{ $events->links() }}
            </div>
        </div>

    </div>
@endsection
