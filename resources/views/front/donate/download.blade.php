<link rel="stylesheet" href="{{asset("css/front.css")}}">
<h1>表單下載</h1>
<hr>


<div class="collapse">
    <h3>提醒您</h3>
    <ul>
        <li>請詳細閱讀信用卡款約定條款。</li>
        <li>我們收到您的捐款資料後將發送簡訊通知或主動去電確認；亦歡迎您來電至本會查詢。</li>
        <li>正式捐款收據將於單次捐款後2～4週或定期捐款之次年3月底前寄發，收據可供所得稅列舉申報。</li>
        <li>若您信用卡掛失、停用、換卡或升級，請待新卡收到後來電告知，以利處理後續捐款事宜。</li>
        <li>您的每一筆捐款皆公開徵信；若您不願意公開徵信，請於空白處註明，謝謝。</li>
    </ul>
</div>
<div class="download-list">
    <div class="section-box">
        <h3>信用卡表單專區</h3>
        <p>
            <a href="{{asset('file/PATA信用卡捐款授權.docx')}}"><i class="fa fa-angle-right"></i> PATA信用卡捐款授權書</a>
        </p>
    </div>

    {{--<div class="section-box">--}}
        {{--<h3>銀行/郵局表單專區</h3>--}}
        {{--<p>--}}
            {{--<a href=""><i class="fa fa-angle-right"></i> ACH定期定額轉帳捐款授權書   </a><br>--}}
            {{--<a href=""><i class="fa fa-angle-right"></i> 銀行自動轉帳捐款授權書(限:台北富邦、中國信託、國泰世華、彰化銀行、郵局使用)</a><br>--}}
            {{--<a href=""><i class="fa fa-angle-right"></i> 銀行定期轉帳變更授權書  </a><br>--}}
        {{--</p>--}}
    {{--</div>--}}


    {{--<div class="section-box">--}}
        {{--<h3>其他</h3>--}}
        {{--<p>--}}
            {{--<a href=""><i class="fa fa-angle-right"></i>  國稅局同意書 </a>--}}
        {{--</p>--}}
    {{--</div>--}}
</div>
