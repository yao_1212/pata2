<h1>捐款芳名錄</h1>
<hr>
{{--<div class="text-box">--}}
    {{--本會是一個社會公益團體，除了輔導工作由受過專業訓練義工擔任外，--}}
    {{--其經費一向來自熱心社會公益人士的贊助；我們亟需您一起加入生命守護者的行列，--}}
    {{--贊助本會諮商輔導、自殺防治工作與心理健康推廣活動等經費。--}}
    {{--您的愛心與慷慨解囊將有助於在人生道路走來顛沛的人們，將心中的烏雲，--}}
    {{--化為晴空萬里的藍天！--}}
{{--</div>--}}

<table class="table">
    <thead>
    <tr>
        <th>#</th>
        <th>收據編號</th>
        <th>捐款人姓名</th>
        <th>捐款日期</th>
        <th>捐款物資</th>
    </tr>
    </thead>
    <tbody>
    @foreach($donates as $key => $donate)
        <tr>
            <td>{{  $donates->total() - (($donates->currentPage()-1) * $donates->perPage()) - $key}}</td>
            <td>{{$donate->number}}</td>
            <td>{{$donate->getHideName() }}</td>
            <td>{{$donate->donate_time}}</td>
            <td>{{$donate->money}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<div align="center">
    {{ $donates->links() }}
</div>
