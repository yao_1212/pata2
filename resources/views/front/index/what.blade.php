<div class="container flex-3" 
	style="
	background-image: url({{ url('img/pata_what.png') }});    
	background-size: 400px;
    background-position: right;
    background-repeat: no-repeat;    
    ">

    <div class="form_box">
        <p style="font-size:18px;">
    		<h1 style="font-size: 24px">什麼是「動物輔助治療」呢？</h1>        
    		<hr>
            動物輔助治療是加入動物參與的一種療育方式，在經過特定條件篩選和訓練後的動物都可以被運用來達成個案在認知、情緒、社會人際、教育、復健、壓力調適等功能。
            <br><br>
            本協會目前以犬類動物為主，針對服務對象的療育目標，在動輔師、動輔員(治療犬主人)、治療犬三者一體的合作模式下設計活動內容，引領個案和治療犬互動以完成療育目的。
        </p>		
    </div>    
    <br><br>
    <div class="form_box">
        <a href="{{url('/news/12')}}">
            <button class="apply-now-btn">立即申請協會活動<button>
        </a>
	  	<!-- <h1>如何申請協會活動？</h1> -->
	</div>

</div>

<style>
.form_box{
	width:50%;
}
.form_box > a{
    cursor: pointer;
}
.what_img img{
	width:100%;	
}
</style>