<div class="container flex-3 hide" style="max-height: 375px;">
    <h1>您可以這樣協助 我們</h1>
    <hr>
    <div class="help_div">
        <div class="sort_num">1</div>
        <div class="content">
            <div class="title">
                <b>與我們同工</b>
            </div>
            <div class="list">
                <ul class="list">
                    <li>
                        <i class="icon ion-arrow-right-b coffee"></i>
                        <span class="title">
                            加入會員
                        </span>
                    </li>
                    <li>
                        <i class="icon ion-arrow-right-b coffee"></i>
                        <span class="title">
                            接受培訓
                        </span>
                    </li>
                    <li>
                        <i class="icon ion-arrow-right-b coffee"></i>
                        <span class="title">
                            投入服務
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="help_div">
        <div class="sort_num">2</div>
        <div class="content">
            <div class="title">
                <b>與我們同心</b>
            </div>
            <div class="list">
                <ul class="list">
                    <li>
                        <i class="icon ion-arrow-right-b coffee"></i>
                    <span class="title">
                        加入粉絲團
                    </span>
                    </li>
                    <li>
                        <i class="icon ion-arrow-right-b coffee"></i>
                    <span class="title">
                        分享協會訊息
                    </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="help_div">
        <div class="sort_num">3</div>
        <div class="content">
            <div class="title">
                <b>贊助資源</b>
            </div>
            <ul class="list">
                <li>
                    <i class="icon ion-arrow-right-b coffee"></i>
                    <span class="title">
                        定期捐款
                    </span>
                </li>
                <li>
                    <i class="icon ion-arrow-right-b coffee"></i>
                    <span class="title">
                        專案贊助
                    </span>
                </li>
                <li>
                    <i class="icon ion-arrow-right-b coffee"></i>
                    <span class="title">
                        企業贊助
                    </span>
                </li>
            </ul>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.info_button').mouseover(function () {
            $(this).find('.container').removeClass('hide', 1000, "easeInBack");
        }).mouseout(function () {
            $(this).find('.container').addClass('hide', 1000, "easeInBack");
        });
    })
</script>
