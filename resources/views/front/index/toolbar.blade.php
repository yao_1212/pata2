<div class="container flex-3" style="min-height: 340px;">
    <h1>會員資訊</h1>
    <hr>
    <form action="{{url("login")}}" method="post">
        {{ csrf_field() }}
        <div class="status-box" align="center">
            <h3 class="hello">Hello，{{Auth::user()->tw_name}}</h3>
            <div class="sub-title">{{Auth::user()->email}}</div>
        </div>
        <div class="form_box">
            <div class="">
                <span class="form_title">繳費狀況</span>
                @if($user->FrontGetIsPay() == 0)
                    <button disabled class="btn btn-yellow cursor_default">未繳費</button>
                @elseif($user->FrontGetIsPay() == 1)
                    <button disabled class="btn btn-danger cursor_default">繳費過期</button>
                @elseif($user->FrontGetIsPay() == 2)
                    <button disabled class="btn btn-success cursor_default">已繳費</button>
                @endif
            </div>
            @if($user->FrontGetIsPay() != 0)
            <div class="">
                <span class="form_title">最後繳費日期</span>
                <button disabled class="btn btn-success cursor_default">{{$user->payTime}}</button>
            </div>
            @endif
            <div class="">
                <span class="form_title">活動積分</span>
                <a href="{{url("user/event/list")}}">
                    <button disabled class="btn btn-yellow number cursor_default" align="center">{{$user->score}}</button>
                </a>
            </div>

            <div class="form_group fun-box" style="padding-right:0px;margin-top: 40px;" align="center">
                <a href="{{url("user/event/list")}}"class="btn btn-function btn-fun">活動積分</a>
                <a href="{{url("user/edit")}}"class="btn btn-function btn-fun">修改資料</a>
                <a href="{{url("logout")}}" class="btn btn-function btn-fun">登出</a>
            </div>
        </div>
    </form>

</div>
