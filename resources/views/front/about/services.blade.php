@extends('front.layout.master2')
@section('content')
    <div class="container flex-3">
        <h1>服務實績</h1>
        <hr>
        <div class="text-box">
            <table class="table">
                <thead>
                <tr>
                    <th width="110">發佈日期</th>
                    <th width="580">服務單位</th>
                </tr>
                </thead>
                <tbody>

                @foreach($services as $key => $data)
                        <tr>
                            <td>{{$data->getDateTime()}}</td>
                            <td>{{$data->title}}</td>
                        </tr>
                @endforeach
                </tbody>
            </table>

            <div align="center">
                {{ $services->links() }}
            </div>

        </div>

    </div>
@endsection