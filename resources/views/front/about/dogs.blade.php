@extends('front.layout.master2')

@section('content')
    <link rel="stylesheet" href="{{asset("css/album.css")}}">
    <div class="body_bg">
        <div class="content">
            <h1>治療犬介紹</h1>
            <br>   
            @foreach($dogs as $dog)
            <div class="dog_row">
                <div class="img">
                    <div class="dog" style="background:url({{asset("upload/dog_album/".$dog->img)}});"></div>
                </div>
                <div class="intro">
                    <div class="title">{{$dog->name}}</div>
                    <div class="dog_content">{{ $dog->type }} / {{ $dog->getSex() }} / {{$dog->getAge()}}歲</div>
                    <div class="subtitle">個性</div>
                    <div class="dog_content">{{ $dog->personality }}</div>

                    <div class="subtitle">喜歡吃什麼或做什麼事</div>
                    <div class="dog_content">{{ $dog->like }}</div>

                    <div class="subtitle">希望別人怎麼對待他</div>
                    <div class="dog_content">{{ $dog->treat }}</div>
                </div>
            </div>
            @endforeach


            {{-- <div class="flex-box flex-row flex-center">
                @foreach($dogs as $dog)
                    <div class="img-wrap dog_album">
                        <img class="" src="" alt="">
                        <div class="album-title">
                            <div class="album-content">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                {{$dog->name}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div> --}}
        </div>
    </div>
@endsection