@extends('front.layout.master2')
@section('content')

    <div class="body_bg">
        <div class="content">
            <h1>理監事名單</h1>
            <hr><br>
            <select name="" id="index" class="form-control">
            @foreach($structures as $index => $structure)
                <?php $staff = json_decode($structure->staff);?>
                <option value="{{$structure->id}}">{{$structure->title}}</option>
            @endforeach
            </select>

            <article class="structure-content">
                @foreach($structures as $index => $structure)
                    <?php $staff = json_decode($structure->staff);?>
                <table class="table structure-table" id="structure{{$structure->id}}">

                    <tr>
                        <td width="100" class="structure-title">理事長</td>
                        <td width="400" class="structure-sutitle">{{ $staff[0][0] }} <small> - {{ $staff[0][1] }}</small></td>
                    </tr>
                    <tr>
                        <td width="100" class="structure-title">常務理事</td>
                        <td width="400" class="structure-sutitle">
                            @foreach($staff[1] as $people)
                                {{$people[0]}}<small> - {{$people[1]}}</small><br>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td width="100" class="structure-title">理事</td>
                        <td width="400" class="structure-sutitle">
                            @foreach($staff[2] as $people)
                                {{$people[0]}}<small> - {{$people[1]}}</small><br>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td width="100" class="structure-title">常務監事</td>
                        <td width="400" class="structure-sutitle">
                            @foreach($staff[3] as $people)
                                {{$people[0]}}<small> - {{$people[1]}}</small><br>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td width="100" class="structure-title">監事</td>
                        <td width="400" class="structure-sutitle">
                            @foreach($staff[4] as $people)
                                {{$people[0]}}<small> - {{$people[1]}}</small><br>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td width="100" class="structure-title">秘書長</td>
                        <td width="400" class="structure-sutitle">
                            @foreach($staff[5] as $people)
                                {{$people[0]}}<small> - {{$people[1]}}</small><br>
                            @endforeach
                        </td>
                    </tr>
                </table>
                @endforeach

            </article>
        </div>
    </div>

    <script>
        $(function(){
            init($("#index").val());
            $("#index").change(function(){
                init($("#index").val());
            })
            function init($id){
                $(".structure-table").hide();
                $("#structure"+$id).show();

            }
        })
    </script>
@endsection
@section('css')
    <style>
        .structure-table{
            display: none;
            font-size: 20px;
        }
        .body_bg{
            width: 100%;
        }
        .structure-content{
            width:100%;
        }
        .structure dt{
            float: left;
            padding: 10px 10px;
        }
        .structure dd {
            margin-left: 96px;
            border-left: 1px dotted #ccc;
            padding: 10px 10px 10px 20px;
        }
        .structure .title{
            margin: 15px 0 0;
            font-size: 20px;
            background-color: #A7957A;
            color:#fff;
            padding: 5px 8px;
            border-radius: 3px;
        }
        .structure dd:nth-of-type(even) {
            background-color: #f6f6f6;
        }
        .table tbody .structure-title {
            text-align: center;
        }
    </style>
@endsection