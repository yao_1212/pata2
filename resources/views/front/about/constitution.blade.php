@extends('front.layout.master2')
@section('content')
    <style>
        .constitution dt{
            float: left;
            padding: 10px 10px;
        }
        .constitution dd {
            margin-left: 96px;
            border-left: 1px dotted #ccc;
            padding: 10px 10px 10px 20px;
        }
        .constitution .title{
            margin: 15px 0 0;
            font-size: 20px;
            background-color: #A7957A;
            color:#fff;
            padding: 5px 8px;
            border-radius: 3px;
        }
        .constitution dd:nth-of-type(even) {
            background-color: #f6f6f6;
        }
    </style>
    <div class="body_bg">
        <div class="content">
            <h1>協會章程</h1>
            <hr><br>
            <div align="right">
                101年01月01日第一屆第一次會員大會通過。<br>
                101年2月24日台內社字第1010089998號准予備查。<br>
                101年12月02日第一屆第二次會員大會通過。<br>
                102年01月02日台內社字第1010404012號函同意備查。<br>
                105年05月27日第三屆第三次理監事聯合會議通過。<br>
                105年09月02日第三屆第四次理監事聯合會議討論。<br>
                105年12月02日第三屆第五次理監事聯合會議通過。<br>
                106年02月25日第三屆第二次會員大會通過。<br>
                106年09月15日第三屆第八次理監事聯合會議通過。<br>
                107年02月24日第三屆第十次理監事聯合會議通過。<br>
                107年03月03日第四屆第一次會員大會通過。<br>
            </div>
            <article class="constitution">
                <dl>
                    <div class="title"><b>第一章　總則</b></div>
                    @foreach($total as $value)
                    <dt>{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}</dt>
                    <dd>{!! (nl2br($value->value)) !!}</dd>
                    @endforeach
                </dl>
                <dl>
                    <div class="title"><b>第二章　會員</b></div>
                    @foreach($member as $value)
                        <dt>{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}</dt>
                        <dd>{!! (nl2br($value->value)) !!}</dd>
                    @endforeach
                </dl>
                <dl>
                    <div class="title"><b>第三章　組織及職權</b></div>
                    @foreach($power as $value)
                        <dt>{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}</dt>
                        <dd>{!! (nl2br($value->value)) !!}</dd>
                    @endforeach
                </dl>
                <dl>
                    <div class="title"><b>第四章　會議</b></div>
                    @foreach($meeting as $value)
                        <dt>{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}</dt>
                        <dd>{!! (nl2br($value->value)) !!}</dd>
                    @endforeach
                </dl>
                <dl>
                    <div class="title"><b>第五章　經費及會計</b></div>
                    @foreach($money as $value)
                        <dt>{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}</dt>
                        <dd>{!! (nl2br($value->value)) !!}</dd>
                    @endforeach
                </dl>
                <dl>
                    <div class="title"><b>第六章　附則</b></div>
                    @foreach($sub as $value)
                        <dt>{{'第'.\App\Constitution::getNum2speak($value->key).'條'}}</dt>
                        <dd>{!! (nl2br($value->value)) !!}</dd>
                    @endforeach
                </dl>
            </article>
        </div>
    </div>


@endsection