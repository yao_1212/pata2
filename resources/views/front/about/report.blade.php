@extends('front.layout.master2')
@section('content')
    <div class="container flex-3">
        <h1>媒體報導</h1>
        <hr>
        <div class="text-box">
            <table class="table">
                <thead>
                <tr>
                    <th width="110">報導日期</th>
                    <th width="580">新聞標題</th>
                </tr>
                </thead>
                <tbody>
                @foreach($reports as $key => $data)
                    <tr>
                        <td>{{$data->getDateTime()}}</td>
                        <td><a target="_blank" href="{{$data->link}}">{{$data->title}}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div align="center">
                {{ $reports->links() }}
            </div>

        </div>

    </div>
@endsection