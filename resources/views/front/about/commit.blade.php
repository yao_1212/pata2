@extends('front.layout.master2')
@section('content')

    <div class="body_bg">
        <div class="content">
            <h1>各組委員會名單</h1>
            <hr><br>
            <select name="" id="index" class="form-control">
                @foreach($commits as $index => $commit)
                    <?php $staff = json_decode($commit->staff);?>
                    <option value="{{$commit->id}}">{{$commit->title}}</option>
                @endforeach
            </select>

            <article class="commit-content">
                @foreach($commits as $index => $commit)
                    <?php $staff = json_decode($commit->staff);?>
                    <table class="table commit-table" id="commit{{$commit->id}}">

                        <tr>
                            <td width="100" class="commit-title">教育研究委員會</td>
                            <td width="400" class="commit-sutitle">
                                主任委員 - {{ $staff[0][0] }}<br>
                                <small> {{ $staff[0][1] }}</small>
                            </td>
                        </tr>
                        <tr>
                            <td width="100" class="commit-title">國際事務委員會</td>
                            <td width="400" class="commit-sutitle">
                                @foreach($staff[1] as $people)
                                    主任委員 - {{$people[0]}}<br>
                                    <small>{{$people[1]}}</small><br>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td width="100" class="commit-title">會員委員會</td>
                            <td width="400" class="commit-sutitle">
                                @foreach($staff[2] as $people)
                                    主任委員 - {{$people[0]}}<br>
                                    <small>{{$people[1]}}</small><br>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td width="100" class="commit-title">財務委員會</td>
                            <td width="400" class="commit-sutitle">
                                @foreach($staff[3] as $people)
                                    主任委員 - {{$people[0]}}<br>
                                    <small>{{$people[1]}}</small><br>
                                @endforeach
                            </td>
                        </tr>
                    </table>
                @endforeach

            </article>
        </div>
    </div>

    <script>
        $(function(){
            init($("#index").val());
            $("#index").change(function(){
                init($("#index").val());
            })
            function init($id){
                $(".commit-table").hide();
                $("#commit"+$id).show();

            }
        })
    </script>
@endsection
@section('css')
    <style>
        .commit-table{
            display: none;
            font-size:20px;
        }
        .body_bg{
            width: 100%;
        }
        .commit-content{
            width:100%;
        }
        .commit dt{
            float: left;
            padding: 10px 10px;
        }
        .commit dd {
            margin-left: 96px;
            border-left: 1px dotted #ccc;
            padding: 10px 10px 10px 20px;
        }
        .commit .title{
            margin: 15px 0 0;
            font-size: 20px;
            background-color: #A7957A;
            color:#fff;
            padding: 5px 8px;
            border-radius: 3px;
        }
        .commit dd:nth-of-type(even) {
            background-color: #f6f6f6;
        }
        .table tbody .commit-title {
            text-align: center;
        }
    </style>
@endsection