@extends('front.layout.master2')
@section('content')
    <div class="container flex-3">
        <h1>相關研究文獻</h1>
        <hr>
        <div class="text-box">
            <table class="table">
                <thead>
                <tr>
                    <th width="110">出版年份</th>
                    <th width="580">文獻標題</th>
                </tr>
                </thead>
                <tbody>
                @foreach($researches as $key => $data)
                    <tr>
                        <td style="padding-left: 20px;">{{$data->getDateTime()}}</td>
                        <td>
                            @if(isset($data->link))
                                <a target="_blank" href="{{$data->link}}">{{$data->title}}</a>
                            @else
                                {{$data->title}}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div align="center">
                {{ $researches->links() }}
            </div>

        </div>

    </div>
@endsection