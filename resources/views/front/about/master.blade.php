@extends('front.layout.master2')

@section('content')
    <link rel="stylesheet" href="{{asset("css/album.css")}}">
    <div class="body_bg">
        <div class="content">
            <h1>動輔師介紹</h1>
            <br>   
            @foreach($masters as $master)
            <div class="dog_row master_row">
                <div class="img">
                    <div class="dog" style="background:url({{asset("upload/master/".$master->img)}});"></div>
                </div>
                <div class="intro">
                    <div class="title">{{$master->name}}</div>
                    <div class="subtitle">動輔師類別</div>
                    <div class="dog_content">{{ $master->work_type }}</div>
                    <div class="subtitle">執行資歷</div>
                    <div class="dog_content">{{$master->getYearsText()}}</div>
                    <div class="subtitle">動輔師類別</div>
                    <div class="dog_content">{{ $master->level }}</div>
                    <div class="subtitle">動輔師活動狀態</div>
                    <div class="dog_content">{{ $master->active }}</div>
                </div>
                <div class="master_intro">
                    <div class="subtitle">自我介紹</div>
                <div class="dog_content">{{$master->intro}}</div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection