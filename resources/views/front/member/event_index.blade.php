@extends('front.layout.master')

@section('content')
    <link rel="stylesheet" href="{{asset("css/album.css")}}">
    <div class="bg-coffee" style="height:100%">
        <div class="content">
            <div class="flex-box">
                @if(Auth::check("user"))
                    @include("front.index.toolbar")
                @else
                    @include("front.index.login")
                @endif
                    @include("front.member.event.list")
            </div>
            @foreach($user->dog as $dog)
                <div class="flex-box">
                    @include("front.member.event.dog")
                    @include("front.member.event.dog_event_list")
                </div>
            @endforeach
        </div>
    </div>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
    <script>
        $(function(){
            $(document).on('click', '#detail-btn', function(event){
                event.preventDefault();

                var $id = $(this).attr('dog_id');
                var dog;
                $.get('/user/dog/'+$id, function(data){
                    //success
                    dog = data;
                    swal({
                        title:'治療犬基本資料',
                        html:data,
                        width: 800,
                        padding: 50,
                    })
                }).fail(function(error){
                    console.log(error);
                });

            });
        })
    </script>
@endsection