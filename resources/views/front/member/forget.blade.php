@extends('front.layout.master')

@section('content')
    <link rel="stylesheet" href="{{asset("css/album.css")}}">
    <div class="body_bg">
        <div class="content login-box">
            <h1>忘記密碼</h1>
            <hr>
            <form action="{{url('user/reset/password')}}" method="post">
            {{ csrf_field() }}
                @include("errors.error")

            <div class="form_box">
                <div class="form_group">
                    <span class="form_title">Email</span>
                    <input type="text" name="email" id="email" placeholder="請輸入Email">
                </div>

                <div class="flex-box">
                    <div class="form_group flex-1 ">
                        <span class="form_title">驗證碼</span>
                        <input type="text" name="text_input" id="text_input" placeholder="請輸入驗證碼">
                    </div>
                    <div class="form_group flex-1 capture_box">
                        <img src="{{URL::to('captcha')}}" class="capture" >
                    </div>
                </div>

                <div class="form_group" style="padding-right:0px;margin-bottom: 10px;">
                    <input type="submit" class="btn btn-yellow" value="送出">
                </div>
            </div>
            <div class="info-text">
                <div class="info-title">注意事項</div>


                <ul>
                    <li>1. 帳號已寄至您私人信箱</li>
                    <li>2. 如個人資料有謬誤與操作問題，請聯絡 黃蘭嵐秘書 0978-794-231</li>
                    <li>3. 建議登入時使用IE10以上版本，網頁方能正常顯示與操作</li>
                </ul>
            </div>
            </form>
        </div>
    </div>
@endsection