<div class="container flex-3" style="max-height: 340px;">
    <h1>治療犬積分</h1>
    <hr>
    <div class="status-box" align="center">
        <h3 class="hello">{{$dog->name}}</h3>
        <div class="sub-title">{{$dog->number}}</div>
    </div>
    <div class="form_box">
        <div class="">
            <span class="form_title">治療犬開始日期</span>
            <button disabled class="btn btn-success">{{$dog->start_date}}</button>
        </div>
        <div class="">
            <span class="form_title">治療犬結束日期</span>
            <button disabled class="btn btn-success">{{$dog->end_date}}</button>
        </div>
        <div class="">
            <span class="form_title">活動積分</span>
            <a href="{{url("user/event/list")}}">
                <div class="btn btn-yellow number" align="center">{{$dog->score}}</div>
            </a>
        </div>

        <div class="form_group fun-box" style="padding-right:0px;margin-top: 40px;" align="center">
            {{--<a href="{{url("user/dog/list")}}"class="btn btn-function btn-fun">詳細資料</a>--}}
            <button class="btn  btn-fun" style="padding:0px" id="detail-btn" dog_id="{{$dog->dog_id}}">詳細資料</button>
        </div>
    </div>

</div>
