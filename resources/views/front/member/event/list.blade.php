<div class="container flex-7" id="">

    <h1>您已參加的活動</h1>
    <hr>
    <br>
    <ul class="list">
        <table class="table">
            <thead>
            <tr>
                <th width="400">活動名稱</th>
                <th width="80">積分</th>
                <th width="120">類別</th>
                <th width="120">活動日期</th>
            </tr>
            </thead>
            <tbody>
            @if(count($user->frontGetEventList) == 0)
                <tr>
                    <td colspan="4">尚未參加活動</td>
                </tr>
            @else
                @foreach($user->frontGetEventList as $event)
                    <tr>
                        <td><a href="{{url("events/".$event->id)}}">{{$event->title}}</a></td>
                        <td>{{$event->pivot->score}}</td>
                        <td><b>{{$event->getType()}}</b></td>
                        <td>{{$event->comedate}}</td>
                    </tr>
                @endforeach
            @endif

            </tbody>
        </table>


    </ul>
</div>
