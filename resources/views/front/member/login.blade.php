@extends('front.layout.master')

@section('content')
    <link rel="stylesheet" href="{{asset("css/album.css")}}">
    <div class="body_bg bg-coffee">
        <div class="content login-box">
            <h1>會員登入</h1>
            <hr>
            <form action="{{url("login")}}" method="post">
            {{ csrf_field() }}
                @include("errors.error")

            <div class="form_box">
                <div class="form_group">
                    <span class="form_title">帳號</span>
                    <input type="text" name="user" id="user" placeholder="請輸入帳號">
                </div>

                <div class="form_group">
                    <span class="form_title">密碼</span>
                    <input type="password" name="ps" id="ps" placeholder="請輸入密碼">
                </div>

                <div class="flex-box">
                    <div class="form_group flex-1 ">
                        <span class="form_title">驗證碼</span>
                        <input type="text" name="text_input" id="text_input" placeholder="請輸入驗證碼">
                    </div>
                    <div class="form_group flex-1 capture_box">
                        <img src="{{URL::to('captcha')}}" class="capture" >
                    </div>
                </div>

                <div class="form_group" style="padding-right:0px;">
                    <input type="submit" class="btn btn-yellow" value="登入">
                    <div class="forget"><a href="{{url('user/reset/password')}}">忘記密碼</a></div>
                </div>
            </div>
            <div class="info-text">
                <div class="info-title">注意事項</div>


                <ul>
                    <li>1. 密碼預設為身分證字號</li>
                    <li>2. 如個人資料有謬誤與操作問題，請聯絡 黃蘭嵐秘書 0978-794-231</li>
                    <li>3. 建議登入時使用IE10以上版本，網頁方能正常顯示與操作</li>
                </ul>
            </div>
            </form>
        </div>
    </div>
@endsection