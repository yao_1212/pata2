@extends('front.layout.master2')

@section('content')
    <link rel="stylesheet" href="{{asset("css/album.css")}}">
    <div class="container flex-3 bg-coffee" style="box-shadow:none;">

    <div class="body_bg" style="margin-top:-30px;padding:0px;">
        <div class="content sign-box" style="box-shadow:none;">
            <h1>會員修改</h1>
            <hr>
            <form action="{{url("user/edit")}}" method="post">
                {{ csrf_field() }}
                @include("errors.error")

                <div class="table-section">
                    <b>- 基本資料</b>
                    <table width="90%">
                        <tr>
                            <td class="table-title">中文姓名</td>
                            <td><input  type="text" name="tw_name" value="{{ $user->tw_name }}"></td>
                            <td class="table-title">英文姓名</td>
                            <td><input  type="text" name="en_name" value="{{$user->en_name}}"></td>
                        </tr>
                        <tr>
                            <td class="table-title">身分證字號</td>
                            <td><input  type="text" value="{{$user->twID}}" disabled></td>
                            <td class="table-title">生日</td>
                            <td><input  type="date" value="{{$user->birth}}" disabled></td>
                        </tr>
                        <tr>
                            <td class="table-title">性別</td>
                            <td>
                                <select name="gender" id="gender">
                                    <option value="1" >男</option>
                                    <option value="0" >女</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="table-title">通訊地址</td>
                            <td colspan="3">
                                <input  type="text" name="addr1" value="{{$user->addr1}}">
                            </td>
                        </tr>
                        <tr>
                            <td class="table-title">住家電話</td>
                            <td><input  type="text" name="phone1" value="{{$user->phone1}}"></td>
                            <td class="table-title">手機</td>
                            <td><input  type="text" name="mphone" value="{{$user->mphone}}"></td>
                        </tr>
                        <tr>
                            <td class="table-title">E-mail</td>
                            <td colspan="3">
                                <input name="email" type="email" value="{{$user->email}}">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="table-section">
                    <b>- 學歷 / 經歷 / 服務單位</b>
                    <table width="90%">
                        <tr>
                            <td class="table-title">學歷</td>
                            <td colspan="3">
                                <select name="degree" id="gender">
                                    <option value="1" {{ ($user->degree == "1" ? "selected":"") }}>學士</option>
                                    <option value="2" {{ ($user->degree == "2" ? "selected":"") }}>學士後靜修</option>
                                    <option value="3" {{ ($user->degree == "3" ? "selected":"") }}>碩士</option>
                                    <option value="4" {{ ($user->degree == "4" ? "selected":"") }}>博士</option>
                                    <option value="5" {{ ($user->degree == "5" ? "selected":"") }}>研究員</option>
                                    <option value="6" {{ ($user->degree == "6" ? "selected":"") }}>助理教授</option>
                                    <option value="7" {{ ($user->degree == "7" ? "selected":"") }}>教授</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="table-title">畢業學校</td>
                            <td colspan="3">
                                <input  type="text" name="school" value="{{$user->school}}">
                            </td>
                        </tr>
                        <tr>
                            <td class="table-title">服務單位</td>
                            <td colspan="3">
                                <input  type="text" name="office" value="{{$user->office}}">
                            </td>
                        </tr>
                        <tr>
                            <td class="table-title">服務單位電話</td>
                            <td><input  type="text" name="ophone" value="{{$user->ophone}}"></td>
                            <td class="table-title">傳真</td>
                            <td><input  type="text" name="fax" value="{{$user->fax}}"></td>
                        </tr>
                    </table>
                    <div class="info-text" style="margin-top:20px;">
                        <div class="info-title">注意事項</div>


                        <ul>
                            <li>1. 密碼預設為身分證字號</li>
                            <li>2. 如個人資料有謬誤與操作問題，請聯絡 黃蘭嵐秘書 0978-794-231</li>
                            <li>3. 建議登入時使用IE11以上版本，網頁方能正常顯示與操作</li>
                        </ul>
                    </div>
                    <div class="form_group" style="padding-right:0px;margin-top:30px;">
                        <input type="submit" class="btn btn-yellow" value="修改">
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
    <style>
        input[type="text"]:disabled {
            background: #dddddd;
        }
        input[type="select"]{
            background: #fff;
        }
    </style>
@endsection