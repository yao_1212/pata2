@extends('front.layout.master2')

@section('content')
    <link rel="stylesheet" href="{{asset("css/album.css")}}">
    <div class="container flex-3 bg-coffee">

        <div class="body_bg bg-coffee" style="padding:0px">
            <div class="content sign-box">
                <h1>密碼修改</h1>
                <hr>
                <form action="{{url("user/password")}}" method="post">
                    {{ csrf_field() }}
                    @include("errors.error")

                    <div class="table-section">
                        <b>- 請輸入密碼</b>
                        <table width="90%">
                            <tr>
                                <td class="table-title">新密碼</td>
                                <td><input  type="password" name="password" value=""></td>
                            </tr>
                            <tr>
                                <td class="table-title">確認新密碼</td>
                                <td><input  type="password" name="password_confirmation" value="" ></td>
                            </tr>
                        </table>
                    </div>
                    <div class="info-text" style="margin-top:20px;">
                        <div class="info-title">注意事項</div>


                        <ul>
                            <li>1. 密碼預設為身分證字號</li>
                            <li>2. 如個人資料有謬誤與操作問題，請聯絡 黃蘭嵐秘書 0978-794-231</li>
                            <li>3. 建議登入時使用IE11以上版本，網頁方能正常顯示與操作</li>
                        </ul>
                    </div>
                    <div class="form_group" style="padding-right:0px;margin-top:30px;">
                        <input type="submit" class="btn btn-yellow" value="修改">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection