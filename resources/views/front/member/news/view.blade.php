@extends('front.layout.master2')
@section('content')
    <link rel="stylesheet" href="{{asset("css/front.css")}}">

    <div class="container flex-3 bg-coffee">
        <h1>{{$news->title}}</h1>
        <hr>
        <div class="text-right">
            {{$news->getDateTime()}}
        </div>
        <div class="text-box">
            {!! $news->content !!}
        </div>
        @if(isset($news->filepath))
            <div class="file section-box">
                <p>
                    <a href="{{url("file/news/filepath/".$news->id)}}">
                        <i class="fa fa-angle-right" aria-hidden="true"></i> {{$news->filename}}
                    </a>
                </p>
            </div>
        @endif


    </div>
@endsection