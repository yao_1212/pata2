<div id="dog_template" class="table-section" align="center">
    <div class="img-wrap" src="{{asset("upload/dog/".$dog->img)}}" style="width: 300px;min-height: 200px;background-image: url('{{asset("upload/dog/".$dog->img)}}')">
    </div>
    <table width="80%" align="center">
        <tr>
            <td class="table-title">名稱</td>
            <td id="dog_name">
                {{$dog->name }}
            </td>
            <td class="table-title">晶片號</td>
            <td id="dog_number">
                {{$dog->number }}
            </td>
        </tr>
        <tr>
            <td class="table-title">性別</td>
            <td id="dog_sex">{{ ($dog->sex)==1? '公':'母' }}</td>
            <td class="table-title">品種</td>
            <td id="dog_type">{{$dog->type }}</td>
        </tr>
        <tr>
            <td class="table-title">出生年</td>
            <td id="dog_birth">{{$dog->getBirth() }}</td>
            <td class="table-title">合格日期</td>
            <td id="dog_first_ok">{{$dog->getFirst_ok() }}</td>
        </tr>
        <tr>
            <td class="table-title">效期開始</td>
            <td id="dog_start_date" >{{$dog->getStart_date() }}</td>
            <td class="table-title">效期結束</td>
            <td id="dog_end_date" >{{$dog->getEnd_date() }}</td>
        </tr>
    </table>
</div>