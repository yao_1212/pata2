@extends('front.member.mail.template')

@section('title')
    忘記密碼
@endsection

@section('content')
已將您的密碼，重置為身分證字號。
@endsection

@section('document')
    請使用身分證字號當作密碼登入。
@endsection
