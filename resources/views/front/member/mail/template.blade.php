<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0;">
    <meta name="format-detection" content="telephone=no"/>
<style>
body { 
    margin: 0; 
    padding: 0; 
    min-width: 100%; 
    width: 100% !important; 
    height: 100% !important;
}
body, table, td, div, p, a { 
    -webkit-font-smoothing: antialiased; 
    text-size-adjust: 100%; 
    -ms-text-size-adjust: 100%; 
    -webkit-text-size-adjust: 100%; 
    line-height: 100%; 
}
table, td { 
    mso-table-lspace: 0pt; 
    mso-table-rspace: 0pt; 
    border-collapse: collapse 
    !important; border-spacing: 0; 
}
img { 
    border: 0; line-height: 100%; 
    outline: none; 
    text-decoration: none; 
    -ms-interpolation-mode: bicubic; 
}
#outlook a { padding: 0; }
.ReadMsgBody { width: 100%; } 
.ExternalClass { width: 100%; }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { 
    line-height: 100%; 
}

/* Rounded corners for advanced mail clients only */ 
@media all and (min-width: 560px) {
    .container { 
        border-radius: 8px; 
        -webkit-border-radius: 8px; 
        -moz-border-radius: 8px; 
        -khtml-border-radius: 8px;
    }
}

/* Set color for auto links (addresses, dates, etc.) */ 
a, a:hover {
    color: #127DB3;
}
.footer a, .footer a:hover {
    color: #999999;
}

</style>
</head>

<body topmargin="0" rightmargin="0" bottommargin="0" leftmargin="0" marginwidth="0" marginheight="0" width="100%" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
    background-color: #F0F0F0;
    color: #000000;"
    bgcolor="#F0F0F0"
    text="#000000">

<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; margin-bottom: 50px; padding: 0; width: 100%;" class="background">
    <tr>
        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;" bgcolor="#F0F0F0">

            <table border="0" cellpadding="0" cellspacing="0" align="center" width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit; min-width: 560px;" class="wrapper">
                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
                        padding-top: 20px;
                        padding-bottom: 20px;">
                    </td>
                </tr>
            </table>

            <table border="0" cellpadding="0" cellspacing="0" align="center"
                bgcolor="#FFFFFF"
                width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
                min-width: 560px;" class="container">

                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 24px; font-weight: bold; line-height: 130%;
                        padding-top: 25px;
                        color: #000000;
                        font-family: sans-serif;" class="header">
                            @yield('title')
                    </td>
                </tr>
                
                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-bottom: 3px; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 300; line-height: 150%;
                        padding-top: 5px;
                        color: #000000;
                        font-family: sans-serif;" class="subheader">
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                        padding-top: 20px;" class="hero">
                        <img border="0" vspace="0" hspace="0"
                        src="http://www.taiwan-pata.org.tw/img/pata_what.png"
                        alt="Please enable images to view this content" 
                        title="Hero Image"
                        width="560" 
                        style="width: 100%; max-width: 250px; color: #000000; font-size: 13px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;"/>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
                        padding-top: 25px;
                        padding-bottom: 5px;" class="button">
                        @yield('content')
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%;" class="list-item">
                        <table align="center" border="0" cellspacing="0" cellpadding="0" style="width: inherit; margin: 0; padding: 0; border-collapse: collapse; border-spacing: 0;">
                            <tr>
                                <td align="left" valign="top" style="font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                                    padding-top: 25px;
                                    color: #000000;
                                    font-family: sans-serif;" class="paragraph">
                                        @yield('document')
                                </td>

                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
                        padding-top: 25px;" class="line"><hr
                        color="#E0E0E0" align="center" width="100%" size="1" noshade style="margin: 0; padding: 0;" />
                    </td>
                </tr>
                
                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-weight: 400; line-height: 160%;
                        padding-top: 20px;
                        padding-bottom: 25px;
                        color: #000000;
                        font-family: sans-serif;" class="paragraph"> 
                            <div style="color: #127DB3; font-family: sans-serif; font-weight: 400; line-height: 160%;">
                                若有相關問題請來電，黃蘭嵐秘書 0978-794-231 <br>
                                連絡信箱：pata.tw@gmail.com
                            </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;">
                        <a href="http://www.taiwan-pata.org.tw">
                            <img border="0" vspace="0" hspace="0"
                            src="http://www.taiwan-pata.org.tw/img/pata.jpg"
                            alt="Please enable images to view this content" 
                            title="Hero Image"
                            width="560" 
                            style="width: 100%; max-width: 260px; color: #000000; font-size: 13px; margin:0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block; margin-bottom: 30px;"/>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>