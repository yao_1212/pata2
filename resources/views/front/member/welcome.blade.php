@extends('front.layout.master')

@section('content')
    <link rel="stylesheet" href="{{asset("css/album.css")}}">
    <div class="body_bg">
        <div class="content sign-box ">
            <h1>入會申請成功</h1>
            <hr>
            <div class="alert alert-success"style="background-color:#dff0d8 !important;" role="alert">
                <div class="flex middle">
                    <h3 style="color:#388e3c !important;">
                            恭喜，您已成為 <b>台灣動物輔助治療專業發展協會會員</b>。
                    </h3>
                </div>
            </div>
            <p><a href="{{ URL::asset('/login') }}" style="color: #0288d1;">
                    <button class="btn btn-yellow btn-sm">請重新登入會員</button>
                </a></p>



            <div class="info-text" style="margin-top:20px;">
                <div class="info-title">聯絡資訊</div>
                <ul>
                    <li>聯絡電話：(02)2552-5645 </li>
                    <li>通訊地址：112 台北市北投區立農街二段155號 (轉台灣動物輔助治療專業發展協會)</li>
                    <li>台灣動物輔助治療專業發展協會 敬上</li>
                </ul>
            </div>
        </div>
    </div>
    <br><br><br><br>
@endsection