@extends('back.layout.master')
@section('content')
    <div class="container panel flex-3 bg-coffee">
        <h3 class="div_relative">
            各組委員會名單管理
            <div class="inline btn-right">
                <a href="{{URL("admin/commit/create")}}" class="btn btn-success">
                    <i class="fa glyphicon-plus">新增各組委員會名單</i>
                </a>
            </div>
        </h3>
        <hr>
        <table class="table table-striped table-hover" >
            <tr align="center">
                <td>排序</td>
                <td><b>各屆各組委員會</b></td>
                <td><b>新增日期</b></td>
                <td></td>
            </tr>
            @foreach($commits as $commit)
                <tr>
                    <td align="center">{{$commit->rank}}</td>
                    <td align="center">{{$commit->title}}</td>
                    <td align="center">{{$commit->created_at}}</td>
                    <td>
                        <a href="{{ URL('admin/commit/'.$commit->id.'/edit') }}" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span></a>

                        <form class="F_del" action="{{URL("admin/commit/".$commit->id)}}" method="POST" style="display: inline-block;">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                        </form>
                    </td>
                </tr>
            @endforeach

        </table>
    </div>
@endsection

