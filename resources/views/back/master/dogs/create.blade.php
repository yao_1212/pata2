@extends('back.layout.master')
@section('content')
    <script src="{{asset("js/back/btn-action.js")}}"></script>
    <style>
        td{
            height:50px;
            text-align: right;
            padding-right: 20px;;
        }
    </style>
    <div class="container panel flex-3">

        <h3 class="div_relative">
            新增 治療犬
            <div class="inline btn-right">
                <a href="{{URL("admin/master")}}" class="btn btn-info">
                    <i class="fa fa-info-o">回到首頁</i>
                </a>
            </div>
        </h3>
        <hr>
        <form action="{{url($actionUrl)}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div>
                @include("back.errors.error")
            </div>
            <div class="table-section">
                <b>- 治療犬基本資料</b>
                <table width="90%">
                    <tr>
                        <td class="table-title">治療犬主人綽號</td>
                        <td>
                            <input  class="form-control" type="text" name="nickname" value="{{Input::old("name")}}" required>
                        </td>
                        </td>
                        <td class="table-title"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="table-title">治療犬名稱</td>
                        <td colspan="3">
                            <input  class="form-control" type="text" name="name" value="{{Input::old("name")}}" required>
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">晶片號</td>
                        <td colspan="3">
                            <input  class="form-control" type="text" name="number" value="{{Input::old("number")}}" required>
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">性別</td>
                        <td>
                            <select name="sex" id="sex"class="form-control" required>
                                <option value="1" >公</option>
                                <option value="0">母</option>
                            </select>
                        </td>
                        <td class="table-title">品種</td>
                        <td><input  class="form-control" type="text" name="type" value="{{Input::old("type")}}"required></td>
                    </tr>
                    <tr>
                        <td class="table-title">出生年</td>
                        <td><input  class="form-control" type="date" name="birth" value="{{Input::old("birth")}}"required></td>
                        <td class="table-title">首次合格日期</td>
                        <td><input  class="form-control" type="date" name="first_ok" value="{{Input::old("first_ok")}}"required></td>
                    </tr>
                    <tr>
                        <td class="table-title">效期開始</td>
                        <td><input  class="form-control" type="date" name="start_date" value="{{Input::old("start_date")}}"required></td>
                        <td class="table-title">效期結束</td>
                        <td><input  class="form-control" type="date" name="end_date" value="{{Input::old("end_date")}}"required></td>
                    </tr>
                    <tr>
                        <td class="table-title">圖片</td>
                        <td colspan="3">
                            <input  class="form-control" type="file" name="img" value="{{Input::old("img")}}">
                        </td>
                    </tr>
                </table>
            </div>

            <div class="info-text" style="margin-top:20px;">
                <div class="info-title">注意事項</div>
                <ul>
                    <li>3. 建議登入時使用IE11以上版本，網頁方能正常顯示與操作</li>
                </ul>
            </div>
            <div class="form_group" style="padding-right:0px;margin-top:30px;">
                <input type="submit" class="btn btn-success" value="新增治療犬">
            </div>
        </form>
    </div>
    @include("back.component.modify")

@endsection

