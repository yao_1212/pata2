@extends('back.layout.master')
@section('content')
    <script src="{{asset("js/back/btn-action.js")}}"></script>
    <style>
        td{
            height:50px;
            text-align: right;
            padding-right: 20px;;
        }
    </style>
    <div class="container panel flex-3">

        <h3 class="div_relative">
            修改 治療犬
            <div class="inline btn-right">
                <a href="{{URL("admin/master/".$dog->user_id)}}" class="btn btn-info">
                    <i class="fa fa-info-o">回到首頁</i>
                </a>
                <form class="F_del" action="{{URL("admin/master/".$dog->user_id."/dogs/".$dog->dog_id)}}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i>刪除</button>
                </form>
            </div>
        </h3>
        <hr>
        <form action="{{url('admin/master/'.$dog->user_id.'/dogs/'.$dog->dog_id)}}" method="POST" enctype="multipart/form-data">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <div>
                @include("back.errors.error")
            </div>
            <div class="table-section">
                <b>- 治療犬基本資料</b>
                <table width="90%">
                    <tr>
                        <td class="table-title">治療犬主人綽號</td>
                        <td>
                            <input  class="form-control" type="text" name="nickname" value="{{old("nickname",$dog->nickname)}}" required>
                        </td>
                        </td>
                        <td class="table-title"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="table-title">治療犬名稱</td>
                        <td colspan="3">
                            <input  class="form-control" type="text" name="name" value="{{old("name",$dog->name)}}"required>
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">晶片號</td>
                        <td colspan="3">
                            <input  class="form-control" type="text" name="number" value="{{old("number",$dog->number)}}"required>
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">性別</td>
                        <td>
                            <select name="sex" id="sex"class="form-control" >
                                <option value="1" {{ ($dog->sex)==1? 'selected':'' }}>公</option>
                                <option value="0" {{ ($dog->sex)==0? 'selected':'' }}>母</option>
                            </select>
                        </td>
                        <td class="table-title">品種</td>
                        <td><input  class="form-control" type="text" name="type" value="{{old("type",$dog->type)}}"required></td>
                    </tr>
                    <tr>
                        <td class="table-title">出生年</td>
                        <td><input  class="form-control" type="date" name="birth" value="{{old("birth",$dog->getBirth())}}"required></td>
                        <td class="table-title">首次合格日期</td>
                        <td><input  class="form-control" type="date" name="first_ok" value="{{old("first_ok", $dog->getFirst_ok())}}"required></td>
                    </tr>
                    <tr>
                        <td class="table-title">效期開始</td>
                        <td><input  class="form-control" type="date" name="start_date" value="{{old("start_date", $dog->getStart_date())}}"required></td>
                        <td class="table-title">效期結束</td>
                        <td><input  class="form-control" type="date" name="end_date" value="{{old("end_date", $dog->getEnd_date())}}"required></td>
                    </tr>
                    <tr>
                        <td class="table-title">圖片</td>
                        <td colspan="3">
                            <input  class="form-control" type="file" name="img" value="{{Input::old("img")}}">
                            <img src="{{asset('upload/dog/'.$dog->img)}}">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="form_group" style="padding-right:0px;margin-top:30px;">
                <input type="submit" class="btn btn-success btn-block" value="修改資料">
            </div>
        </form>
        <br>
        <br>
        <br>
        <br>
        <br>
        <input type="hidden" name="id" id="id" value="{{$dog->dog_id}}">
        <input type="hidden" name="model" id="model" value="dogs">
        <?php $model_path = 'admin/master/'.$dog->dog_id.'/event_dog_list/'; ?>

        @include('back.master.event_list')
        @include('back.master.select_event')

    </div>


@endsection

