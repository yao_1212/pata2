<div id="modal">
    <table width="100%">
        <tr>
            <td class="table-title">新增參加活動</td>
            <td colspan="3">
                <select name="event_id" id='event_id' class="form-control">
                    <option value="0">請選擇</option>
                    @foreach($events as $event)
                        <option value="{{$event->id}}">
                            {{
                            ( mb_strlen($event->title) > 25 )? mb_substr($event->title,0,25,'UTF8').'...' :$event->title
                            }}
                        </option>
                    @endforeach                    
                </select>
            </td>
        </tr>
        <tr>
            <td class="table-title">時數</td>
            <td colspan="3">
                <input type="number" class="form-control" value="0" id="score" name="score" placeholder="請輸入時數:">                
            </td>
        </tr>
        <td colspan="4" align="center">
            <input type="submit" class="btn btn-success" id="saveEventBtn" value="確定參加活動">
        </td>
    </table>
</div>
<script>
    $(function(){

        $("#modal").iziModal({
            title: '請選擇要參加的活動',
            subtitle: '',
            headerColor: '#88A0B9',
            theme: '',  // light
            appendTo: '.body',
            autoOpen: false,
            closeButton:true
        });
        $(document).on('click', '.trigger', function (event) {
            event.preventDefault();
            $('#modal').iziModal('open');
        });
        $(document).on('click','#saveEventBtn',function(event){
            event.preventDefault();
            var path;
            if($('#model').val() == 'users'){
                path = '/admin/eventList/save';
            }else if($('#model').val() == 'dogs'){
                path = '/admin/eventDogList/save';
            }

            $.post( path, {
                id : $('#id').val(),
                score: $('#score').val(),
                event_id: $('#event_id').val(),
                _token: "{{ csrf_token() }}"
            }).done(function(data){
                $data = JSON.parse(data);
                console.log($data);
                if($data.action == 100){
                    alert('請選擇活動。');
                }else if($data.action == 101){                    
                    alert('已參加過此活動。');
                }else if($data.action == 102){
                    alert('請輸入時數。');                    
                }else if($data.action == 400){
                    alert('找不到此活動。');
                }else if($data.action == 200){
                    alert('成功!');
                    location.reload();
                }else{
                    alert('發生錯誤，請聯絡工程師。');
                }
            });
        });
    })
</script>