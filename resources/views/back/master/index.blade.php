@extends('back.layout.master')
@section('content')
    <div class="container panel flex-3 bg-coffee">
        <h3 class="div_relative">
            會員管理
            <div class="inline btn-right">
                <a href="{{URL("admin/master/create")}}" class="btn btn-success">
                    <i class="fa glyphicon-plus">新增會員</i>
                </a>
            </div>
        </h3>
        <hr>
        <form action="{{url("admin/master")}}" method="get">
            <div class="form-group  col-md-3">
                <input type="text" id="twID" name="twID" class="form-control" placeholder="請輸入身分證字號或姓名">
            </div>
            <input type="submit" class="btn btn-default" value="搜尋">
        </form>
        <table class="table table-striped table-hover" >
            <thead>
            <tr>
                <th>#</th>
                <th>身分證</th>
                <th>姓名</th>
                <th>繳費狀況</th>
                <th>繳費日期</th>
                {{--<th></th>--}}
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)

                <tr>
                    <td>{{ $user->user_id }}</td>
                    <td><a href="{{ URL('admin/master/'.$user->user_id) }}">{{ $user->twID }}</a></td>
                    <td>{{ $user->tw_name }}</td>
                    <td>{!! $user->getIsPay() !!} </td>
                    <td>{{ $user->payTime }}</td>
{{--                    <td><a href="{{URL("admin/master/".$user->user_id)}}" class="btn btn-info"><i class="glyphicon glyphicon-pencil"></i></a></td>--}}

                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $users->links() }}

    </div>

@endsection