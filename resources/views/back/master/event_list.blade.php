<h3>會員已參加活動</h3>
<button type="button" class="trigger btn btn-warning">新增參加活動</button>

<table width="90%" class="table">
    <tr>
        <td style="text-align: left">活動名稱</td>
        <td style="text-align: left">時數</td>
        <td style="text-align: left">新增日期</td>
        <td></td>
    </tr>
    <?php $total = 0;?>
    @foreach($event_list as $list)

        <?php 
        if(!$list->event)
            continue;        
        $total += $list->score;
        ?>
        <form class="F_del"  action="{{url($model_path.$list->id)}}" method="POST">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <tr>
                <td width="500" style="text-align: left">
                    {{ ( mb_strlen($list->event->title) > 25 )? mb_substr($list->event->title,0,25,'UTF8').'...' :$list->event->title}}
                </td>
                <td style="text-align: left; padding-left: 25px;">{{$list->score}}</td>
                <td style="text-align: left;">{{$list->created_at}}</td>
                <td style="text-align: left"><button class="btn btn-danger">刪除</button></td>
            </tr>
        </form>
    @endforeach
    <tr style="border-top:#000">
        <td></td>
        <td style="text-align: left">總時數</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td style="text-align: left;  padding-left: 25px;">{{$total}}</td>
        <td></td>
    </tr>
</table>