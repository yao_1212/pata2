@extends('back.layout.master')
@section('content')
    <script src="{{asset("js/back/btn-action.js")}}"></script>
    <style>
        td{
            height:50px;
            text-align: right;
            padding-right: 20px;;
        }
    </style>
    <div class="container panel flex-3">

        <h3 class="div_relative">
            新增會員
            <div class="inline btn-right">
                <a href="{{URL("admin/master")}}" class="btn btn-info">
                    <i class="fa fa-info-o">回到首頁</i>
                </a>
            </div>
        </h3>
        <hr>
        <form action="{{url($actionUrl)}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div>
                @include("back.errors.error")
            </div>
            <div class="table-section">
                <b>- 基本資料</b>
                <table width="90%">                    
                    <tr>
                        <td class="table-title">中文姓名</td>
                        <td><input class="form-control" type="text" name="tw_name" value="{{Input::old("tw_name")}}"></td>
                        <td class="table-title">英文姓名</td>
                        <td><input class="form-control"  type="text" name="en_name" value="{{Input::old("en_name")}}"></td>
                    </tr>
                    <tr>
                        <td class="table-title">身分證字號</td>
                        <td><input  class="form-control" type="text" name="twID" value="{{Input::old("twID")}}"></td>
                        <td class="table-title">生日</td>
                        <td><input  class="form-control" type="date" name="birth" value="{{Input::old("birth")}}"></td>
                    </tr>
                    <tr>
                        <td class="table-title">性別</td>
                        <td>
                            <select name="gender" id="gender"class="form-control" >
                                <option value="1" >男</option>
                                <option value="0">女</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">通訊地址</td>
                        <td colspan="3">
                            <input class="form-control"  type="text" name="addr1" value="{{Input::old("addr1")}}">
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">住家電話</td>
                        <td><input  class="form-control" type="text" name="phone1" value="{{Input::old("phone1")}}"></td>
                        <td class="table-title">手機</td>
                        <td><input class="form-control"  type="text" name="mphone" value="{{Input::old("mphone")}}"></td>
                    </tr>
                    <tr>
                        <td class="table-title">E-mail</td>
                        <td colspan="3">
                            <input  class="form-control" type="email" name="email" value="{{Input::old("email")}}">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="table-section">
                <b>- 學歷 / 經歷 / 服務單位</b>
                <table width="90%">
                    <tr>
                        <td class="table-title">學歷</td>
                        <td colspan="3">
                            <select name="degree" id="gender"class="form-control" >
                                <option value="1" {{ (Input::old("degree") == "1" ? "selected":"") }}>學士</option>
                                <option value="2" {{ (Input::old("degree") == "2" ? "selected":"") }}>學士後靜修</option>
                                <option value="3" {{ (Input::old("degree") == "3" ? "selected":"") }}>碩士</option>
                                <option value="4" {{ (Input::old("degree") == "4" ? "selected":"") }}>博士</option>
                                <option value="5" {{ (Input::old("degree") == "5" ? "selected":"") }}>研究員</option>
                                <option value="6" {{ (Input::old("degree") == "6" ? "selected":"") }}>助理教授</option>
                                <option value="7" {{ (Input::old("degree") == "7" ? "selected":"") }}>教授</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">畢業學校</td>
                        <td colspan="3">
                            <input  class="form-control" type="text" name="school" value="{{Input::old("school")}}">
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">服務單位</td>
                        <td colspan="3">
                            <input  class="form-control" type="text" name="office" value="{{Input::old("office")}}">
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">服務單位電話</td>
                        <td><input  class="form-control" type="text" name="ophone" value="{{Input::old("ophone")}}"></td>
                        <td class="table-title">傳真</td>
                        <td><input  class="form-control" type="text" name="fax" value="{{Input::old("fax")}}"></td>
                    </tr>
                </table>
            </div>
            {{--<div class="table-section">--}}
                {{--<b>- 治療犬基本資料</b>--}}
                {{--<table width="90%">--}}
                    {{--<tr>--}}
                        {{--<td class="table-title">治療犬名稱</td>--}}
                        {{--<td colspan="3">--}}
                            {{--<input  class="form-control" type="text" name="dog_name" value="{{Input::old("dog_name")}}">--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="table-title">晶片號</td>--}}
                        {{--<td colspan="3">--}}
                            {{--<input  class="form-control" type="text" name="dog_id" value="{{Input::old("dog_id")}}">--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="table-title">性別</td>--}}
                        {{--<td>--}}
                            {{--<select name="dog_sex" id="dog_sex"class="form-control" >--}}
                                {{--<option value="1" >公</option>--}}
                                {{--<option value="0">母</option>--}}
                            {{--</select>--}}
                        {{--</td>--}}
                        {{--<td class="table-title">品種</td>--}}
                        {{--<td><input  class="form-control" type="text" name="dog_type" value="{{Input::old("dog_type")}}"></td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="table-title">出生年</td>--}}
                        {{--<td><input  class="form-control" type="date" name="dog_birth" value="{{Input::old("dog_birth")}}"></td>--}}
                        {{--<td class="table-title">首次合格日期</td>--}}
                        {{--<td><input  class="form-control" type="date" name="dog_first_ok" value="{{Input::old("dog_first_ok")}}"></td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="table-title">效期開始</td>--}}
                        {{--<td><input  class="form-control" type="date" name="dog_start_date" value="{{Input::old("dog_start_date")}}"></td>--}}
                        {{--<td class="table-title">效期結束</td>--}}
                        {{--<td><input  class="form-control" type="date" name="dog_end_date" value="{{Input::old("dog_end_date")}}"></td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="table-title">圖片</td>--}}
                        {{--<td colspan="3">--}}
                            {{--<input  class="form-control" type="file" name="dog_img" value="{{Input::old("dog_img")}}">--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                {{--</table>--}}
            {{--</div>--}}

            <div class="info-text" style="margin-top:20px;">
                <div class="info-title">注意事項</div>
                <ul>
                    <li>1. 密碼預設為身分證字號</li>
                    <li>3. 建議登入時使用IE11以上版本，網頁方能正常顯示與操作</li>
                </ul>
            </div>
            <div class="form_group" style="padding-right:0px;margin-top:30px;">
                <input type="submit" class="btn btn-success" value="儲存後，新增治療犬">
            </div>
        </form>
    </div>
    @include("back.component.modify")

@endsection

