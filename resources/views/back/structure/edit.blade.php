@extends('back.layout.master')
@section('content')
    <script src="{{asset("js/back/btn-action.js")}}"></script>

    <div class="container panel flex-3 bg-coffee">

        <h3 class="div_relative">
            理監事名單修改
            <div class="inline btn-right">
                <a href="{{URL("admin/structure")}}" class="btn btn-info">
                    <i class="fa fa-info-o">回到首頁</i>
                </a>
                <form class="F_del" action="{{URL("admin/structure/".$structures->id)}}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i>刪除</button>
                </form>
            </div>
        </h3>
        <hr>

        <?php
        $staff = json_decode($structures->staff);
        ?>
        <div class="invoice" style="min-height: 600px">
            <form action="{{ url('admin/structure/'.$structures->id) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <table class="table">
                <tr>

                    <td><b>名單標題</b>
                        <div class="input-group-lg">
                            <input required type="text" placeholder="請輸入名單標題" name="title" id="title" class="form-control" value="{{$structures->title}}"/>
                        </div>
                        <b>排序 <span style="color:#ef5350 ">   *數字越大越前面</span></b>
                        <div class="input-group-lg">
                            <input required type="number"  id="rank" name="rank" class="form-control" value="{{$structures->rank}}"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>理事長</b>
                        <div class="input-group-lg">
                            <div class="col-md-5">
                                姓名
                                <input required type="text"  name="name1" id="name1" class="form-control" value="{{$staff[0][0]}}"/>
                            </div>

                            <div class="col-md-5">
                                服務單位
                                <input required type="text"  name="office1" id="office1" class="form-control" value="{{$staff[0][1]}}"/>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td id="2">
                        <b>常務理事</b> <a class="btn btn-info" id="add_2">新增</a>
                        @foreach($staff[1] as $people)
                            <div class="input-group-lg">
                                <div class="col-md-5">
                                    姓名
                                    <input required type="text"  name="name2[]" id="name2[]" class="form-control" value="{{ $people[0]}}"/>
                                </div>

                                <div class="col-md-5">
                                    服務單位
                                    <input required type="text"  name="office2[]" id="office2[]" class="form-control" value="{{ $people[1]}}"/>
                                </div>
                                <div class="col-md-2"><a class="del btn btn-danger">刪除</a></div>
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td id="3">
                        <b>理事</b>
                        <a class="btn btn-info" id="add_3">新增</a>
                        @foreach($staff[2] as $people)
                            <div class="input-group-lg">
                                <div class="col-md-5">
                                    姓名
                                    <input required type="text"  name="name3[]" id="name3[]" class="form-control" value="{{ $people[0]}}"/>
                                </div>

                                <div class="col-md-5">
                                    服務單位
                                    <input required type="text"  name="office3[]" id="office3[]" class="form-control" value="{{ $people[1]}}"/>
                                </div>
                                <div class="col-md-2"><a class="del btn btn-danger">刪除</a></div>
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td id="4">
                        <b>常務監事</b> <a class="btn btn-info" id="add_4">新增</a>
                        @foreach($staff[3] as $people)
                            <div class="input-group-lg">
                                <div class="col-md-5">
                                    姓名
                                    <input required type="text"  name="name4[]" id="name4[]" class="form-control" value="{{ $people[0]}}"/>
                                </div>

                                <div class="col-md-5">
                                    服務單位
                                    <input required type="text"  name="office4[]" id="office4[]" class="form-control" value="{{ $people[1]}}"/>
                                </div>
                                <div class="col-md-2"><a class="del btn btn-danger">刪除</a></div>
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td id="5">
                        <b> 監事</b> <a class="btn btn-info" id="add_5">新增</a>
                        @foreach($staff[4] as $people)
                            <div class="input-group-lg">
                                <div class="col-md-5">
                                    姓名
                                    <input required type="text"  name="name5[]" id="name5[]" class="form-control" value="{{ $people[0]}}"/>
                                </div>

                                <div class="col-md-5">
                                    服務單位
                                    <input required type="text"  name="office5[]" id="office5[]" class="form-control" value="{{ $people[1]}}"/>
                                </div>
                                <div class="col-md-2"><a class="del btn btn-danger">刪除</a></div>
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td id="6">
                        <b>秘書長</b> <a class="btn btn-info" id="add_6">新增</a>
                        @foreach($staff[5] as $people)
                            <div class="input-group-lg">
                                <div class="col-md-5">
                                    姓名
                                    <input required type="text"  name="name6[]" id="name6[]" class="form-control" value="{{ $people[0]}}"/>
                                </div>

                                <div class="col-md-5">
                                    服務單位
                                    <input required type="text"  name="office6[]" id="office6[]" class="form-control" value="{{ $people[1]}}"/>
                                </div>
                                <div class="col-md-2"><a class="del btn btn-danger">刪除</a></div>
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="確定新增" class="btn btn-success btn-lg"/>
                        <a href="{{ URL('admin/structure') }}" class="btn btn-danger  btn-lg">取消新增</a>
                    </td>
                </tr>
            </table>
            </form>
            <div id="clone2" style="display: none">
                <div id="clone2_content">
                    <div class="col-md-5">
                        姓名
                        <input required type="text"  name="name2[]" id="name2[]" class="form-control" value=""/>
                    </div>

                    <div class="col-md-5">
                        服務單位
                        <input required type="text"  name="office2[]" id="office2[]" class="form-control" value=""/>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function(){

                $( document ).on( "click", ".del", function() {
                    $(this).parents('.input-group-lg').remove();  // jQuery 1.7+
                });
                $("#add_2").click(function(){
                    $("#2").append('<div class="input-group-lg">' +
                            '<div class="col-md-5">' +
                            '姓名<input required type="text"  name="name2[]" id="name2[]" class="form-control" value=""/>' +
                            '</div>' +
                            '<div class="col-md-5">服務單位' +
                            '<input required type="text"  name="office2[]" id="office2[]" class="form-control" value=""/>' +
                            ' </div><div class="col-md-2"><a class="del btn btn-danger">刪除</a></div></div>');
                })
                $("#add_3").click(function(){
                    $("#3").append('<div class="input-group-lg">' +
                            '<div class="col-md-5">' +
                            '姓名<input required type="text"  name="name3[]" id="name3[]" class="form-control" value=""/>' +
                            '</div>' +
                            '<div class="col-md-5">服務單位' +
                            '<input required type="text"  name="office3[]" id="office3[]" class="form-control" value=""/>' +
                            ' </div><div class="col-md-2"><a class="del btn btn-danger">刪除</a></div></div>');
                })
                $("#add_4").click(function(){
                    $("#4").append('<div class="input-group-lg">' +
                            '<div class="col-md-5">' +
                            '姓名<input required type="text"  name="name4[]" id="name4[]" class="form-control" value=""/>' +
                            '</div>' +
                            '<div class="col-md-5">服務單位' +
                            '<input required type="text"  name="office4[]" id="office4[]" class="form-control" value=""/>' +
                            ' </div><div class="col-md-2"><a class="del btn btn-danger">刪除</a></div></div>');
                })
                $("#add_5").click(function(){
                    $("#5").append('<div class="input-group-lg">' +
                            '<div class="col-md-5">' +
                            '姓名<input required type="text"  name="name5[]" id="name5[]" class="form-control" value=""/>' +
                            '</div>' +
                            '<div class="col-md-5">服務單位' +
                            '<input required type="text"  name="office5[]" id="office5[]" class="form-control" value=""/>' +
                            ' </div><div class="col-md-2"><a class="del btn btn-danger">刪除</a></div></div>');
                })
                $("#add_6").click(function(){
                    $("#6").append('<div class="input-group-lg">' +
                            '<div class="col-md-5">' +
                            '姓名<input required type="text"  name="name6[]" id="name6[]" class="form-control" value=""/>' +
                            '</div>' +
                            '<div class="col-md-5">服務單位' +
                            '<input required type="text"  name="office6[]" id="office6[]" class="form-control" value=""/>' +
                            ' </div><div class="col-md-2"><a class="del btn btn-danger">刪除</a></div></div>');
                })
            })
        </script>
    </div>
    @include("back.component.modify")

@endsection

