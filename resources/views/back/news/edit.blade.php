@extends('back.layout.master')
@section('content')
    <script src="{{asset("js/back/btn-action.js")}}"></script>

    <div class="container panel flex-3 bg-coffee">

        <h3 class="div_relative">
            {{$yvtset->labelname}}修改
            <div class="inline btn-right">
                <a href="{{URL("admin/yvtset/".$yvtset->getTable())}}" class="btn btn-info">
                    <i class="fa fa-info-o">回到首頁</i>
                </a>
                <form class="F_del" action="{{URL("admin/yvtset/".$yvtset->getTable()."/".$id)}}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i>刪除</button>
                </form>
            </div>
        </h3>
        <hr>

        <form action="{{URL("admin/yvtset/".$yvtset->getTable()."/".$id)}}" method="POST" enctype="multipart/form-data">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
        <table class="table">
            @include("back.component.master")
            <tr>
                <td colspan="2">
                    <input type="submit" class="btn btn-success btn-block" value="修改">
                </td>
            </tr>
        </table>

        </form>
    </div>
    @include("back.component.modify")

@endsection

