@extends('back.layout.master')
@section('content')

    <div class="container panel flex-3 bg-coffee">

        <h3 class="div_relative">
            {{$yvtset->labelname}}-新增
            <div class="inline btn-right">
                <a href="{{URL("admin/yvtset/".$yvtset->getTable())}}" class="btn btn-info">
                    <i class="fa fa-info-o">回到首頁</i>
                </a>
            </div>
        </h3>
        <hr>
        <form action="{{URL("admin/yvtset/".$yvtset->getTable())}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <table class="table">
                @include("back.component.master")
            </table>
            <br><br><br><br>
            <input type="submit" class="btn btn-success btn-block" value="新增">
        </form>
    </div>
@endsection

