<tr>
    <td>
        {{$column["label"]}}
    </td>
    <td>
        <textarea name="{{$name}}" class="{{$class}}" placeholder="{{$column["placeholder"]}}" {{$required}}>{{$value}}</textarea>
    </td>
</tr>

<script>
    CKEDITOR.replace( '.ckeditor' );
</script>