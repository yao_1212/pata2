<tr class="table-height">
    <td class="table-title">
        {{$column["label"]}}
    </td>
    <td>
        <input type='text' name="{{$name}}" class="{{$class}}" value="{{$value}}" placeholder="{{$column["placeholder"]}}" {{$required}}/>
    </td>
</tr>