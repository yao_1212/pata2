<tr>
    <td>
        {{$column["label"]}}
    </td>
<td>
    <select name="{{$name}}"  class="{{$class}}">
        @foreach ($column["editarray"] as $option)
            <?php
                $selected = ($value == $option["value"])? "selected" : "";
            ?>
            <option value="{{$option["value"]}}" {{$selected}}>{{$option["text"]}}</option>
        @endforeach
    </select>
    </td>
</tr>