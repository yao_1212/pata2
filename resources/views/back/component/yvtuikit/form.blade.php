

@include("back.component.master")
@if(isset($p_key))
    <input type="hidden" name="p_key" id="p_key" value="{{$p_key}}">
@endif

<input type="hidden" name="yvtset" id="yvtset" value="{{$yvtset->getTable()}}">
<footer>
    <button data-iziModal-close>取消</button>
    <button class="btn btn-info rel-btn" id="">確認新增</button>
</footer>
<script>

    $(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".rel-btn").click(function(e){
//            var $data = $('#rel-form').serialize();
            var $data = new FormData($('#rel-form')[0]);
            var yvtset = $("#yvtset").val();
            var $url = "/ajax/yvtset/"+yvtset;

            $.ajax({
                mimeType:"multipart/form-data",
                url:$url,
                data:$data,
                cache: false,
                dataType:'json',
                processData: false, // Don't process the files
                contentType: false,
                type:"POST",
                success:function(data){
                    location.reload();
                }
            });
            return false;
        })
    })
</script>
