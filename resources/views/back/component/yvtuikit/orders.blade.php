@foreach($yvtset["data"] as $data)
    <div class="yvt-orders div_relative">
        <i class="fa fa-2x fa-angle-double-down open-box" aria-hidden="true"></i>
        <i class="fa fa-2x fa-angle-double-up close-box" aria-hidden="true"></i>

    @foreach($yvtset["table_columns"] as $name => $column)

        <?php

        $value = $data->$name;

        $url_photo_flag = 0;

        if($yvtset["table_val"]["edittype"] == $column["edittype"]){
            $url_photo_flag = 1;
        }



        $required = "";
        if(isset($column["required"])){
            $required = "required";
        }

        //要在上面給參數設定
        $class = "form-control ";

        ?>
        @if($url_photo_flag)
        <div class="orders-img-wrap col-3">
            <img src="{{ $data->$name }}"/>
        </div>
        @endif
        <div class="orders-content-wrap col-7">
            <table width="100%">
                @if($column["edittype"]=="input")
                    @include("back.component.input")
                @elseif($column["edittype"]=="textarea")
                    @include("back.component.textarea")
                @elseif($column["edittype"]=="select")
                    @include("back.component.select")
                @elseif($column["edittype"]=="file")
                    @include("back.component.file")
                @elseif($column["edittype"]=="ckeditor")
                    @include("back.component.ckeditor")
                @endif
            </table>
        </div>
    @endforeach
    </div>
@endforeach
<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>

<script>
    $(function(){
        $(".close-box").click(function(){
            var yvt_orders = $(this).parent(".yvt-orders");
            yvt_orders.find(".table-height").hide();
            $(this).hide();
            yvt_orders.find(".open-box").show();
            yvt_orders.find(".table-height:first").show();
        })
        $(".open-box").click(function(){
            var yvt_orders = $(this).parent(".yvt-orders");
            yvt_orders.find(".table-height").show();
            $(this).hide();
            yvt_orders.find(".close-box").show();
        })



    })
</script>