<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<?php $idx = $yvtset_arr["table_val"]["idx"]; ?>


<form id="rel-form" enctype="multipart/form-data" >
<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
<input type="hidden" name="{{$yvtset_arr["table_val"]["idx"]}}" value="{{$yvtset->$idx}}">
<div id="modal-custom" data-iziModal-group="{{$table_name}}">
    <button data-iziModal-close class="icon-close">x</button>
    <header>
        <h4 href="" class="title">新增</h4>
    </header>
    <section id="form">

    </section>
</div>

</form>

<script>
    $(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".save-btn").click(function(){
            var order_header  = $(this).parents(".content-wrap-header");
            var order_content = $(this).parents(".order-content");

            var drag          = order_content.find(".drag");
            var enable_btn    = order_content.find(".order-enable-btn");
            var notify_btn    = order_content.find(".notify-btn");

            drag.sortable({disabled:true});

            drag.removeClass("drag-enable");

            notify_btn.removeClass("hide");
            enable_btn.addClass("hide");
            $(this).addClass("hide");

            var $data = new FormData($('#order-form')[0]);
            var yvtset = $("#yvtset").val();
            var $url = "/ajax/yvtset/<?php echo $yvtset_arr["table"]?>/order";

            $.ajax({
                url:$url,
                data:$data,
                cache: false,
                dataType:'json',
                processData: false, // Don't process the files
                contentType: false,
                type:"POST",
                success:function(data){
                    console.log(data);
                }
            });
            return false;
        });

        //按下編輯順序
        $(".notify-btn").click(function(){
            var order_header  = $(this).parents(".content-wrap-header");
            var order_content = $(this).parents(".order-content");

            var drag          = order_content.find(".drag");
            var enable_btn    = order_header.find(".order-enable-btn");
            var save_btn      = order_content.find(".save-btn");


            save_btn.removeClass("hide");
            enable_btn.removeClass("hide");

            $(this).addClass("hide");
            drag.addClass("drag-enable");

            drag.sortable({
                disabled: false,
                scroll: true,
                revert: true,
                cursor: "move",
                placeholder: "order-selected",
                activate:function( event, ui ) {
                    ui.item.addClass("num-selected-border");
                    ui.item.find(".num-wrap").addClass("num-selected");
                },
                beforeStop: function( event, ui ) {
                    ui.item.removeClass("num-selected-border");
                    ui.item.find(".num-wrap").removeClass("num-selected");

                }
            });

            $( "#sortable" ).disableSelection();

        });



        //按鈕提示
        $(".notify-btn").hover(function(e){
            var notify = $(this).parent().find(".notify-sm");
            notify.removeClass("hide");

        },function(){
            var parent = $(this).parent();
            var notify = $(this).parent().find(".notify-sm");
            notify.addClass("hide");
        })



    })
</script>
<div class="body-wrap">

    {{-- order-content div start --}}
    <div class="order-content">
        <div class="content-wrap-header flex end-right">
            <div class="notify">
                <button class="my-btn edit-bg notify-btn">
                    <div class="notify-sm edit-bd edit-color hide">
                        更動呈現於頁面的順序
                    </div>
                    <i class="fa fa-pencil-square-o edit-icon" aria-hidden="true"></i>
                    更動順序
                </button>
            </div>
            <div class="my-btn order-temp-color white-bg order-temp-border order-enable-btn hide">
                更動順序中
            </div>
        </div>

        {{--drag--}}
        <form id="order-form">
        <div class="drag" id="sortable">
            @foreach($yvtset_arr["data"] as $key => $data)
                <?php
                foreach($yvtset_arr["table_columns"] as $column_name => $column){
                    if(isset($column["editTitle"]) && $column["editTitle"]){
                        $title = $data->$column_name;
                    }
                    else if($column["edittype"]=="url_photo"){
                        $photo = $data->$column_name;
                    }
                }

                ?>
            <div class="order-item">
                <div class="num-wrap num-bg center middle ">
                    <div class="num">
                        {{$key+1}}
                        <input type="hidden" name="p_key[]" value="{{$data->$p_key}}"/>
                    </div>
                </div>
                <div class="img-wrap flex middle">
                    @if(isset($photo))
                    <img class="img" src="{{URL("upload/".$yvtset_arr["table"]."/".$photo)}}" alt="">
                    @else
                    <img class="img" src="http://www.tainan-hch.com.tw/site/themes/default/cht/images/nopic.jpg" alt="">
                    @endif
                </div>
                <div class="content-wrap center middle">
                    <div class="content-wrap-title">
                        <div class="title">
                            {{--{{$title}}--}}
                        </div>
                    </div>
                    <div class="edit">
                        <div class="circle-wrap edit-btn modal-btn" data-izimodal-open="modal-custom" yvtset="{{$table_name}}" data="{{$data->$p_key}}">
                            <div class="circle" align="center">
                                <i class="fa fa-pencil-square-o fa-2x edit-icon" aria-hidden="true"></i>
                                <span class="edit-text">編輯</span>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            @endforeach



        </div>
        {{--end drag--}}
        </form>
        <div class="content-wrap-footer flex end-right">
            <button class="my-btn order-temp-bg my-btn-md save-btn hide">儲存</button>
        </div>
    </div>
    {{--end order div --}}
</div>
{{--@foreach($yvtset_arr["data"] as $data)--}}
{{--<div class="order-content">--}}
    {{--@foreach($yvtset_arr["table_columns"] as $column_name => $column)--}}
        {{--@if($column["edittype"]=="url_photo")--}}
        {{--<div class="orders-img-wrap">--}}
            {{--<img src="{{URL("upload/".$yvtset_arr["table"]."/".$data->$column_name)}}" alt="">--}}
        {{--</div>--}}
        {{--@endif--}}
        {{--@if(isset($column["editTitle"])&& $column["editTitle"] )--}}
        {{--<div class="orders-content-wrap">--}}
            {{--{{$data}}--}}
        {{--</div>--}}
        {{--@endif--}}
    {{--@endforeach--}}
{{--</div>--}}

{{--@endforeach--}}
<script>
    $(function(){
            var AJAX_URL;
            $("#modal-custom").iziModal({
                overlayClose: false,
                overlay: false,
                width: 600,
                autoOpen: false,
                overlayColor: 'rgba(0, 0, 0, 0.6)',
                onOpened: function(modal) {
                    modal.startLoading();
//                    console.log(modal);
//                    console.log(DATA_ID);
                    var yvtset = modal.group.name;
                    var id = DATA_ID;
                    console.log(id);
                    if(typeof id === "undefined"){
                        $url = "/ajax/yvtset/"+yvtset;
                    }
                    else{
                        $url = "/ajax/yvtset/"+yvtset+"/"+id;
                    }
                    $.get($url ,function(data){
//                    console.log(data);
                        $("#form").html(data);
                        modal.stopLoading();
                    });
                },
                onClosed: function() {
                    //關掉之後把html清空
                    $("#form").html(" ");
                }
            });
    })
</script>