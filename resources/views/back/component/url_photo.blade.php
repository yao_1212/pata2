<tr>
    <td>
        {{$column["label"]}}

    </td>
    <td>
        <input type="file" name="{{$name}}" class="{{$class}}" value="{{$value}}"/>
    </td>
    <td>
        <span class="file-note">{{$column["file_note"]}}</span>
    </td>
</tr>
@if($value)
    <tr>
        <td></td>

        <td>
        <div class="img-wrap flex middle">
            <img class="img" src="{{ URL( "upload/".$yvtset->getTable()."/".$value )}}" alt="">
        </div>
    </td>
    </tr>
@endif