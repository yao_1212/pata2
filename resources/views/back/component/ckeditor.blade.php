{{--class `ckeditor`--}}
<tr>
    <td>
        {{$column["label"]}}
    </td>
    <td>
        <textarea name="{{$name}}" class="{{$class}} ckeditor" placeholder="{{$column["placeholder"]}}" {{$required}}>{{$value}}</textarea>
    </td>
</tr>

