<tr>
    <td>
        {{$column["label"]}}
    </td>
    <td>

        @if($value)
            <a href="{{url("file/".$yvtset->getTable()."/".$name."/".$yvtset->id)}}"><i class="fa fa-file" aria-hidden="true"></i>{{$yvtset->filename}}</a>
        @endif
            <input type="file" name="{{$name}}" class="{{$class}}" value="{{$value}}"/>
            <span class="file-note">{{$column["file_note"]}}</span>
    </td>
</tr>