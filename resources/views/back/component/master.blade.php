<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

@foreach($columns as $name => $column)
    <?php

    $value = $yvtset->$name;
    if($column["edittype"] == "disabled"){

    }
    if( !isset($column["placeholder"]) ){
        $column["placeholder"] = "";
    }
    $required = "";
    if(isset($column["required"]) && $column["required"]){
        $required = "required";
    }

    //要在上面給參數設定
    $class = "form-control ";

    //應該要把 tr td 也寫到"Form/InputType"，為了以後新的客製化
    ?>

    @if($column["edittype"]=="input")
        @include("back.component.input")
    @elseif($column["edittype"]=="number")
        @include("back.component.number")
    @elseif($column["edittype"]=="date")
        @include("back.component.date")
    @elseif($column["edittype"]=="textarea")
        @include("back.component.textarea")
    @elseif($column["edittype"]=="select")
        @include("back.component.select")
    @elseif($column["edittype"]=="file")
        @include("back.component.file")
    @elseif($column["edittype"]=="ckeditor")
        @include("back.component.ckeditor")
    @elseif($column["edittype"]=="url_photo")
        @include("back.component.url_photo")
    @endif


    {{--                {{$news->createAndModify($column,$key,$news->$key)}}--}}
@endforeach