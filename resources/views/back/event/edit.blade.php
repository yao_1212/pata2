@extends('back.layout.master')
@section('content')
    <script src="{{asset("js/back/btn-action.js")}}"></script>
    <style>
        td{
            height:50px;
            text-align: right;
            padding-right: 20px;;
        }
    </style>
    <div class="container panel flex-3 bg-coffee">

        <h3 class="div_relative">
            活動修改
            <div class="inline btn-right">

                <a href="{{URL("admin/".$yvtset->getTable())}}" class="btn btn-info">
                    <i class="fa fa-info-o">回到首頁</i>
                </a>
                <form class="F_del" action="{{URL("admin/yvtset/".$yvtset->getTable()."/".$yvtset->id)}}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i>刪除</button>
                </form>
                <form class="F_copy" action="{{URL("admin/yvtset/".$yvtset->getTable()."/".$yvtset->id."/copy")}}" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-warning">複製此活動內容</button>
                </form>
            </div>
        </h3>
        <hr>
        <form action="{{URL("admin/event/".$yvtset->id)}}" method="POST" enctype="multipart/form-data">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <table class="table">
                @include("back.component.master")
            </table>
            <br><br><br><br>
            <input type="submit" class="btn btn-success btn-block" value="新增">
        </form>
    </div>
    @include("back.component.modify")

@endsection

