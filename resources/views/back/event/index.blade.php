@extends('back.layout.master')
@section('content')
    <div class="container panel flex-3 bg-coffee">
        <h3 class="div_relative">
            活動管理
            <div class="inline btn-right">
                <a href="{{URL("admin/event/create")}}" class="btn btn-success">
                    <i class="fa glyphicon-plus">新增活動</i>
                </a>
            </div>
        </h3>
        <hr>
        <form action="{{url("admin/event")}}" method="get">
            <div class="form-group  col-md-3">
                <input type="text" id="twID" name="twID" class="form-control" placeholder="請輸入身分證字號或姓名">
            </div>
            <input type="submit" class="btn btn-default" value="搜尋">
        </form>
        <table class="table table-striped table-hover" >
            <thead>
            <tr>
                <td align="center">#</td>
                <td>活動標題</td>
                <td>活動型態</td>
                <td align="center">聯絡人</td>
                <td align="center">參與人數</td>
                <td>發佈日期</td>
            </tr>
            </thead>
            <tbody>
            @foreach($events as $event)
            <tr>
                <td>{{ $event->id }}</td>
                <td>
                    <a href="{{url('admin/event/'.$event->id)}}">
                        {{ ( mb_strlen($event->title) > 20 )? mb_substr($event->title,0,18,'UTF8').'...' :$event->title}}
                    </a>
                </td>
                <td>{{ $event->getType() }}</td>
                <td align="center">{{ $event->admin }}</td>
                <td align="center"><a href="{{url('admin/event/'.$event->id.'/list')}}">{{ $event->total }}</a></td>
                <td>{{ date_format($event->created_at,'Y-m-d') }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
        {{ $events->links() }}

    </div>

@endsection