@extends('back.layout.master')
@section('content')
    <div class="container panel flex-3 bg-coffee">
        <h2 class="div_relative">
            {{$events->title}} 
            <small> - 參加活動列表</small>
            <div class="inline btn-right">
                <a href="{{URL("admin/event")}}" class="btn btn-success">
                    回到上一頁
                </a>
            </div>
        </h2>
        <br>

        <h4>- 治療犬參加名單</h4>
        <table class="table table-striped table-hover" >
            <thead>
            <tr>
                <td align="center">#</td>
                <td>治療犬名稱</td>
                <td>主人綽號</td>
                <td align="center">參加時數</td>
                <td>主人全名</td>
                <td>主人手機</td>
            </tr>
            </thead>
            <tbody>
            @foreach($events->dogList as $index => $dog)
                <tr>
                    <td align="center">{{ $index+1 }}</td>
                    <td><a href="{{url('admin/master/'.$dog->user->user_id.'/dogs/'.$dog->dog_id)}}">{{ $dog->name }}</a></td>
                    <td>{{ $dog->nickname }}</td>
                    <td align="center">{{ $dog->pivot->score }}</td>
                    <td>{{ $dog->user->tw_name }}</td>
                    <td>{{ $dog->user->mphone }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br>
        <h4><br> - 參加活動清單</h4>
        <hr>
        <table class="table table-striped table-hover" >
            <thead>
            <tr>
                <td align="center">#</td>
                <td>姓名</td>
                <td align="center">參加時數</td>
                <td>服務單位</td>
                <td>手機</td>
            </tr>
            </thead>
            <tbody>
            @foreach($events->userList as $index => $user)
                <tr>
                    <td align="center">{{ $index+1 }}</td>
                    <td><a href="{{url('admin/master/'.$user->id)}}">{{ $user->tw_name }}</a></td>
                    <td align="center">{{ $user->pivot->score }}</td>
                    <td>{{ $user->office }}</td>
                    <td>{{ $user->mphone }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br><br>
    </div>

@endsection