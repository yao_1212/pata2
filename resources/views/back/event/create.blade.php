@extends('back.layout.master')
@section('content')
    <script src="{{asset("js/back/btn-action.js")}}"></script>
    <style>
        td{
            height:50px;
            text-align: right;
            padding-right: 20px;;
        }
    </style>
    <div class="container panel flex-3">

        <h3 class="div_relative">
            新增活動
            <div class="inline btn-right">
                <a href="{{URL("admin/user")}}" class="btn btn-info">
                    <i class="fa fa-info-o">回到首頁</i>
                </a>
            </div>
        </h3>
        <hr>
        <form action="{{URL("admin/event")}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <table class="table">
                @include("back.component.master")
            </table>
            <br><br><br><br>
            <input type="submit" class="btn btn-success btn-block" value="新增">
        </form>
    </div>
    @include("back.component.modify")

@endsection

