
@if (count($errors) > 0)
    <div class="alert alert-danger error-list">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif
<script>
    $(function(){
        $(".error-list").click(function (){
            $(this).addClass("hide");
        })
    })
</script>