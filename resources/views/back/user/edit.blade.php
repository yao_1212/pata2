@extends('back.layout.master')
@section('content')
    <script src="{{asset("js/back/btn-action.js")}}"></script>
    <style>
        td{
            height:50px;
            text-align: right;
            padding-right: 20px;;
        }
    </style>
    <div class="container panel flex-3 bg-coffee">

        <h3 class="div_relative">
            動輔師修改
            <div class="inline btn-right">
                <form class="F_check" action="{{url("admin/master/".$user->id."/check")}}" method="post">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-check"></span>確認繳費</button>
                </form>
                <a href="{{URL("admin/master")}}" class="btn btn-info">
                    <i class="fa fa-info-o">回到首頁</i>
                </a>
                <form class="F_del" action="{{URL("admin/yvtset/".$user->getTable()."/".$user->id)}}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i>刪除</button>
                </form>
            </div>
        </h3>
        <hr>

        <form action="{{URL("admin/master/".$user->id)}}" method="POST" enctype="multipart/form-data">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            @include("errors.error")

            <div class="table-section">
                <b>- 基本資料</b>
                <table width="90%">
                    <tr>
                        <td class="table-title">繳費狀況</td>
                        <td style="text-align: left;">{!! $user->getIsPay() !!} </td>
                        <td class="table-title">繳費日期</td>
                        <td style="text-align: left;">{{$user->payTime}}</td>
                    </tr>
                    <tr>
                        <td class="table-title">中文姓名</td>
                        <td><input  type="text" class="form-control" name="tw_name" value="{{ old('tw_name', $user->tw_name) }}"></td>
                        <td class="table-title">英文姓名</td>
                        <td><input  type="text" class="form-control" name="en_name" value="{{ old('en_name',$user->en_name)}}"></td>
                    </tr>
                    <tr>
                        <td class="table-title">身分證字號</td>
                        <td><input  type="text" class="form-control" value="{{ old('twID',$user->twID)}}" disabled ></td>
                        <td class="table-title">生日</td>
                        <td><input  type="date" class="form-control" name="birth" value="{{ old('birth', $user->birth) }}"></td>
                    </tr>
                    <tr>
                        <td class="table-title">性別</td>
                        <td>
                            <select name="gender" id="gender"class="form-control">
                                <option value="1" >男</option>
                                <option value="0" >女</option>
                            </select>
                        </td>
                        <td class="table-title">綽號</td>
                        <td><input class="form-control"  type="text" name="nickname" value="{{Input::old("nickname", $user->nickname)}}"></td>
                    </tr>
                    <tr>
                        <td class="table-title">通訊地址</td>
                        <td colspan="3">
                            <input  type="text" name="addr1" class="form-control" value="{{ old('addr1',$user->addr1)}}">
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">住家電話</td>
                        <td><input  type="text" name="phone1" class="form-control" value="{{ old('phone1',$user->phone1)}}"></td>
                        <td class="table-title">手機</td>
                        <td><input  type="text" name="mphone" class="form-control" value="{{ old('mphone',$user->mphone)}}"></td>
                    </tr>
                    <tr>
                        <td class="table-title">E-mail</td>
                        <td colspan="3">
                            <input  type="email" name="email" class="form-control" value="{{ old('email',$user->email)}}">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="table-section">
                <b>- 學歷 / 經歷 / 服務單位</b>
                <table width="90%">
                    <tr>
                        <td class="table-title">學歷</td>
                        <td colspan="3">
                            <select name="degree" id="gender" class="form-control">
                                <option value="1" {{ ($user->degree == "1" ? "selected":"") }}>學士</option>
                                <option value="2" {{ ($user->degree == "2" ? "selected":"") }}>學士後靜修</option>
                                <option value="3" {{ ($user->degree == "3" ? "selected":"") }}>碩士</option>
                                <option value="4" {{ ($user->degree == "4" ? "selected":"") }}>博士</option>
                                <option value="5" {{ ($user->degree == "5" ? "selected":"") }}>研究員</option>
                                <option value="6" {{ ($user->degree == "6" ? "selected":"") }}>助理教授</option>
                                <option value="7" {{ ($user->degree == "7" ? "selected":"") }}>教授</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">畢業學校</td>
                        <td colspan="3">
                            <input  type="text" name="school" class="form-control" value="{{ old('school',$user->school)}}">
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">服務單位</td>
                        <td colspan="3">
                            <input  type="text" name="office" class="form-control" value="{{ old('office',$user->office)}}">
                        </td>
                    </tr>
                    <tr>
                        <td class="table-title">服務單位電話</td>
                        <td><input  type="text" name="ophone" class="form-control" value="{{ old('ophone',$user->ophone)}}"></td>
                        <td class="table-title">傳真</td>
                        <td><input  type="text" name="fax" class="form-control" value="{{ old('fax',$user->fax)}}"></td>
                    </tr>

                </table>

            </div>
            
            <div class="table-section">
                <b>- 參加活動</b>
                <table width="90%">
                    <tr>
                        <td class="table-title">新增參加活動</td>
                        <td colspan="3">
                            <select name="events" class="form-control">
                                <option value="0">請選擇</option>
                                @foreach($evnets as $event)
                                    <option value="{{$event->id}}">{{$event->score.' 小時 / '.$event->title}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <td colspan="4">
                        <input type="submit" class="btn btn-success btn-block" value="修改">
                    </td>
                </table>



            </div>
        </form>
        <br>
        <br>
        <br>
        <br>
        <h1>已參加活動</h1>
        <table width="90%" class="table">
            <tr>
                <td style="text-align: left">活動名稱</td>
                <td style="text-align: left">時數</td>
                <td></td>
            </tr>
            <?php $total = 0;?>
            @foreach($event_list as $list)
                <?php $total += $list->event->score ?>
                <form action="{{url("admin/master/".$user->id."/event_list/".$list->id)}}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <tr>
                        <td width="500" style="text-align: left">{{$list->event->title}}</td>
                        <td style="text-align: left; padding-left: 25px;">{{$list->event->score}}</td>
                        <td style="text-align: left"><button class="btn btn-danger">刪除</button></td>
                    </tr>
                </form>
            @endforeach
            <tr style="border-top:#000">
                <td></td>
                <td style="text-align: left">總時數</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: left;  padding-left: 25px;">{{$total}}</td>
                <td></td>
            </tr>
        </table>
    </div>
    @include("back.component.modify")

@endsection

