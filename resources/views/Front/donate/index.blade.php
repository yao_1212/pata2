@extends('front.layout.master2')
@section('content')
    <div class="container flex-3">
        @if(isset($type) && $type=="QA")
            @include("front.donate.QA")
        @elseif(isset($type) && $type=="support")
            @include("front.donate.support")
        @elseif(isset($type) && $type=="download")
            @include("front.donate.download")
        @else
            @include("front.donate.list")
        @endif
    </div>
@endsection