@extends('front.layout.master')

@section('content')
<div class="body_bg bg-coffee">
    <div class="content">
        <div class="flex-box">
            @include("front.index.what")
            {{--@if(Auth::check("user"))--}}
                {{--@include("front.index.toolbar")--}}
            {{--@else--}}
                {{--@include("front.index.login")--}}
            {{--@endif--}}            
        </div>
    </div>
</div>
<div class="mid_bg">
    <div class="content">
        <div class="events">
            @include("front.index.news")            
        </div>
    </div>
</div>
@if(Auth::check("user"))
<div class="how_to">
    <div class="content">
        <div class="events">
            @include("front.index.event")
        </div>
    </div>
</div>
@endif
<div class="intro">
    <div class="content">
        @include("front.index.dog")
    </div>
</div>
<div class="intro">
    <div class="content">
        @include("front.index.video")
    </div>
</div>
<div class="intro">
    <div class="content">
        @include("front.index.friend")
    </div>
</div>
<div align="center" class="info_button">
    <i class="fa fa-info-circle fa-3x" aria-hidden="true" style="color:white;"></i>
    <div class="intro">
        <div class="content">
            @include("front.index.help")
        </div>
    </div>
</div>
@endsection