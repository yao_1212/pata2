<div class="container flex-3" style="max-height: 375px;">
    <h1>會員登入</h1>
    <hr>
    <form action="{{url("login")}}" method="post">
        {{ csrf_field() }}

        <div class="form_box">
        <div class="form_group">
            <span class="form_title">帳號</span>
            <input type="text" name="user" id="user" placeholder="請輸入帳號">
        </div>

        <div class="form_group">
            <span class="form_title">密碼</span>
            <input type="text" name="ps" id="ps" placeholder="請輸入密碼">
        </div>
        <div class="flex-box">
            <div class="form_group flex-1 ">
                <span class="form_title">驗證碼</span>
                <input type="text" name="text_input" id="text_input" placeholder="請輸入驗證碼">
            </div>
            <div class="form_group flex-1 capture_box">
                <img src="{{URL::to('captcha')}}" class="capture" >
            </div>
        </div>

        <div class="form_group" style="padding-right:0px;">
            <input type="submit" class="btn btn-yellow" value="登入">
            <div class="forget"><a href="">忘記密碼</a></div>
        </div>
    </div>
    </form>

</div>
