<div class="container no-border" id="dog">

    <h1>動輔員介紹</h1>
    <span class="more"><a href="{{url("about/master")}}">更多動輔員</a></span>
    <hr>
    <div class="photo-list">

        @foreach($masters as $master)
            <div class="flex-1">
                <div class="img-wrap" style="background-image: url('{{asset("upload/master/".$master->img)}}')"></div>

                <div class="brief">
                    <h3 class="title">{{$master->name}}</h3>
                <span class="right-brief">
                    <a href="{{url("about/master")}}">更多</a>
                    <div class="date">
                        {{$master->getDateTime()}}
                    </div>
                </span>
                </div>
            </div>
        @endforeach


    </div>
</div>
