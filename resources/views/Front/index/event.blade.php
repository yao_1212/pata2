<div class="container no-border" id="events">
    <h1>動輔活動</h1>
    <span class="more"><a href="{{url('events')}}">更多活動</a></span>
    <hr>

    <ul class="list">
        @foreach($events as $event)
            <li>
                <div class="day">
                    <div class="m">{{$event->getMonth()}}月</div>
                    <div class="d">{{$event->getDay()}}</div>
                </div>
                <div class="brief">
                    <div class="title"><a href="{{url("events/".$event->id)}}">{{$event->title}}</a></div>
                    <div class="location">
                        <i class="icon ion-ios-location coffee"></i>
                        <span class="sub-title">{{$event->addr}}</span>
                    </div>
                    <div class="start">
                        <i class="icon ion-android-calendar coffee"></i>
                        <span class="sub-title">{{$event->startdate}}~{{$event->outdate}}</span>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</div>
