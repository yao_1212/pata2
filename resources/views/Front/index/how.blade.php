@extends('front.layout.master')

@section('content')
    <link rel="stylesheet" href="{{asset("css/front.css")}}">
    <div class="body_bg">
        <div class="content">
            <div class="container ">

                <h1>如何申請協會活動？</h1>
                <hr>
                <div class="text-box">
                    <p>哈囉~~~~您們好~~~~</p>

                    <p>在協會、學校與大家的支持努力下，</p>

                    <p>動物輔助治療已經越來越多人知道，</p>

                    <p>往往還是有很多單位會不清楚怎麼來跟我們聯繫邀請，</p>

                    <p>現在已經有「<strong>協會活動申請書</strong>」的表單可以下載囉~~</p>

                    <p>不過<ins>並不是填寫完寄回來就表示申請成功</ins>，(注意!!)</p>

                    <p>填寫的內容主要是讓我們動輔師群能初步的瞭解貴單位之需求，</p>

                    <p>需要經過審核，</p>

                    <p>之後我們會有專人與貴單位的窗口聯絡人來做聯繫。</p>

                    <p>另外，</p>

                    <p>申請的對象從小孩到老人並沒有什麼限制，</p>

                    <p>只是目前礙於人力，</p>

                    <p>無法做到1對1或到居家做服務，</p>

                    <p><ins>主要還是以&nbsp;<strong>團體&nbsp;</strong>及&nbsp;<strong>機構&nbsp;</strong>申請為主</ins>。</p>

                    <p>-------------------------------------------------------------------------------------------</p>

                    <p>簡略說明我們服務的對象類型及活動：</p>

                    <p>1.一般孩童-親子班、兒童營隊、學校宣導課程、班級體驗活動&hellip;等。</p>

                    <p>2.特殊兒童(特教班、早療兒童、自閉症、身心障礙&hellip;等)-&nbsp;北市特教案、單位或機構活動申請&hellip;等。</p>

                    <p>3.一般成人-舒壓活動、宣導演講、北護大學課程、醫護及照護員講座體驗、長者樂活&hellip;等。</p>

                    <p>4.特殊成人(身心障礙、失智/能老人、精障、安寧&hellip;等)- 長照2.0專案、桃園市衛生局失智非藥物治療專案、醫院或機構活動申請&hellip;等。</p>

                    <p>-------------------------------------------------------------------------------------------</p>

                    <p>假如在填表上有疑問，</p>

                    <p>請撥打<strong> (02)2822-7101轉3663</strong></p>

                    <p>聯絡人：<strong>台灣動物輔助治療專業發展協會-幹事/秘書&nbsp; 黃蘭嵐&nbsp; 小姐</strong></p>

                    <p>或來信 <strong>pata.tw@gmail.com</strong></p>

                    <p>也可以到我們Facebook官方粉絲團(搜尋 <strong>@pata.tw</strong>)留言詢問哦~</p>
                    
                    <div class="file section-box">
                    <p  style="background-color: #FAF8F4">
                        <a href="{{url('how/download')}}">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>  PATA-如何邀請協會申請書(word版)製.docx
                        </a>
                    </p>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection