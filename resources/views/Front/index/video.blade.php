<div class="container no-border" id="dog">
    <link rel="stylesheet" href="{{asset("css/album.css")}}">

    <h1>更認識協會</h1>
    <hr>
    <div class="photo-list">
        <div class="flex-2">
            <video style="width:90%;" controls poster="{{ asset('upload/videos/pina.png')}} ">
                <source src="{{ asset('upload/videos/PINA_I_LOVE_YOU.mp4')}} " type="video/mp4">
                Your browser does not support the video tag.
            </video>
            <div class="brief">
                披拿我愛你
            </div>
        </div>
        <div class="flex-2">
            <video  style="width:90%;" controls poster="{{ asset('img/pata_what.png')}} ">
                <source src="{{ asset('upload/videos/SMILE_HALO.mp4')}} " type="video/mp4">
                Your browser does not support the video tag.
            </video>
            <div class="brief">
                微笑吧！哈樂！
            </div>
        </div>
    </div>
</div>
