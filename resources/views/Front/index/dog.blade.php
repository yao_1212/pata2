<div class="container no-border" id="dog">
    <link rel="stylesheet" href="{{asset("css/album.css")}}">

    <h1>治療犬介紹</h1>
    <span class="more"><a href="{{url("about/dogs")}}">更多治療犬</a></span>
    <hr>
    <div class="photo-list">

        @foreach($dogs as $dog)
        <div class="flex-1">
            <div class="img-wrap" style="background-image: url('{{asset("upload/dog_album/".$dog->img)}}')"></div>

            <div class="brief">
                <h3 class="title">{{$dog->name}}</h3>
                <span class="right-brief">
                    <a href="{{url("about/dogs/".$dog->id)}}">更多</a>
                    <div class="date">
                        {{ $dog->type }} / {{ $dog->getSex() }} / {{$dog->getAge()}}歲
                    </div>
                </span>
            </div>
        </div>
        @endforeach

    </div>
</div>
