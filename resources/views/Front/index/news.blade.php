<div class="container flex-7" id="news" style="box-shadow: none;">

    <h1>最新消息</h1>
    <span class="more"><a href="{{url("news")}}">更多消息</a></span>
    <hr>
    <ul class="list">
        @foreach($news as $new)
        <li>
            <i class="icon ion-arrow-right-b coffee"></i>
            <div class="title">
                <a href="{{url("news/".$new->id)}}">
                    {{((mb_strlen($new->title,'utf8')> 27) ? mb_substr($new->title,0, 27,'utf8') : $new->title).' '.((mb_strlen($new->title,'utf8')> 27) ? " ..." : "")}}
                </a>
            </div>
            <span class="date">{{$new->getDateTime()}}</span>
        </li>
        @endforeach
    </ul>
</div>
