@extends('front.layout.master2')
@section('content')
    <div class="container flex-3">
        <h1>最新消息</h1>
        <hr>
        <div class="text-box">
            <table class="table">
                <thead>
                <tr>
                    <th width="110">發佈日期</th>
                    <th width="120">類別</th>
                    <th width="580">標題</th>
                </tr>
                </thead>
                <tbody>

                @foreach($news as $key => $data)
                        <tr>
                            <td>{{$data->getDateTime()}}</td>
                            <td>{{$data->type}}</td>
                            <td><a href="{{URL("news/".$data->id)}}">{{$data->title}}</a></td>
                        </tr>
                @endforeach
                </tbody>
            </table>

            <div align="center">
                {{ $news->links() }}
            </div>

        </div>

    </div>
@endsection