{{--<link rel="stylesheet" href="{{asset("css/app.css")}}">--}}
<link rel="stylesheet" href="{{asset("css/header.css")}}">
    <div class="main-header flex middle">
        <div class="logo">
            <div class="tw flex middle">
                <div style="display: inline-block"><a href="{{URL("/")}}" ><img src="{{asset("img/pata.jpg")}}" width="520"></a></div>
            </div>
            {{--<div class="en">Professional Animal-Assisted Therapy Association of Taiwan</div>--}}
        </div>
        <div class="header-right">
            <ul class="nav">
                <li data-hover>
                    <span class="title">協會簡介</span>
                    <ul class="subul">
                        <a href="{{URL("about/constitution")}}"><li>協會章程</li></a>
                        <a href="{{URL("about/structure")}}"><li>協會組織</li></a>
                        <a href="{{URL("about/commit")}}"><li>各組委員會</li></a>
                        <a href="{{URL("about/master")}}"><li>動輔師</li></a>
                        <a href="{{URL("about/dogs")}}"><li>治療犬</li></a>
                        <a href="{{URL("about/research")}}"><li>相關研究文獻</li></a>
                    </ul>
                </li>
                <li data-hover>
                    <span class="title">協會消息</span>
                    <ul class="subul">
                        <a href="{{URL("news")}}"><li>最新消息</li></a>
                        <a href="{{URL("news/type/2")}}"><li>學術研討會</li></a>
                        <a href="{{URL("news/type/3")}}"><li>好康報報</li></a>
                        <a href="{{URL("news/type/4")}}"><li>徵才訊息</li></a>
                        <a href="{{URL("news/report")}}"><li>媒體報導</li></a>
                    </ul>
                </li>
                <li>
                    <span class="title">會員專區</span>
                    <ul class="subul">
                    @if(!Auth::check("user"))
                        <a href="{{URL("login")}}"><li> 會員登入</li></a>
                    @else
                        <a href="{{URL("events")}}"><li>活動列表</li></a>
                        <a href="{{URL("user/event/list")}}"><li>積分查詢</li></a>
                        <a href="{{URL("user/news")}}"><li>會員專屬消息</li></a>
                        <a href="{{URL("logout")}}"><li>登出</li></a>
                    @endif
                    </ul>
                </li>
                <a href="{{URL("donate")}}"><li class="{{ Request::is('donate') ? 'active' : '' }}">愛心捐款</li></a>
                <li data-hover>
                    <span class="title">服務紀錄</span>
                    <ul class="subul">
                        <a href="{{URL("album")}}"><li>活動花絮</li></a>
                        <a href="{{URL("service")}}"><li>服務實績</li></a>
                    </ul>
                </li>
            </ul>
        </div>
    </div>


<script type="text/javascript" >
    $(function(){
        $('body').on('touchstart','[data-hover]',function(e){
            $(e.target).addClass('touching')
        });

        $('body').on('touchend','[data-hover]',function(e){
            $(e.target).removeClass('touching')
        });

    })
</script>
<style>

</style>