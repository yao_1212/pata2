<div class="main-footer">
    <div class="footer">
        <div align="center">
            <strong>Copyright © 2017  台灣動物輔助治療專業發展協會</strong> <br>
            Professional Animal-Assisted Therapy Association of Taiwan.  瀏覽人次：
            <?php 
		        $exists = Storage::disk('public')->get('viewer.txt');
		        Storage::disk('public')->put('viewer.txt', ++$exists);
				echo $exists;
            ?>
           <br>
            建議使用 螢幕解析度1280x768 & IE 11.0+/Chrome 瀏覽本站，以達到最佳效果
        </div>
    </div>
</div>