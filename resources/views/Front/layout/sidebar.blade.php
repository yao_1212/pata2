<?php

//
$donate_arr = array("list"=>"捐款芳名錄",
                    "support"=>"捐款說明",
                    "download"=>"表單下載區",
                    "QA"=>"捐款Q&A");

$about_arr = array("constitution"=>"協會章程",
                    "structure"=>"理監事名單",
                    "commit"=>"各組委員會",
                    "dogs"=>"治療犬介紹",
                    "master"=>"動輔師介紹",
                    "research"=>"相關研究文獻"
);

$album_arr = array(
                  "album"=>"活動花絮",
                  "service"=>"服務實績"
                );

$user_news_arr = array(""=>"全部",
                  "type/1"=>"協會公告",
                  "type/2"=>"動輔師公告",
                  "type/3"=>"治療犬公告");

$news_arr = array(""=>"全部",
                  "type/1"=>"最新消息",
                  "type/2"=>"學術研討會",
                  "type/3"=>"好康報報",
                  "type/4"=>"徵才訊息",
                  "report"=>"媒體報導"
                );

$events_arr = array(""=>"全部",
                    "type/1" => "訓練課程",
                    "type/2" => "治療師培訓",
                    "type/3" => "治療犬考試", 
                    "type/4" => "動輔服務", 
                    "type/5" => "動輔推廣教育"
                  );
$user_arr = array("edit"=>"修改資料","password"=>"修改密碼");
if($meta == 'user/news'){
  $arr = 'user_news_arr';
}elseif($meta == 'album'){
  // 為了去掉 prefix
  $arr = $meta."_arr";
  $meta = '';
}else{
  $arr = $meta."_arr";
}

?>

<div class="sidebar-box">
    <ul>
        @foreach($$arr as $key => $list)
            <a href="{{URL($meta."/".$key)}}"><li>{{$list}}</li></a>
        @endforeach

    </ul>
</div>
