const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {

    mix.sass(['front/collapse.scss','front/download.scss'],'public/css/front.css')
        .sass('sidebar.scss', 'public/css/sidebar.css')
        .sass(['style.scss', 'front/pagination.scss'], 'public/css/style.css')
        .sass('header.scss', 'public/css/header.css')
        .sass('album.scss','public/css/album.css')
        .sass(['back.scss','yvtkit/orders.scss','component.scss',"back/datatable.scss"], 'public/css/back.css');


    // mix.sass(['back.scss','yvtkit/orders.scss','component.scss',"back/datatable.scss"], 'public/css/back.css');
    //sass('back.scss', 'public/css/back.css')
    // sass('login.scss', 'public/css/login.css')
        //sass('back.scss', 'public/css/back.css')
        // .sass('style.scss', 'public/css/style.css')
        // .sass('header.scss', 'public/css/header.css')
        // .sass('album.scss','public/css/album.css');
        // .sass('app.scss', 'public/css/app.css')
        // .sass('sidebar.scss', 'public/css/sidebar.css');

    // .webpack('app.js');
});
