<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Action\Yvtuikit\OrderModel;

class Roster_master extends Model implements OrderModel
{
    protected $table = 'roster_master';
    protected $primaryKey = 'm_id';
    public $labelname = "理監事";



    protected $fillable = [
        'name',
        'service',
        'task',
        'img',
        'm_order',
        'published_at'
    ];

    public function getOrderColumns(){
        return "m_order";
    }
    public function getOrderForeignKeyName(){
        return "ros_id";
    }


    public $columns = array(
        "m_id"	    => array( "label" => "#", "listshow" => true, "edittype" => "disabled" ),
        "ros_id"	=> array( "label" => "#", "listshow" => true, "edittype" => "foreignKey" ),
        "name"      => array("label"=>"姓名", "listshow" => true, "editTitle"=>true, "edittype" => "input", "placeholder" => "請輸入名單標題", "required"=> false),
        "service"   => array("label"=>"服務單位", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> false),
        "task"      => array("label"=>"工作任務", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> false),
        "img"       => array("label"=>"特色照片","listshow"=>true,"edittype"=>"url_photo","listshowtype"=>"url_photo","file_note"=>"請上傳2MB以下的檔案"),
//        "order"  => array("label"=>"圖片", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "created_at"=> array("label"=>"新增時間", "listshow" => true, "edittype" => "disabled"),
    );

}
