<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'album';

    public $labelname = "活動花絮";
    public $primaryKey = "album_id";

    protected $fillable = [
        'title',
    ];


    public $columns = array(
        "album_id"   => array("label" => "#",       "listshow" => true, "edittype" => "disabled"),
        "title"      => array("label" => "相簿名稱", "listshow" => true, "edittype" => "input", "placeholder" => "請輸入標題", "required" => true),
        "cover"      => array("label"=>"封面","edittype" => "url_photo","listshow"=>"url_photo","file_note"=>"請上傳2MB以下的檔案"),

        "created_at" => array("label" => "建立時間", "listshow" => true, "edittype" => "disabled"),

    );


    public function Album_photo()
    {
        return $this->hasMany('App\Album_photo',"album_id");
    }

    public $rel_table = array(
        "Album_photo"	=>	array("label"=>'相片', "idx"=>"album_id","reledittype"=>"orders","edittype"=>"url_photo",'photo_type'=>'jpg' ),
    );
}
