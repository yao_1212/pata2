<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_list extends Model
{
    protected $table = 'event_list';
    public $primaryKey = "id";

    protected $fillable = [
        'events_id',
        'users_id',
        'isPay',
        'score'
    ];

    public function event(){
        return $this->belongsTo('App\Event','events_id','id');
    }
    public function getUser(){
        return $this->belongsToMany('App\User','users_id','user_id');
    }
}
