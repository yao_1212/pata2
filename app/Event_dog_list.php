<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_dog_list extends Model
{
    protected $table = 'event_dog_lists';
    public $primaryKey = "id";

    protected $fillable = [
        'events_id',
        'dog_id',
        'isPay',
        'score'
    ];

    public function event(){
        return $this->belongsTo('App\Event','events_id','id');
    }
}
