<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'event';

    public $labelname = "動輔活動";
    public $primaryKey = "id";

    protected $fillable = [
        'id',
        'title',
        'type',
        'startdate',
        'comedate',
        'outdate',
        'master',
        'addr',
        'admin',
        'phone',
        'score',
        'content'
    ];

//    public function UserList(){
//        return $this->hasMany('App\Event_list','events_id','id');
//    }
    public function UserList()
    {
        return $this->belongsToMany('App\User', 'event_list', 'events_id', 'users_id')->withPivot('score')
                    ->select(['user_id', 'tw_name', 'office', 'mphone','email']);
    }
    public function DogList()
    {
        return $this->belongsToMany('App\Dog', 'event_dog_lists', 'events_id', 'dog_id')->withPivot('score')
            ->select(['dogs.dog_id', 'name','user_id', 'nickname']);
    }
    public function getMonth()
    {
        $month_arr = ["一","二","三","四","五","六","七","八","九","十","十一","十二"];
        $month = date("m", strtotime($this->comedate));
        return $month_arr[$month-1];
    }
    public function getDay()
    {
        $day = date("d", strtotime($this->comedate));
        return $day;
    }
    
    public function getType()
    {
        $type_arr = ["訓練課程","治療師培訓","治療犬考試", "動輔服務", "動輔推廣教育"];
        $type = $this->type;
        return $type_arr[$type-1];
    }
//    public function EventList()
//    {
//        return $this->hasMany('App\Event_list');
//    }
    public $columns = array(
        'id'=> array( "label" => "#", "listshow" => true, "edittype" => "disabled" ),
        'title'=> array( "label" => "活動標題", "listshow" => true, "edittype" => "input" ),
        'type'=> array( "label" => "活動型態", "listshow" => true, "edittype" => "select", 'editarray'=>[
                                                                                ['value'=>'1','text'=>'訓練課程'],
                                                                                ['value'=>'2','text'=>'治療師培訓'],
                                                                                ['value'=>'3','text'=>'治療犬考試'],
                                                                                ['value'=>'4','text'=>'動輔服務'],
                                                                                ['value'=>'5','text'=>'動輔推廣教育']
                                                                                ]),
        'startdate'=> array( "label" => "報名開始日期", "listshow" => true, "edittype" => "date" ),
        'outdate'=> array( "label" => "報名結束日期", "listshow" => false, "edittype" => "date" ),
        'comedate'=> array( "label" => "報到日期", "listshow" => true, "edittype" => "date" ),
        'master'=> array( "label" => "主辦單位", "listshow" => false, "edittype" => "input" ),
        'addr'=> array( "label" => "地址", "listshow" => false, "edittype" => "input" ),
        'admin'=> array( "label" => "聯絡人", "listshow" => true, "edittype" => "input" ),
        'phone'=> array( "label" => "聯絡電話", "listshow" => false, "edittype" => "input" ),
        // 'score'=> array( "label" => "積分", "listshow" => true, "edittype" => "number" ),
        'content'=> array( "label" => "內文", "listshow" => false, "edittype" => "ckeditor" ),
    );
}
