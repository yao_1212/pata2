<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Researches extends Model
{
    protected $table = 'researches';
    public $labelname = "動輔相關研究文獻";

    public $delete = true;
    
    public function getDateTime()
    {
        $d = strtotime($this->date);
        $this->date = date("Y", $d);
        return date("Y", $d);
    }

    protected $fillable = [
        'title',
        'type',
        'link',
        'date'
    ];
    public $columns = array(
        "id"	=> array( "label" => "#", "listshow" => true, "edittype" => "disabled" ),
        "title"  => array("label"=>"文獻標題", "listshow" => true, "edittype" => "input", "placeholder" => "請輸入標題", "required"=> true),
        "date"  => array("label"=>"出版年份", "listshow" => true, "edittype" => "date", "placeholder" => "請輸入日期", "required"=> true),
        "link"  => array("label"=>"連結", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入連結", "required"=> false),
    );
}
