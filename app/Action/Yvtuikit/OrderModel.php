<?php
namespace App\Action\Yvtuikit;

interface OrderModel{

    public function getOrderColumns();
    public function getOrderForeignKeyName();

}