<?php

namespace App\Http;


use App\Http\Controllers\Controller;
use Validator;
use FontLib\TrueType\File;
use Symfony\Component\HttpFoundation\Request;
use Intervention\Image\Facades\Image;

class YvtAction extends Controller
{

    public $requests; // requests
    public $yvtset;   // yvtset(model)
    public $columns;  // yvtset(model)欄位
    public $data;     // requests送來的data

    public function __construct($requests,$yvtset)
    {
        $this->requests = $requests;
        $this->data = $requests->all();
        $this->yvtset = $yvtset;
        $this->columns = $yvtset->columns;
        $this->tableName = $yvtset->getTable();
    }




    public function setInputType($inputName){
        $this->yvtset->$inputName = $this->data[$inputName];
    }

    public function setUuid($inputName){
        $uuid = date("Y")-1911;
        $this->yvtset->$inputName = $uuid.date("md")."h".randomString(4);
    }


    //PHP >= 5.4
    //Fileinfo Extension
    //And one of the following image libraries.
    //
    //GD Library (>=2.0) … or …
    //Imagick PHP extension (>=6.5.7)
    public function setImageUpload($column,$inputName){
        $file = $this->requests->file($inputName);
        //檢查檔案
        $this->validate($this->requests, [
            $inputName => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if(!$file){
//            $this->yvtset->$inputName = null;
            return false;
        }

//        $filename = $file->getClientOriginalName();
        $ext      = $file->Extension();//副檔名
        $realPath = $file->getRealPath();
        $filename = sprintf("%s%s%s.%s",str_random(3),date("smdY"),str_random(5),$ext);
        $photo_size = (isset($column["photo_size"]))?$column["photo_size"] : null;
        $img = Image::make( $realPath );
        $org_w = $img->width();
        $org_h = $img->height();

        //判斷有沒有給大小
        if(isset($photo_size)){

            if(isset($photo_size["w"])){
                $output_w = $photo_size["w"];
            }

            if(isset($photo_size["h"])){
                $output_h = $photo_size["h"];

                $org_ratio = $org_w / $org_h;
                $output_ratio = $output_w / $output_h;

                if ( $org_ratio > $output_ratio ){
                    // Triggered when source image is wider
                    $temp_height = $output_h;
                    $temp_width = ( int ) ( $output_h * $org_ratio );
                } else {
                    // Triggered otherwise (i.e. source image is similar or taller)
                    $temp_width = $output_w;
                    $temp_height = ( int ) ( $output_w / $org_ratio );
                }
            }
            else{
                $output_h = $output_w / $org_w * $org_h;
                $temp_height = $output_h;
                $temp_width = $output_w;
            }
            $img->resize($temp_width,$temp_height);
        }

        //儲存資料夾的地方是 "upload/{model_name}/
        $dir = 'upload/'.$this->tableName."/".$filename;
        $img->save(public_path($dir));
//        $path     = $file->storeAs($this->tableName,$filename);
        $this->yvtset->$inputName = $filename;
    }




    // $this->yvtset->$inputName model->columns的foreach到哪個欄位
    // 需要注意檔案上傳資安的問題
    public function setFileType($column,$inputName){
        //檔案
        $file = $this->requests->file($inputName);
        //檢查檔案
        if(!$file){
//            $this->yvtset->$inputName = null;
            return false;
        }

        //驗證
        $validator = Validator::make($this->data, [
            $inputName => 'max:2048' // KB單位,2048 = 2MB
        ]);


        if ($validator->fails())
        {
            $this->yvtset->$inputName = null;
        }
        else
        {
            //檔名要記得改，現在是會重複檔名
            $ext      = $file->Extension();//副檔名
//            $filename = $file->getClientOriginalName().'.'.$ext;
            $filename = $file->getClientOriginalName();

            $path     = $file->store($column["file_dir"]);
//            return $filename;
            $this->yvtset->$inputName = $path;
            $this->yvtset->filename = $filename;

        }

    }






    

    public function getRequest(){
        echo "<pre>";
        print_r($this->data);
    }

    public function getYvtset(){
        return $this->yvtset;
    }






}
