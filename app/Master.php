<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    protected $table = 'master';

    public $labelname = "動輔師介紹";
    public $primaryKey = "id";

    protected $fillable = [
        'name',
        'img',
    ];

    public function getYearsText()
    {
        // 超過10年, 5-10年,
        $years = $this->years;
        if ($years > 10) {
            return '超過 10 年';
        } elseif ($years>=5 && $years<10) {
            return '5 - 10 年';
        } else {
            return $this->years."年";
        }
    }
    public function getDateTime()
    {
        $d = strtotime($this->created_at);
        $this->created_at = date("Y-m-d", $d);
        return date("Y-m-d", $d);
    }
    public $columns = array(
        "id"	=> array( "label" => "#", "listshow" => true, "edittype" => "disabled" ),
        "name"  => array("label"=>"名字", "listshow" => true, "edittype" => "input", "placeholder" => "請輸入姓名", "required"=> true),
        "work_type"   => array("label"=>"類別", "listshow" => true, "edittype" => "select",
            "editarray"=>array(
                array("value" => "護理師","text"=>"護理師"),
                array("value" => "心理師","text"=>"心理師"),
                array("value" => "特教教師","text"=>"特教教師")
            )),
        "years"  => array("label"=>"執行資歷", "listshow" => true, "edittype" => "number", "placeholder" => "請輸入年份", "required"=> true),
        "level"   => array("label"=>"動輔師類別", "listshow" => true, "edittype" => "select",
            "editarray"=>array(
                array("value" => "初級","text"=>"初級"),
                array("value" => "中級","text"=>"中級"),
                array("value" => "高級","text"=>"高級"),
                array("value" => "資深高級","text"=>"資深高級")
            )),
        "active"   => array("label"=>"動輔師活動狀態", "listshow" => true, "edittype" => "select",
            "editarray"=>array(
                array("value" => "活躍","text"=>"活躍"),
                array("value" => "經常","text"=>"經常"),
                array("value" => "偶爾","text"=>"偶爾"),
                array("value" => "極少","text"=>"極少")
            )),
        "intro"  => array("label"=>"自我介紹", "listshow" => true, "edittype" => "textarea", "placeholder" => "請輸入自我介紹", "required"=> false),

        "img"=> array("label"=>"圖片", "photo_size"=> ["w"=>360,"h"=>250],"edittype" => "url_photo","listshow"=>"url_photo","file_note"=>"請上傳2MB以下的檔案"),
    );
}
