<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structures extends Model
{
    protected $table = 'structures';

    protected $fillable = [
        'title',
        'staff',
        'rank'
    ];
}
