<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "tw_name"=> "required|max:15",
            "en_name"=> "required|max:25",
            "twID" => "required|unique:user",
            "birth"=> "required|date",
            "gender"=> "required",
            "addr1" => "required",
            "phone1"=> "required",
            "mphone"=> "required",
            "email" => "required|unique:user",
            "degree"=> "required",
            "school"=> "required",
            "office"=> "required",
            "ophone"=> "required",
            "fax"   => "required"
        ];
    }
}
