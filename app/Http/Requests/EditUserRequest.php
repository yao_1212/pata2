<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "tw_name"=> "required|max:15",
            "en_name"=> "required|max:25",
            "gender"=> "required",
            "addr1" => "required",
            "phone1"=> "required",
            "mphone"=> "required",
            "degree"=> "required",
            "school"=> "required",
            "office"=> "required",
            "ophone"=> "required"
        ];
    }
}
