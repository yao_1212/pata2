<?php

namespace App\Http\Controllers;

use App\Commit;
use App\DogAlbum;
use App\Master;
use App\Structures;
use App\Constitution;
use App\Services;
use App\Reports;
use App\Researches;
use Illuminate\Http\Request;

class AboutController extends Controller
{

    public function research(){
        $meta = "about";
        $researches = Researches::orderBy('date', 'desc')->paginate(20);
        return view('front.about.researches',compact("meta","researches"));
    }

    public function service(){
        $meta = "album";
        $services = Services::orderBy('date', 'desc')->paginate(20);
        return view('front.about.services',compact("meta","services"));
    }

    public function report(){
        $meta = "news";
        $reports = Reports::orderBy('date', 'desc')->paginate(20);
        return view('front.about.report',compact("meta","reports"));
    }

    public function master(){
        $meta = "about";
        $masters = Master::orderBy("created_at","DESC")->get();
        return view('front.about.master',compact("meta","masters"));
    }
    public function dogs(){
        $meta = "about";
        $dogs = DogAlbum::orderBy("created_at","DESC")->get();
        return view('front.about.dogs',compact("meta","dogs"));
    }
    public function dogsView($id){
        $meta = "about";
        $dogs = DogAlbum::where('id', $id)->get();
        return view('front.about.dogs',compact("meta","dogs"));
    }
    public function constitutions(){
        $constitutions = Constitution::find(1);
        $total = json_decode($constitutions['total']);
        $member = json_decode($constitutions['member']);
        $power = json_decode($constitutions['power']);
        $meeting = json_decode($constitutions['meeting']);
        $money = json_decode($constitutions['money']);
        $sub = json_decode($constitutions['sub']);

        $meta = "about";
        return view('front.about.constitution',compact("meta"))
            ->with('total', $total)
            ->with('member', $member)
            ->with('power', $power)
            ->with('meeting', $meeting)
            ->with('money', $money)
            ->with('sub', $sub);
    }
    public function structure(){
        $meta = "about";
        $structures = Structures::OrderBy('rank','desc')->get();
        return view('front.about.structure',compact("meta","structures"));
        return view('front.about.structure',compact("meta"));
    }
    public function commit(){
        $meta = "about";
        $commits = Commit::OrderBy('rank','desc')->get();
        return view('front.about.commit',compact("meta","commits"));
    }
}
