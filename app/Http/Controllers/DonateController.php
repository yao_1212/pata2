<?php

namespace App\Http\Controllers;

use App\Donate;
use Illuminate\Http\Request;

use App\Http\Requests;

class DonateController extends Controller
{
    public function index2(Request $request){
        $meta = $request->path();
        $donates = Donate::orderBy("donate_time","DESC")->get();
        return view('front.donate.index',compact("meta","donates"));
    }
    public function index($type=null){
        $meta = "donate";
        $donates = Donate::orderBy("donate_time","DESC")->paginate(20);
        return view('front.donate.index',compact("meta","donates","type"));
    }
}
