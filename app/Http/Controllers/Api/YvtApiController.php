<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class YvtApiController extends Controller
{
    public function yvt_list($yvtset){
        $yvtset = "App\\".ucfirst($yvtset);
        $yvtset = new $yvtset;

        $columns = $yvtset->columns;
        $yvtset = $yvtset->all();

        return $yvtset;
    }
    public function yvt_detail($yvtset,$id){
        $yvtset = "App\\".ucfirst($yvtset);
        $yvtset = new $yvtset;
        $columns = $yvtset->columns;

        $yvtset = $yvtset->find($id);

        return $yvtset;
    }

}
