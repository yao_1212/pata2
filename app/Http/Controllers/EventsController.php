<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;

use App\Http\Requests;

class EventsController extends Controller
{
    public function index(Request $request, $type=null)
    {
        $meta = "events";
        if (isset($type)) {
            $events = Event::where("type", $type)->orderBy("created_at", "DESC")->paginate(10);
        } else {
            $events = Event::orderBy("created_at", "DESC")->paginate(20);
        }
        return view('front.events.index', compact("meta", "events"));
    }
    public function view($id)
    {
        $meta = "events";

        $events = Event::find($id);

        return view('front.events.view', compact("meta", "events"));
    }
    public function calendar()
    {
        $meta = "events";
        return view('front.events.calendar', compact("meta"));
    }
}
