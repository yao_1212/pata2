<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserNews;
use App\Http\Requests;
use Illuminate\Support\Facades\View;

class UserNewsController extends Controller
{
    public function index(Request $request,$type=null){
        $meta = "user/news";
        $news_arr = array(
        	"1"=>"協會公告",
            "2"=>"動輔師組公告",
            "3"=>"治療犬組公告"
        );

        if(isset($type) && array_key_exists($type,$news_arr)){
            $news = UserNews::where("type",$news_arr[$type])->orderBy("created_at","DESC")->paginate(10);
        }else{
            $news = UserNews::orderBy("created_at","DESC")->paginate(10);
        }
        return view('front.member.news.index',compact("meta","news"));
    }

    public function view($id){
        $meta = "user/news";
        $news = UserNews::find($id);
        return view('front.news.view',compact("meta","news"));

    }
}
