<?php

namespace App\Http\Controllers;

use App\Album;
use Illuminate\Http\Request;

use App\Http\Requests;

class AlbumController extends Controller
{
    public function index(Request $request){
        $meta = $request->path();
        $albums = Album::orderBy("created_at","DESC")->paginate(9);
        return view('front.album.index',compact("albums","meta"));
    }
    public function view(Request $request, $id){
        $meta = 'album';
        $albums = Album::find($id);
        $photos = $albums->Album_photo()->orderBy("photo_order","ASC")->get();
        return view('front.album.view',compact("albums","photos","meta"));
    }
}
