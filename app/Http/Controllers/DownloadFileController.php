<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadFileController extends Controller
{
	public function getYvtset($yvtset){
        return "App\\" . str_replace('_', '', ucwords($yvtset, '_'));
    }
    public function download($yvtset, $column="filepath", $id){
        $yvtset = $this->getYvtset($yvtset);       
        $yvtset = new $yvtset;
        $news = $yvtset::find($id);

        $filepath = storage_path("app/".$news->filepath);
        $filename = $news->filename;
        return response()->download($filepath ,$filename);
    }
}
