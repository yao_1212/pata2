<?php

namespace App\Http\Controllers;

use App\Dog;
use App\DogAlbum;
use App\Master;
use App\Event;
use Illuminate\Http\Request;
use App\News;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Response;

class IndexController extends Controller
{
    public function index(){
        $score = 0;
        $user = Auth::user();
        
        $news = News::orderBy("created_at","DESC")->take(6)->get();

        $dogs = DogAlbum::inRandomOrder()->take(3)->get();

        $masters = Master::orderBy("created_at","DESC")->take(3)->get();

        $events = Event::orderBy("created_at","DESC")->take(3)->get();

        return View("front.index",compact("meta","news","dogs","masters","events",'score'));
    }

    public function how(){
        return view('front.index.how');
    }

    public function howDownload(){
        $file = Storage::disk('public')->url('how.docx');
        $headers = array(
              'Content-Type: application/pdf',
            );
        return response()->download(storage_path('app/public/how.docx'), 'PATA-如何邀請協會申請書(word版)製.docx');
    }
}
