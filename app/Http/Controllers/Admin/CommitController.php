<?php

namespace App\Http\Controllers\Admin;

use App\Commit;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;

class CommitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commits = Commit::OrderBy('rank','desc')->get();
        return View::make("back.commit.index")
            ->withcommits($commits);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make("back.commit.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        echo "<pre>";
        $array = array();
        $array = array_add($array,"0",array($request['name1'],$request['office1']));
        $into = array();
        foreach($request['name2'] as $index => $name2)
        {
            $into = array_add($into,$index,array($name2,$request['office2'][$index]));
        }
        $array = array_add($array,"1",$into);
//        print_r($array);
//        echo "===================================";
        $into = array();
        foreach($request['name3'] as $index => $name3)
        {
            $into = array_add($into,$index,array($name3,$request['office3'][$index]));
        }
        $array = array_add($array,"2",$into);
//        print_r($array);
//        echo "===================================";
        $into = array();
        foreach($request['name4'] as $index => $name3)
        {
            $into = array_add($into,$index,array($name3,$request['office4'][$index]));
        }
        $array = array_add($array,"3",$into);
//        print_r($array);
//        echo "===================================";
        $into = array();
//        foreach($request['name5'] as $index => $name3)
//        {
//            $into = array_add($into,$index,array($name3,$request['office5'][$index]));
//        }
//        $array = array_add($array,"4",$into);
////        print_r($array);
////        echo "===================================";
//        $into = array();
//        foreach($request['name6'] as $index => $name3)
//        {
//            $into = array_add($into,$index,array($name3,$request['office6'][$index]));
//        }
//        $array = array_add($array,"5",$into);


        Commit::create([
            'title'=> $request['title'],
            'staff'=> json_encode($array),
            'rank' => $request['rank']
        ]);
        return redirect('admin/commit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $commits = Commit::findOrFail($id);
        return View::make("back.commit.view")
            ->withcommits($commits);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $commits = Commit::findOrFail($id);
        return View::make("back.commit.edit")
            ->withcommits($commits);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $array = array();
        $array = array_add($array,"0",array($request['name1'],$request['office1']));
        $into = array();
        foreach($request['name2'] as $index => $name2)
        {
            $into = array_add($into,$index,array($name2,$request['office2'][$index]));
        }
        $array = array_add($array,"1",$into);
//        print_r($array);
//        echo "===================================";
        $into = array();
        foreach($request['name3'] as $index => $name3)
        {
            $into = array_add($into,$index,array($name3,$request['office3'][$index]));
        }
        $array = array_add($array,"2",$into);
//        print_r($array);
//        echo "===================================";
        $into = array();
        foreach($request['name4'] as $index => $name3)
        {
            $into = array_add($into,$index,array($name3,$request['office4'][$index]));
        }
        $array = array_add($array,"3",$into);
//        print_r($array);
//        echo "===================================";
        $into = array();
////        foreach($request['name5'] as $index => $name3)
////        {
////            $into = array_add($into,$index,array($name3,$request['office5'][$index]));
////        }
////        $array = array_add($array,"4",$into);
//////        print_r($array);
//////        echo "===================================";
////        $into = array();
////        foreach($request['name6'] as $index => $name3)
////        {
////            $into = array_add($into,$index,array($name3,$request['office6'][$index]));
////        }
//        $array = array_add($array,"5",$into);

        $commit = Commit::findOrFail($id);
        $commit->title = $request['title'];
        $commit->staff = json_encode($array);
        $commit->rank = $request['rank'];
        $commit->save();
        return redirect('admin/commit/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Commit::destroy($id);
        return redirect("admin/commit");
    }
}
