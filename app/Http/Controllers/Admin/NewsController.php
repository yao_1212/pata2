<?php

namespace App\Http\Controllers\Admin;

use App\Events\FileUpload;
use App\Events\SomeEvent;
use App\Http\YvtAction;
use App\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;

class NewsController extends Controller
{
    public function index(){
//        dd(Auth::user()->password);
        $news = News::all();
        return view("back.news.index")->with("news",$news);
    }


    public function create(){
        $news = new News;
        $columns = $news->columns;
        return view("back.news.create",compact("news","columns"));
    }
    public function show(News $news){
        $columns = $news->columns;
        return view("back.news.edit",compact("news","columns"));
    }






    public function store(Request $requests){

        $news = new News;
        $columns = $news->columns;

        //傳入data 跟 yvtset(model)
        $yvtAction = new YvtAction($requests,$news);

        $this->storeData($columns,$yvtAction);

        //debug
        $news = $yvtAction->getRequest();
        $news->save();
        return redirect("admin/news/".$news->id);
    }


    public function update(Request $requests,$id){
        $news = News::find($id);

        //為了foreach
        $columns = $news->columns;

        //傳入data 跟 yvtset(model)
        $yvtAction = new YvtAction($requests,$news);

        $this->storeData($columns,$yvtAction);

//        $news = $yvtAction->getRequest();
        $news->save();

        return back();

        //接著關聯
    }

    public function storeData($columns,$yvtAction){
        foreach ($columns as $key => $column) {
            // 這邊應該要寫一個class
            // YvtAction 放新增時要存的東西
            // 再來分別去call -> create() 和 update
            // 就可以知道是 新增還是修改
            // 上傳圖片 與 上傳檔案 要分開寫一個service
            //
            switch ($column["edittype"]) {
                case "input":
                case "textarea":
                case "select":
                case "ckeditor":

                $yvtAction->setInputType($key);
                    //這邊要創一個class -> "Form/InputType"
                    break;
                case "file":
                    $yvtAction->setFileType($column, $key);
//                    event(new FileUpload($requests->$key));
                    break;
            }
        }
    }


}
