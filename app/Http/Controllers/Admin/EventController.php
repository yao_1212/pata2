<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Event_dog_list;
use App\Event_list;
use App\Events\FileUpload;
use App\Events\SomeEvent;
use App\Http\YvtAction;
use App\News;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;

class EventController extends Controller
{
    public function saveEventDogList(Request $request){
        $result = [];
        $data = $request->all();
//        return $data;
        //檢查是否有選活動
        if($data['event_id'] == 0){
            $result['message'] = 'PLEASE SELECT OPTION';
            $result['action'] = 100;
            return json_encode($result);
        }
        //檢查是否有這個活動
        $event = Event::find($data['event_id']);
        if(!$event){
            $result['message'] = 'NOT FOUND EVENT';
            $result['action'] = 400;
            return json_encode($result);
        }
        //檢查是否參加過活動
        $count = Event_dog_list::where('events_id',$data['event_id'])
                                ->where('dog_id',$data['id'])
                                ->count();
        if($count > 0){
            $result['message'] = 'REPEAT JOIN EVENT';
            $result['action'] = 101;
            return json_encode($result);
        }
        if($data['score'] <= 0){
            $result['message'] = 'PLEASE ENTER SCORE';
            $result['action'] = 102;
            return json_encode($result);
        }

        if(Event_dog_list::create([
            'events_id' =>  $data['event_id'],
            'dog_id'  => $data['id'],
            'score' => $data['score']            
        ])){
            $result['message'] = 'SUCCESS';
            $result['action'] = 200;
            return json_encode($result);
        }
        $result['message'] = 'ERROR';
        $result['action'] = 500;
        return json_encode($result);
    }

    public function saveEventList(Request $request){
        $result = [];
        $data = $request->all();

        //檢查是否有選活動
        if($data['event_id'] == 0){
            $result['message'] = 'PLEASE SELECT OPTION';
            $result['action'] = 100;
            return json_encode($result);
        }
        //檢查是否有這個活動
        $event = Event::find($data['event_id']);
        if(!$event){
            $result['message'] = 'NOT FOUND EVENT';
            $result['action'] = 400;
            return json_encode($result);
        }

        //檢查是否參加過活動
        $count = Event_list::where('events_id',$data['event_id'])
                            ->where('users_id',$data['id'])
                            ->count();
        if($count > 0){
            $result['message'] = 'REPEAT JOIN EVENT';
            $result['action'] = 101;
            return json_encode($result);
        }

        if($data['score'] <= 0){
            $result['message'] = 'PLEASE ENTER SCORE';
            $result['action'] = 102;
            return json_encode($result);
        }

        if(Event_list::create([
            'events_id' =>  $data['event_id'],
            'users_id'  => $data['id'],
            'score' => $data['score']
        ])){
            $result['message'] = 'SUCCESS';
            $result['action'] = 200;
            return json_encode($result);
        }
        $result['message'] = 'ERROR';
        $result['action'] = 500;
        return json_encode($result);
    }

    public function listPage($id){
        $events = Event::with(['dogList'=>function($query){
            $query->with('User');
        }, 'userList'])->find($id);

        return View::make('back.event.list',compact("events"));
    }
    public function deleteList($user_id, $id){
        Event_list::destroy($id);
        return redirect("admin/master/" . $user_id);
    }
    public function deleteDogList($dog_id, $id){
        Event_dog_list::destroy($id);
        return Redirect::back();;
    }
    public function index(){
        $events = Event::orderBy('id','desc')->paginate(10);
        foreach ($events as $event){
            $user_count = Event_list::where('events_id', $event->id)->count();
            $dog_count  = Event_dog_list::where('events_id', $event->id)->count();
            $event->total = $user_count + $dog_count;
        }
        return View::make('back.event.index',compact("events"));

    }

    public function create(){
        $yvtset = new Event();

        $columns = $yvtset->columns;
        return View('back.event.create',compact("yvtset","columns"));
    }
    public function store(Request $requests){
        $yvtset = new Event();

        $columns = $yvtset->columns;
        $yvtAction = new YvtAction($requests, $yvtset);

        $this->storeData($columns, $yvtAction);

        $yvtset = $yvtAction->getYvtset();
        $yvtset->save();
        return redirect("admin/event/" . $yvtset->id);
    }

    public function storeData($columns, $yvtAction)
    {
        foreach ($columns as $key => $column) {
            switch ($column["edittype"]) {
                case "uuid":
                    $yvtAction->setUuid($key);
                    break;
                case "input":
                case "number":
                case "date":
                case "textarea":
                case "select":
                case "ckeditor":
                case "foreignKey":
                    $yvtAction->setInputType($key);
                    //這邊要創一個class -> "Form/InputType"
                    break;
                case "file":
                    $yvtAction->setFileType($column, $key);
//                    event(new FileUpload($requests->$key));
                    break;
                case "url_photo":
                    $yvtAction->setImageUpload($column, $key);
                    break;
            }
        }
    }
    public function show($id){
        $yvtset = Event::find($id);
        $columns = $yvtset->columns;

        return View('back.event.edit',compact("yvtset", "columns"));
    }


    public function update(Request $requests,$id){
        $yvtset_class = new Event();
        $columns = $yvtset_class->columns;

        $yvtset = $yvtset_class::find($id);
        //傳入data 跟 yvtset(model)
        $yvtAction = new YvtAction($requests, $yvtset);

        $this->storeData($columns, $yvtAction);


        $yvtset->save();

        return redirect("admin/event/" . $yvtset->id);
    }

}
