<?php

namespace App\Http\Controllers\Admin;

use App\Constitution;
use App\Structures;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;

class ConstitutionController extends Controller
{
    public function show()
    {
        $constitutions = Constitution::find(1);
        $total = json_decode($constitutions['total']);
        $member = json_decode($constitutions['member']);
        $power = json_decode($constitutions['power']);
        $meeting = json_decode($constitutions['meeting']);
        $money = json_decode($constitutions['money']);
        $sub = json_decode($constitutions['sub']);

        return view("back.constitution.edit")
                ->with('total', $total)
                ->with('member', $member)
                ->with('power', $power)
                ->with('meeting', $meeting)
                ->with('money', $money)
                ->with('sub', $sub);

    }


    public function store(Request $request){
        $data = $request->all();
//        return $request->all();
        $constitution = Constitution::find(1);

        $total_arr = [];
        $total = 0;
        foreach($data['total_value'] as $key => $total_key){
            $arr['key'] = ++$total;
            $arr['value'] = $data['total_value'][$key];
            array_push($total_arr, $arr);
        }

        $member_arr = [];
        foreach($data['member_value'] as $key => $member_key){
            $arr['key'] = ++$total;
            $arr['value'] = $data['member_value'][$key];
            array_push($member_arr, $arr);
        }

        $power_arr = [];
        foreach($data['power_value'] as $key => $power_key){
            $arr['key'] = ++$total;
            $arr['value'] = $data['power_value'][$key];
            array_push($power_arr, $arr);
        }

        $meeting_arr = [];
        foreach($data['meeting_value'] as $key => $meeting_key){
            $arr['key'] = ++$total;
            $arr['value'] = $data['meeting_value'][$key];
            array_push($meeting_arr, $arr);
        }

        $money_arr = [];
        foreach($data['money_value'] as $key => $money_key){
            $arr['key'] = ++$total;
            $arr['value'] = $data['money_value'][$key];
            array_push($money_arr, $arr);
        }

        $sub_arr = [];
        foreach($data['sub_value'] as $key => $sub_key){
            $arr['key'] = ++$total;
            $arr['value'] = $data['sub_value'][$key];
            array_push($sub_arr, $arr);
        }

        $constitution->total = json_encode($total_arr);
        $constitution->member = json_encode($member_arr);
        $constitution->power = json_encode($power_arr);
        $constitution->meeting = json_encode($meeting_arr);
        $constitution->money = json_encode($money_arr);
        $constitution->sub = json_encode($sub_arr);

        $constitution->save();
        return redirect('admin/constitution/edit');
    }

//    public function store(Request $request){
//        $data = $request->all();
////        return $request->all();
//        $total_arr = [];
//        foreach($data['total_key'] as $key => $total_key){
//            $arr['key'] = $total_key;
//            $arr['value'] = $data['total_value'][$key];
//            array_push($total_arr, $arr);
//        }
//        $result['total'] = $total_arr;
//        Constitution::create([
//            'total'=>json_encode($total_arr)
//        ]);
//    }
}
