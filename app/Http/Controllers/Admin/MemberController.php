<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Event_list;
use App\Events\FileUpload;
use App\Events\SomeEvent;
use App\Http\YvtAction;
use App\News;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;



class MemberController extends Controller
{

    public function checkPay($id){
        $user = User::find($id);
        $user->payTime = date("Y-m-d H:i:s");
        $user->save();
        return redirect::back();
    }

    //動輔師
    public function index(){
        if(Input::has('twID'))
        {
            $twID = Input::get('twID');
//            $User = User::all()->where('twID',$twID);
            $users = User::whereRaw("`type` = 1 AND (`twID` LIKE '%".$twID."%' or `tw_name` LIKE '%".$twID."%')")->paginate(10);
        }
        else
        {
            $users = User::whereRaw("`type` = 1")->paginate(10);
        }
        return View::make('back.user.index',compact("users"));

    }

    public function create(){
//        $user = User::find($id);
        $type = 2;
        $actionUrl = 'admin/user';
        return View('back.master.create', compact('type','actionUrl'));
    }
    public function store(Request $request){
        $rules = array('tw_name'=>'required','twID'=>'required|unique:users','birth'=>'required','dog_img'=>'image|mimes:jpeg,png,jpg|max:2048');
        $validator = Validator::make($request->all(),$rules);

        if(!$validator->fails()) {
            $user = new User;
            $check = $user->AddMaster($request, 2);

            if($check === "OK"){
                return Redirect::to('admin/user');
            }else if($check === "ID_ERROR"){
                return Redirect::back()->withErrors(["fail"=>"請確認身分證是否正確"])->withInput();
            }
        }

        return Redirect::back()
            ->withErrors($validator )->withInput();
    }
    
    
    public function show($id){
        $evnets = Event::all();
        $user = User::find($id);
        $event_list = Event_list::with("event")->where("users_id",$id)->orderBy("events_id")->get();
        return View('back.user.edit',compact("user","evnets","event_list"));
    }


    public function update(Request $request,$id){
        $rules = array('tw_name'=>'required', 'birth'=>'required');
        $validator = Validator::make($request->all(),$rules);
        if(!$validator->fails()) {

            $user = new User();
            $check = $user->AdminEditUser($request,$id);
            if($check === "OK"){
                return Redirect::to('admin/user');
            }else if($check === "ID_ERROR"){
                return Redirect::back()->withErrors(["fail"=>"請確認身分證是否正確。"])->withInput();
            }else if($check === "NOT_FOUND"){
                return Redirect::back()->withErrors(["fail"=>"找不到此會員。"])->withInput();
            }
        }
        return Redirect::back()
            ->withErrors($validator )->withInput();
    }

}
