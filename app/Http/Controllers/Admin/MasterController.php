<?php

namespace App\Http\Controllers\Admin;

use App\Dog;
use App\Event;
use App\Event_list;
use App\Events\FileUpload;
use App\Events\SomeEvent;
use App\Http\YvtAction;
use App\News;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;



class MasterController extends Controller
{

    public function search(){

    }
//治療犬主人
    public function checkPay($id){
        $user = User::find($id);
        $user->payTime = date("Y-m-d H:i:s");
        $user->save();
        return redirect::back();
    }

    public function index(){
        if(Input::has('twID'))
        {
            $twID = Input::get('twID');
            $users = User::whereRaw("`twID` LIKE '%".$twID."%' OR `tw_name` LIKE '%".$twID."%'")->paginate(10);
        }
        else
        {
            $users = User::paginate(10);
        }
        return View::make('back.master.index',compact("users"));

    }

    public function create(){
        $actionUrl = 'admin/master';
        return View('back.master.create', compact('actionUrl'));
    }
    public function store(Request $request){
        $rules = array('tw_name'=>'required','twID'=>'required|unique:users','birth'=>'required');
        $validator = Validator::make($request->all(),$rules);

        if(!$validator->fails()) {
            $user = new User;
            $check = $user->AddMaster($request);

            if($check === "OK"){
                return Redirect::to('admin/master');
            }else if($check === "ID_ERROR"){
                return Redirect::back()->withErrors(["fail"=>"請確認身分證是否正確"])->withInput();
            }
        }

        return Redirect::back()
            ->withErrors($validator )->withInput();
    }


    public function show($id){
        $user = User::find($id);
        $dogs = $user->Dog;
        foreach($dogs as $dog){
            $total = 0;
            foreach($dog->event_list as $dog_event){
                if(!$dog_event->event)
                    continue;
                $total += $dog_event->score;
            }
            $dog->event_total = $total;
        }
        $events = Event::all();
        $event_list = $user->Event_list()->orderBy('created_at','DESC')->orderBy("events_id")->get();
//        $event_list = Event_list::with("event")->where("users_id",$id)->orderBy("events_id")->get();
        return View('back.master.edit',compact('user', 'event_list', 'dogs', 'events'));
    }


    public function update(Request $request,$id){
        $rules = array('tw_name'=>'required', 'birth'=>'required');
        $validator = Validator::make($request->all(),$rules);
        if(!$validator->fails()) {

            $user = new User();
            $check = $user->AdminEditMaster($request,$id);
//            echo $check;
            if($check === "OK"){
                return Redirect::back();
            }else if($check === "ID_ERROR"){
                return Redirect::back()->withErrors(["fail"=>"請確認身分證是否正確。"])->withInput();
            }else if($check === "NOT_FOUND"){
                return Redirect::back()->withErrors(["fail"=>"找不到此會員。"])->withInput();
            }
        }
        return Redirect::back()
            ->withErrors($validator )->withInput();
    }
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect("admin/master");
    }
}
