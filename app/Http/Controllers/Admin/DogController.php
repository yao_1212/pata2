<?php

namespace App\Http\Controllers\Admin;

use App\Dog;
use App\Event;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use Mockery\CountValidator\Exception;

class DogController extends Controller
{
    public function store(Request $requests, $id){
        $data = $requests->all();

        $user = User::find($id);
        if(!isset($user)){
            return view('errors.400');
        }
        //file upload
        $filename = NULL;
        if ($requests->hasFile('img')) {
            $img = Image::make($requests->file('img'));
            $img->fit(300, 200);
            $extension = $requests->img->extension();
            $filename = str_random(10).'.'.$extension;
            $img->save('upload/dog/'.$filename);
        }
        //create a Dog to User
        $dog = $user->Dog()->create([
            'name'=>$data['name'],
            'type'=>$data['type'],
            'nickname' => $data['nickname'],
            'sex'=>$data['sex'],
            'birth'=>($data['birth']=='')?NULL:$data['birth'],
            'number'=>$data['number'],
            'first_ok'=>($data['first_ok']=='')?NULL:$data['first_ok'],
            'start_date'=>($data['start_date']=='')?NULL:$data['start_date'],
            'end_date'=>($data['end_date']=='')?NULL:$data['end_date'],
            'img'=>$filename,
        ]);
        //1 動輔師， 2 治療犬主人
        $path = '/admin/master/' . $id;
        return redirect($path);
    }
    public function create($id){
        $user = User::find($id);
        if(!isset($user)){
            return view('errors.404');
        }
        $actionUrl = 'admin/master/'.$id.'/dogs/create';
        return View::make('back.master.dogs.create',compact('actionUrl'));
    }

    public function update(Request $requests, $user_id, $id){
        $data = $requests->all();

        $dog = Dog::find($id);
        $dog->name = $data['name'];
        $dog->nickname = $data['nickname'];
        $dog->type = $data['type'];
        $dog->sex  = $data['sex'];
        $dog->birth = $data['birth'];
        $dog->number = $data['number'];
        $dog->first_ok = $data['first_ok'];
        $dog->start_date = $data['start_date'];
        $dog->end_date = $data['end_date'];
        $user = User::find($user_id);
        if(!isset($user)){
            return view('errors.404');
        }
        //file upload
        if ($requests->hasFile('img')) {
            $img = Image::make($requests->file('img'));
            $img->fit(300, 200);
            $extension = $requests->img->extension();
            $filename = str_random(10).'.'.$extension;
            $img->save('upload/dog/'.$filename);
            $dog->img = $filename;
        }
        $dog->save();
        return Redirect::back();
    }

    /**
     * show dog and edit that.
     * @param $user_id
     * @param $id
     * @return
     */
    public function show($user_id, $id){
        $dog = Dog::find($id);
        $event_list = $dog->Event_list()->orderBy('created_at','DESC')->orderBy("events_id")->get();
        $events = Event::all();

        return view('back.master.dogs.edit',compact('dog', 'event_list', 'events'));
    }


    public function destroy($user_id, $dog_id){
        $user = User::find($user_id);
        if(!isset($user)){
            return view('errors.400');
        }

        if(!Dog::destroy($dog_id)){
            return view('errors.400');
        }

        $path = '/admin/master/' . $user_id;
        return redirect($path);
    }

}
