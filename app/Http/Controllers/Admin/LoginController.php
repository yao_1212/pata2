<?php

namespace App\Http\Controllers\Admin;

use App\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Validator;

class LoginController extends Controller
{

    protected $redirectTo = "admin/yvtset/news";
    protected $guard = 'admin';

    public function show(){
        return view("back.auth.login");
    }

    public function logout(){
        Auth::guard('admin')->logout();
        return redirect($this->redirectTo);

    }
    public function login(Request $request){

        $rules = array('account'=>'required','password'=>'required');
        $validator = Validator::make($request->all(),$rules);

        if(!$validator->fails()) {
            $attempt = Auth::guard('admin')->attempt([
                'account' => $request->account,
                'password' => $request->password
            ]);
            
            if ($attempt) {
                return redirect($this->redirectTo);
            }
        }
        return redirect('admin/login');

    }




}
