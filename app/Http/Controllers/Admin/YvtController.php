<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\DogAlbum;
use App\Http\YvtAction;
use Illuminate\Support\Facades\Input;

class YvtController extends Controller
{
    public function copy($yvtset, $id)
    {
        $yvtset = $this->getYvtset($yvtset);
        $yvtset_class = new $yvtset;
        $yvtset = $yvtset_class->find($id);
        
        $newYvtSet = $yvtset->replicate();
        $newYvtSet->title = '[複製]'.$newYvtSet->title;
        $newYvtSet->save();
        return redirect("admin/yvtset/" . $yvtset->getTable() . "/" . $newYvtSet->id);
    }

    public function getYvtset($yvtset)
    {
        return "App\\" . str_replace('_', '', ucwords($yvtset, '_'));
    }
    //
    public function index($yvtset)
    {
        $yvtset = $this->getYvtset($yvtset);
        $yvtset = new $yvtset;
        $yvtsets = $yvtset::orderBy("created_at", "DESC")->paginate(10);
        return view("back.news.index", compact("yvtset", "yvtsets"));
    }

    public function show($yvtset, $id)
    {
        $yvtset = $this->getYvtset($yvtset);
        $yvtset_class = new $yvtset;
        $yvtset = $yvtset_class->find($id);

        $columns = $yvtset->columns;

        $rel_table_array = array();
        if (isset($yvtset_class->rel_table)) {
            $rel_tables = $yvtset_class->rel_table;
            foreach ($rel_tables as $rel_key => $rel_table) {
                //比較基本的一對多關聯資料
                $rel_yvtset_str = "App\\" . ucfirst($rel_key);
                $rel_yvtset_class = new $rel_yvtset_str;
                $rel_yvtset_array[$rel_key]["table"] = $rel_key;
                $rel_yvtset_array[$rel_key]["table_val"] = $rel_table;
                $rel_yvtset_array[$rel_key]["table_columns"] = $rel_yvtset_class->columns;
//            $rel_yvtset_array[$rel_key]["data"] = $rel_yvtset_class->where($rel_table["idx"],$id)->get();
                $rel_yvtset_array[$rel_key]["data"] = $yvtset_class->find($id)->$rel_key;

                if ($rel_table["reledittype"] == "orders") {
                    $order = $rel_yvtset_class->getOrderColumns();
                    $rel_yvtset_array[$rel_key]["data"] = $rel_yvtset_class->where($rel_table["idx"], $id)->orderBy($order)->get();
                }


                $rel_yvtset_array[$rel_key]["p_key"] = $rel_yvtset_class->getKeyName();
            }
        }

//        $rel_yvtset["table"] = $rel_tables;
        return view("back.news.edit", compact("yvtset", "columns", "id", "rel_yvtset_array"));
    }

    public function create($yvtset)
    {
        $yvtset = $this->getYvtset($yvtset);
        $yvtset = new $yvtset;

        $columns = $yvtset->columns;
        return view("back.news.create", compact("yvtset", "columns"));
    }

    public function store(Request $requests, $yvtset)
    {
        $yvtset = $this->getYvtset($yvtset);
        $yvtset = new $yvtset;
        $columns = $yvtset->columns;
        //傳入data 跟 yvtset(model)
        $yvtAction = new YvtAction($requests, $yvtset);

        $this->storeData($columns, $yvtAction);

        //debug
        $yvtset = $yvtAction->getYvtset();
        $yvtset->save();
        return redirect("admin/yvtset/" . $yvtset->getTable() . "/" . $yvtset->id);
    }

    public function update(Request $requests, $yvtset, $id)
    {
        $yvtset = $this->getYvtset($yvtset);
        $yvtset_class = new $yvtset;
        $columns = $yvtset_class->columns;

        $yvtset = $yvtset_class::find($id);
        //傳入data 跟 yvtset(model)
        $yvtAction = new YvtAction($requests, $yvtset);

        $this->storeData($columns, $yvtAction);

        //debug
//        $yvtset = $yvtAction->getYvtset();
        $rel_tables = $yvtset_class->rel_table;


        $yvtset->save();

        return redirect("admin/yvtset/" . $yvtset->getTable() . "/" . $yvtset->id);
    }

    //新的模組，都要在這邊做判斷（儲存資料）
    public function storeData($columns, $yvtAction)
    {
        foreach ($columns as $key => $column) {
            switch ($column["edittype"]) {
                case "uuid":
                    $yvtAction->setUuid($key);
                    break;
                case "input":
                case "number":
                case "date":
                case "textarea":
                case "select":
                case "ckeditor":
                case "foreignKey":
                    $yvtAction->setInputType($key);
                    //這邊要創一個class -> "Form/InputType"
                    break;
                case "file":
                    $yvtAction->setFileType($column, $key);
//                    event(new FileUpload($requests->$key));
                    break;
                case "url_photo":
                    $yvtAction->setImageUpload($column, $key);
                    break;
            }
        }
    }

    //需要討論關聯刪除的部分如何實作
    public function delete($yvtset, $id)
    {
        $yvtset = $this->getYvtset($yvtset);
        $yvtset = new $yvtset;
        $yvtset = $yvtset->find($id);
        $yvtset->delete();
        return redirect("admin/yvtset/" . $yvtset->getTable());
    }


    public function getColumn($yvtset, $id = null)
    {
        $yvtset = $this->getYvtset($yvtset);
        $yvtset = new $yvtset;
        $columns = $yvtset->columns;


        return view("back.component.yvtuikit.form", compact("yvtset", "columns"));
    }

    public function getIdColumn($yvtset, $id = null)
    {
        $yvtset = $this->getYvtset($yvtset);

        $yvtset = new $yvtset;
        $columns = $yvtset->columns;
        $yvtset = $yvtset::find($id);
        $keyName = $yvtset->getKeyName();
        $p_key = $yvtset->$keyName;
        return view("back.component.yvtuikit.form", compact("yvtset", "columns", "p_key"));
    }

    public function relStore(Request $requests, $relyvtset)
    {
        $input = Input::all();

        $yvtset = "App\\" . ucfirst($relyvtset);
        $yvtset = new $yvtset;


        if (isset($input["p_key"])) {
            $yvtset = $yvtset->find($input["p_key"]);
        }

        $columns = $yvtset->columns;
        //傳入data 跟 yvtset(model)
        $yvtAction = new YvtAction($requests, $yvtset);

        $this->storeData($columns, $yvtAction);

        //debug
        $yvtset = $yvtAction->getYvtset();

        $order = $yvtset->getOrderColumns();

        $f_key = $yvtset->getOrderForeignKeyName();
        $countRel = $yvtset->where($f_key, $requests->$f_key)->count();

        $yvtset->$order = $countRel + 1;

        $yvtset->save();

        //目前寫好可以新增了～簡單一對多關聯
        //要寫上傳的code，寫在yvtAction
        return $yvtset;
    }

    public function updateOrder(Request $requests, $relyvtset)
    {
        $input = Input::all();
        $yvtset = "App\\" . ucfirst($relyvtset);
        $yvtset = new $yvtset;

        foreach ($requests->p_key as $num => $id) {
            $yvtset = $yvtset->find($id);
            $order = $yvtset->getOrderColumns();
            $yvtset->$order = $num + 1;
            $yvtset->save();
        }
    }
}
