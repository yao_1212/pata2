<?php

namespace App\Http\Controllers\Admin;

use App\DogAlbum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use Mockery\CountValidator\Exception;

class DogAlbumController extends Controller
{
    public function store(Request $requests){
        $data = $requests->all();

        //file upload
        $filename = NULL;
        if ($requests->hasFile('img')) {
            $img = Image::make($requests->file('img'));
            $img->fit(360, 250);
            $extension = $requests->img->extension();
            $filename = str_random(10).'.'.$extension;
            $img->save('upload/dog_album/'.$filename);
        }
        //create a Dog to User
        $dog = DogAlbum::create([
            'name'=>$data['name'],
            'type'=>$data['type'],
            'sex'=>$data['sex'],
            'birth'=>($data['birth']=='')?NULL:$data['birth'],
            'personality' => $data['personality'],
            'like'=>$data['like'],
            'treat'=>$data['treat'],
            'img'=>$filename,
        ]);

        return redirect('/admin/dogAlbum');
    }

    public function index(){
        $dogs = DogAlbum::paginate(10);
        return View::make('back.dog_album.index', compact('dogs'));

    }

    public function create(){
        return View::make('back.dog_album.create');
    }

    public function update(Request $requests, $id){
        $data = $requests->all();

        $dog = DogAlbum::find($id);
        $dog->name = $data['name'];
        $dog->type = $data['type'];
        $dog->sex  = $data['sex'];
        $dog->birth = $data['birth'];
        $dog->personality  =  $data['personality'];
        $dog->like  = $data['like'];
        $dog->treat = $data['treat'];
        
        //file upload
        if ($requests->hasFile('img')) {
            $img = Image::make($requests->file('img'));
            $img->fit(360, 250);
            $extension = $requests->img->extension();
            $filename = str_random(10).'.'.$extension;
            $img->save('upload/dog_album/'.$filename);
            $dog->img = $filename;
        }
        $dog->save();
        return Redirect::back();
    }

    /**
     * show dog and edit that.
     * @param $user_id
     * @param $id
     * @return
     */
    public function show($id){
        $dog = DogAlbum::find($id);
        return view('back.dog_album.edit',compact('dog'));
    }


    public function destroy($id){
        if(!DogAlbum::destroy($id)){
            return view('errors.400');
        }
        $path = '/admin/dogAlbum';
        return redirect($path);
    }

}
