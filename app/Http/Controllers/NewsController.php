<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\View;

class NewsController extends Controller
{
    public function index(Request $request,$type=null){
        $meta = "news";
        $news_arr = array("1"=>"最新消息",
            "2"=>"學術研討會",
            "3"=>"好康報報",
            "4"=>"徵才訊息");
        if(isset($type) && array_key_exists($type,$news_arr)){
            $news = News::where("type",$news_arr[$type])->orderBy("created_at","DESC")->paginate(10);
        }else{
            $news = News::orderBy("created_at","DESC")->paginate(10);
        }
        return view('front.news.index',compact("meta","news"));
    }

    public function view($id){
        $meta = "news";
        $news = News::find($id);
        return view('front.news.view',compact("meta","news"));

    }
}
