<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddUserRequest;
use App\Http\Requests\EditUserRequest;
use App\User;
use Illuminate\Config\Repository;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;
use Validator;


class MemberController extends Controller
{

    public function ResetPasswordPage(){
        return view("front.member.resetPassword");
    }

    public function ResetPassword(Request $request){
        $input = Input::all();
        
        $code = $this->check(Input::get("text_input"));
        if(!$code){
            return Redirect::to('user/reset/password')->withErrors(['fail' => "<b class='error-column'>驗證碼</b> 不正確"] )->withInput();
        }

        $user = User::where('email', $input['email'])->first();
        if($user){
            $from = ['email'  =>"pata.org.tw@gmail.com",
                'name'   =>"台灣動物輔助治療專業發展協會",
                'subject'=>"【台灣動物輔助治療專業發展協會】忘記密碼"
            ];
            $to = ['email' => $user->email,
                'name' => $user->tw_name];
            $data = [
                'name'   => $user->tw_name,
                'subject'=> "【台灣動物輔助治療專業發展協會】忘記密碼",
                'user'   => $user->twID,
            ];
            $this->ForgetPasswordmail($from,$to,$data);
            $user->password = bcrypt($user->twID);
            $user->save();
            return Redirect::to('login');
        }
        return Redirect::to('user/reset/password')->withErrors(['fail' => "<b class='error-column'>Email</b> 不正確"] )->withInput();

    }
    public function ForgetPasswordmail($from,$to,$data){
        Mail::send('front.member.mail.forget', $data, function($message) use ($from, $to) {
            $message->from($from['email'], $from['name']);
            $message->to($to['email'], $to['name'])->subject($from['subject']);
        });
    }



    public function DogPage($id){
        $dog = Auth::user()->dog->find($id);
        if(!$dog){
            return response('NOT_FOUND',404);
        }

        return view('front.member.dogs_list', compact('dog'));
    }

    public function EventPage(){
        $score = 0;
        $user = Auth::user();

        foreach($user->dog as $dog){
            $dog->score = 0;
            foreach($dog->getEventList as $item){
                $dog->score += intval($item->pivot->score);
            }
        }
        if($user){
            $user->score = 0;
            foreach($user->frontGetEventList as $item){
                $user->score += intval($item->pivot->score);
            }
        }
        return view('front.member.event_index', compact('user'));
    }


    public function EditPasswordPage(){
        $meta = "user";
        $user = Auth::user();
        return view("front.member.password",compact("user","meta"));
    }


    /**
     * 修改密碼
     */
    public function EditPassword(Request $request){
        $rules = array(
            'password' => 'required|min:3|confirmed',
            'password_confirmation' => 'required|min:3'
        );

        // Create a new validator instance.
        $validator = Validator::make($request->all(), $rules);


        if ($validator->passes()) {
            $user = Auth::user();
            $user->password = bcrypt($request->password);
            $user->save();
            return Redirect::to("");
        }
        return Redirect::back()->withErrors($validator);

    }

    /**
     * 編輯頁面
     * @return mixed
     */
    public function EditPage(){
        $meta = "user";
        $user = Auth::user();
        return view("front.member.edit",compact("user","meta"));
    }

    /**
     * @param EditUserRequest $request
     * @return string
     */
    public function EditMember(EditUserRequest $request){
        $user = Auth::user();
        if(!$user){
            return "NOT_FOUND";
        }
        $user->tw_name = $request->tw_name;
        $user->en_name = $request->en_name;
        $user->gender = $request->gender;
        $user->addr1 = $request->addr1;
        $user->phone1 = $request->phone1;
        $user->mphone = $request->mphone;
        $user->email = $request->email;
        $user->degree = $request->degree;
        $user->school = $request->school;
        $user->office = $request->office;
        $user->ophone = $request->ophone;
        $user->fax = $request->fax;

        $user->save();
        return Redirect::back();
    }


    public function SignUp(AddUserRequest $request){
        $user = new User;
        $good = $user->AddUser($request);
        if($good){
            $from = ['email'  =>"pata.org.tw@gmail.com",
                'name'   =>"台灣動物輔助治療專業發展協會",
                'subject'=>"【台灣動物輔助治療專業發展協會】歡迎您加入!!"
            ];
            $to = ['email'=>$request->email,
                'name' =>$request->tw_name];
            $data = [
                'name'   => $request->tw_name,
                'subject'=> "【台灣動物輔助治療專業發展協會】歡迎您加入!!",
                'user'   => $request->twID,
            ];
            $this->postnewmail($from,$to,$data);

            return Redirect::to('user/sign_up/welcome');
        }
        return Redirect::to('user/sign_up')
            ->withErrors(['fail' => "<b class='error-column'>身分證字號</b> 格式錯誤"] )->withInput();
    }
    public function postnewmail($from,$to,$data){
        Mail::send('front.member.mail.welcome', $data, function($message) use ($from, $to) {
            $message->from($from['email'], $from['name']);
            $message->to($to['email'], $to['name'])->subject($from['subject']);
        });
    }

    public function welcome(){
        return view("front.member.welcome");
    }


    public function SignUpPage(){
        return view("front.member.signup");
    }

    public function LoginPage(){
        if(Auth::check()){
            return redirect("");
        }
        return view("front.member.login");
    }
    public function Login(){

        $input = Input::all();
        $code = $this->check(Input::get("text_input"));
        if(!$code){
            return Redirect::to('login')->withErrors(['fail' => "<b class='error-column'>驗證碼</b> 不正確"] )->withInput();
        }
        $arr = [
            'twID' => $input['user'],
            'password' => $input['ps'],
        ];
        if(Auth::attempt($arr)){
           return Redirect::to("");
        }

        return Redirect::to('login')->withErrors(['fail' => "<b class='error-column'>帳號</b> 或 <b class='error-column'>密碼</b> 不正確"] )->withInput();

//        return $validator;
//        return Input::get("text_input");
    }
    public function Logout(){
        Auth::logout();
        return redirect("");
    }







    //驗證碼 start
    protected $storeKey = 'captchaHash';

    public function check($string, $formId = null)
    {
        if (empty($formId)) {
            //$formId = $this->fromCurrentUrl();
            $formId = $this->fromAppKey();
        }
        $hash = Session::get("{$this->storeKey}.{$formId}");


        return $this->hash(
            Config::get('captcha.case_sensitive') ? $string : strtolower($string)
        ) == $hash;
    }
    private function fromAppKey()
    {
        return $this->hash(Config::get("app.key"));
    }
    private function hash($string)
    {
        return hash('sha256', $string);
    }
    //驗證碼 end


}
