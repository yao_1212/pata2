<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as BaseEncrypter;
use Illuminate\Support\Facades\Auth;
use Closure;

class AdminCheck
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    public function handle($request,Closure $next,$guard = null)
    {
        if(!Auth::guard("admin")->check())
        {
            return redirect('admin/login');
        }
        return $next($request);
    }
}
