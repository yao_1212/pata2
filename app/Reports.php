<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reports extends Model
{
    protected $table = 'reports';
    public $labelname = "媒體報導";
    public $delete = true;

    public function getDateTime()
    {
        $d = strtotime($this->date);
        $this->date = date("Y-m-d", $d);
        return date("Y-m-d", $d);
    }

    protected $fillable = [
        'title',
        'link',
        'date'
    ];
    public $columns = array(
        "id"	=> array( "label" => "#", "listshow" => true, "edittype" => "disabled" ),
        "title"  => array("label"=>"服務單位", "listshow" => true, "edittype" => "input", "placeholder" => "請輸入標題", "required"=> true),
        "date"  => array("label"=>"報導時間", "listshow" => true, "edittype" => "date", "placeholder" => "請輸入報導時間", "required"=> true),
        "link"  => array("label"=>"連結", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入連結", "required"=> true),
    );
}
