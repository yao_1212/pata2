<?php
//新的模組，都要在 YvtController 裡面做新的判斷

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    public $labelname = "最新消息";
    public $primaryKey = "id";

    public function getDateTime(){
        $d = strtotime($this->created_at );
        $this->created_at = date("Y-m-d",$d);
        return date("Y-m-d",$d);
    }

    protected $fillable = [
        'title',
        'type',
        'content',
        'published_at',
        'filename'
    ];
    public $columns = array(
        "id"	=> array( "label" => "#", "listshow" => true, "edittype" => "disabled" ),
        "type"   => array("label"=>"類別", "listshow" => true, "edittype" => "select",
            "editarray"=>array(
                array("value" => "最新消息","text"=>"最新消息"),
                array("value" => "學術研討會","text"=>"學術研討會"),
                array("value" => "好康報報","text"=>"好康報報"),
                array("value" => "徵才訊息","text"=>"徵才訊息")
            )),
        "title"  => array("label"=>"標題", "listshow" => true, "edittype" => "input", "placeholder" => "請輸入標題", "required"=> true),

        "content"=> array("label"=>"內容","edittype" => "ckeditor","listshow" => false, "placeholder" => "請輸入內容"),
        "filepath"=> array("label"=>"上傳檔案","file_dir"=>"news_file","listshow" => false,"edittype" => "file","file_note"=>"請上傳2MB以下的檔案")
        //記得多開一個filename的欄位
    );





//    // 獨立寫一個class 讓每個model extend後都可以使用
//    // 這樣就不用一直複製了
//    // $edittype 編輯格式
//    // $name 欄位名稱
//    // $value 值
    public function createAndModify($column, $name, $value=""){


        if($column["edittype"] == "disabled"){
            return ;//忘了
        }

        $required = "";
        if(isset($column["required"])){
            $required = "required";
        }

        //要在上面給參數設定
        $class = "form-control ";

        //應該要把 tr td 也寫到"Form/InputType"，為了以後新的客製化
        echo "<tr>";
            echo "<td>".$column["label"]."</td>";
            echo "<td>";
            switch($column["edittype"]){
                case "input":
                    //這邊要創一個class -> "Form/InputType"
                    echo "<input type='text' name='$name' class='$class' value='$value' placeholder='".$column["placeholder"]."' ".$required."/> ";
                break;
                case "textarea":
                    echo "<textarea name='$name'  class='$class' placeholder='".$column["placeholder"]."' rows='5' ".$required.">$value</textarea>";
                break;
                case "select":
                    echo "<select name='$name'  class='$class'>";
                    foreach ($column["editarray"] as $option){
                        echo "<option value='".$option["value"]."'>".$option["text"]."</option>";
                    }
                    echo "</select>";
                break;
                case "file":
                    echo "<input type='file' name='$name' class='$class' value='$value' '/>";
                    echo " <span class='file-note'>".$column["file_note"]."</span> ";
//                    if($column[$name]){

//                    }
                break;
            }
            echo "<td>";
//        return $type;
        echo "</tr>";
    }










    //暫時用不到
    public function dataTable($column, $name, $value=""){
        echo "<thead>";
            echo "<tr>";

            echo "</tr>";
        echo "</thead>";
    }

}
