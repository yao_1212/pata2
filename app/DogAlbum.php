<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DogAlbum extends Model
{

    public $labelname = "治療犬圖片";
    public $primaryKey = "id";

    protected $fillable = [
                "name",
				"type",
				"sex",
				"birth",
				"personality",
				"like",
				"treat",
				"img"
    ];


    public $columns = array(
        "id"    => array( "label" => "#", "listshow" => true, "edittype" => "disabled" ),
        "type"   => array("label"=>"類別", "listshow" => true, "edittype" => "select",
            "editarray"=>array(
                array("value" => "最新消息","text"=>"最新消息"),
                array("value" => "學術研討會","text"=>"學術研討會"),
                array("value" => "好康報報","text"=>"好康報報"),
                array("value" => "徵才訊息","text"=>"徵才訊息")
            )),
        "title"  => array("label"=>"標題", "listshow" => true, "edittype" => "input", "placeholder" => "請輸入標題", "required"=> true),

        "content"=> array("label"=>"內容","edittype" => "ckeditor","listshow" => false, "placeholder" => "請輸入內容"),
        "filepath"=> array("label"=>"上傳檔案","file_dir"=>"news_file","listshow" => false,"edittype" => "file","file_note"=>"請上傳2MB以下的檔案")
        //記得多開一個filename的欄位
    );
    public function getDateTime(){
        $d = strtotime($this->created_at );
        $this->created_at = date("Y-m-d",$d);
        return date("Y-m-d",$d);
    }
    public function getBirth(){
        $d = strtotime($this->birth );
        $this->birth = date("Y-m-d",$d);
        return date("Y-m-d",$d);
    }

    public function getAge(){
        $d = strtotime($this->birth);
        return date('Y') - date("Y",$d);
    }
    
    public function getSex(){
        return ($this->sex == 1)? '公':'母';
    }
    public function saveDog($data, $id){
        $dog = new DogAlbum();
        $dog->user_id = $id;
        $dog->name = $data['name'];
        $dog->type = $data['type'];
        $dog->sex  = $data['sex'];
        $dog->birth = $data['birth'];
        $dog->personality = $data['personality'];
        $dog->like = $data['like'];
        $dog->treat = $data['treat'];
        return $dog;
    }
}
