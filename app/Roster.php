<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roster extends Model
{
    protected $table = 'roster';
    protected $primaryKey = 'ros_id';
    public $labelname = "理監事名單";

    protected $fillable = [
        'title',
        'order',
        'published_at'
    ];
    public $columns = array(
        "ros_id"	=> array( "label" => "#", "listshow" => true, "edittype" => "disabled" ),
        "title"  => array("label"=>"名單標題", "listshow" => true, "edittype" => "input", "placeholder" => "請輸入名單標題", "required"=> true),
        "order"  => array("label"=>"排序", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "created_at"  => array("label"=>"新增時間", "listshow" => true, "edittype" => "disabled"),

    );

    public function Roster_master()
    {
        return $this->hasMany('App\Roster_master',"ros_id");
    }

    public $rel_table = array(
        "Roster_master"	=>	array("label"=>'理事長名單', "idx"=>"ros_id","reledittype"=>"orders","edittype"=>"url_photo",'photo_type'=>'jpg' ),
    );
}
