<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    public $labelname = "服務實績";
    public $delete = true;

    public function getDateTime()
    {
        $d = strtotime($this->created_at);
        $this->created_at = date("Y-m-d", $d);
        return date("Y-m-d", $d);
    }

    protected $fillable = [
        'title',
    ];
    public $columns = array(
        "id"	=> array( "label" => "#", "listshow" => true, "edittype" => "disabled" ),
        "title"  => array("label"=>"服務單位", "listshow" => true, "edittype" => "input", "placeholder" => "請輸入標題", "required"=> true),
        "date"=> array("label"=>"服務日期","edittype" => "date","listshow" => true, "placeholder" => "請輸入時間", "required"=> true),
    );
}
