<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class User extends Authenticatable
{
    //1 動輔師， 2 治療犬主人
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        "tw_name",
        "en_name",
        "twID",
        "birth",
        "type",
        "gender",
        "addr1",
        "phone1",
        "mphone",
        "email",
        "degree",
        "school",
        "office",
        "ophone",
        "fax",
        "password",
        "type"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Dog(){
        return $this->hasMany('App\Dog');
    }
    public function Event_list()
    {
        return $this->hasMany('App\Event_list','users_id','user_id');
    }
    public function frontGetEventList(){
        return $this->belongsToMany('App\Event', 'event_list', 'users_id', 'events_id')
            ->withPivot('score')
            ->select(['event.id', 'title', 'type', 'comedate', 'phone']);
    }
    protected $table = 'users';
    protected $primaryKey = 'user_id';
    public $labelname = "會員管理";

    public $columns = array(
        "id"	   => array( "label" => "#", "listshow" => true, "edittype" => "disabled" ),
        "tw_name"  => array("label"=>"中文姓名", "listshow" => true, "edittype" => "input", "placeholder" => "請輸入名單標題", "required"=> true),
        "en_name"  => array("label"=>"英文姓名", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
//        "type"  => array("label"=>"新增時間", "listshow" => true, "edittype" => "disabled"),
//        "payTime"  => array("label"=>"付款時間", "listshow" => true, "edittype" => "disabled"),
        "gender"  => array("label"=>"性別", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "birth"  => array("label"=>"生日", "listshow" => false, "edittype" => "date", "placeholder" => "請輸入排序", "required"=> true),
        "twID"  => array("label"=>"身分證字號", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "email"  => array("label"=>"信箱", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "degree"  => array("label"=>"學歷", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "school"  => array("label"=>"畢業學校", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "office"  => array("label"=>"服務單位", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "ophone"  => array("label"=>"單位電話", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "fax"  => array("label"=>"傳真", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "addr1"  => array("label"=>"永久地址", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "home"  => array("label"=>"居住地址", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),
        "mphone"  => array("label"=>"行動電話", "listshow" => false, "edittype" => "input", "placeholder" => "請輸入排序", "required"=> true),

    );

    public function AddMaster($request){
        $checkId = $this->checkTwID($request->twID);
        if(!$checkId){
            return "ID_ERROR";//失敗
        }


//        echo $request->tw_name;
        User::create([
            "tw_name"=>$request->tw_name,
            "en_name"=>$request->en_name,
            "twID"  =>$request->twID,
            "birth" =>$request->birth,
            "gender"=>$request->gender,
            "addr1" =>$request->addr1,
            "phone1"=>$request->phone1,
            "mphone"=>$request->mphone,
            "email" =>$request->email,
            "degree"=>$request->degree,
            "school"=>$request->school,
            "office"=>$request->office,
            "ophone"=>$request->ophone,
            "payTime"=>"0000-00-00 00:00:00",
            "fax"   =>$request->fax,
            "password"=>bcrypt($request->twID),
        ]);
        return "OK";
    }
    public function FrontGetIsPay(){
        $expire = date("Y");
        $time = date("Y",strtotime($this->payTime));
        $this->valid = $expire - $time ;

        if($this->payTime == "0000-00-00 00:00:00" || $this->payTime ==NULL)
        {
            return 0;//未繳費
        }
        else
        {
            if($this->valid  > 0)
            {
                return 1;//過期
            }
            else if($this->valid == 0)
            {
                return 2;//已繳費
            }
        }
    }

    public function getIsPay(){
        $expire = date("Y");
        $time = date("Y",strtotime($this->payTime));
        $this->valid = $expire - $time ;

        if($this->payTime == "0000-00-00 00:00:00" || $this->payTime ==NULL)
        {
            return '<span class="label label-warning">未繳費</span>';
        }
        else
        {
            if($this->valid  > 0)
            {
                return '<span class="label label-danger">過期</span>';
            }
            else if($this->valid == 0)
            {
                return '<span class="label label-success">已繳費</span>';
            }
        }
    }
    public function getType(){
        //1 動輔師， 2 治療犬主人
        $type_arr = ["1"=>"動輔師","2"=>"治療犬主人"];
        $type = $this->type;
        if(!array_key_exists($type,$type_arr)){
            $type = 1;
        }
        return $type_arr[$type];
    }
    public function AdminEditMaster($request, $id){
        $user = User::find($id);

        if(!$user){
            return "NOT_FOUND";
        }
        $user->tw_name = $request->tw_name;
        $user->en_name = $request->en_name;
        // $user->nickname z= $request->nickname;
        $user->gender = $request->gender;
        $user->addr1 = $request->addr1;
        $user->phone1 = $request->phone1;
        $user->mphone = $request->mphone;
        $user->email = $request->email;
        $user->degree = $request->degree;
        $user->school = $request->school;
        $user->office = $request->office;
        $user->ophone = $request->ophone;
        $user->fax = $request->fax;
        // $user->type= $request->type;

        $user->save();
        return "OK";
    }
    
    public function checkTwID($string)
    {
        $id = $string;

        $headPoint = array(
            'A'=>1,'I'=>39,'O'=>48,'B'=>10,'C'=>19,'D'=>28,
            'E'=>37,'F'=>46,'G'=>55,'H'=>64,'J'=>73,'K'=>82,
            'L'=>2,'M'=>11,'N'=>20,'P'=>29,'Q'=>38,'R'=>47,
            'S'=>56,'T'=>65,'U'=>74,'V'=>83,'W'=>21,'X'=>3,
            'Y'=>12,'Z'=>30
        );
        $multiply = array(8,7,6,5,4,3,2,1);
        if (preg_match("/^[A-Z][1-2][0-9]+$/",$id) AND strlen($id) == 10){
            $stringArray = str_split($id);
            $total = $headPoint[array_shift($stringArray)];
            $point = array_pop($stringArray);
            $len = count($stringArray);
            for($j=0; $j<$len; $j++){
                $total += $stringArray[$j]*$multiply[$j];
            }
            $last = (($total%10) == 0 )? 0: (10 - ( $total % 10 ));
            if ($last != $point) {
                return false;
            } else {
                return true;
            }
        }  else {
            return false;
        }
    }
}
