<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album_photo extends Model
{
    protected $table = 'album_photo';
    protected $primaryKey = 'photo_id';
    public $labelname = "相片";



    protected $fillable = [
        'img',
        'published_at'
    ];

    public function getOrderColumns(){
        return "photo_order";
    }
    public function getOrderForeignKeyName(){
        return "album_id";
    }


    public $columns = array(
        "photo_id"	    => array( "label" => "#", "listshow" => true, "edittype" => "disabled" ),
        "album_id"	=> array( "label" => "#", "listshow" => true, "edittype" => "foreignKey" ),
//        "title"      => array("label"=>"介紹", "listshow" => true, "editTitle"=>true, "edittype" => "input", "placeholder" => "請輸入介紹", "required"=> false),
        "img"       => array("label"=>"特色照片","listshow"=>true,"edittype"=>"url_photo","listshowtype"=>"url_photo","file_note"=>"請上傳2MB以下的檔案"),
        "created_at"=> array("label"=>"新增時間", "listshow" => true, "edittype" => "disabled"),
    );

}
