<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dog extends Model
{
    protected $table = 'dogs';

    public $labelname = "治療犬介紹";
    public $primaryKey = "dog_id";
    
    protected $fillable = [
                'user_id',
                'name',
                'nickname',                
                'type',
                'sex',
                'birth',
                'number',
                'first_ok',
                'start_date',
                'end_date',
                'img',
    ];

    //前台用
    public function getEventList(){
        return $this->belongsToMany('App\Event', 'event_dog_lists', 'dog_id', 'events_id')
            ->withPivot('score')
            ->select(['title', 'type', 'comedate', 'phone']);
    }

    //後台用
    public function Event_list(){
        return $this->hasMany('App\Event_dog_list','dog_id','dog_id');
    }
    public function User(){
        return $this->belongsTo('App\User')->select(['user_id', 'tw_name', 'mphone']);
    }
    public function getDateTime(){
        $d = strtotime($this->created_at );
        $this->created_at = date("Y-m-d",$d);
        return date("Y-m-d",$d);
    }
    public function getBirth(){
        $d = strtotime($this->birth );
        $this->birth = date("Y-m-d",$d);
        return date("Y-m-d",$d);
    }
    public function getFirst_ok(){
        $d = strtotime($this->first_ok );
        $this->first_ok = date("Y-m-d",$d);
        return date("Y-m-d",$d);
    }
    public function getStart_date(){
        $d = strtotime($this->start_date );
        $this->start_date = date("Y-m-d",$d);
        return date("Y-m-d",$d);
    }
    public function getEnd_date(){
        $d = strtotime($this->end_date );
        $this->end_date = date("Y-m-d",$d);
        return date("Y-m-d",$d);
    }

    public function saveDog($data, $id){
        $dog = new Dog();
        $dog->user_id = $id;
        $dog->name = $data['name'];
        $dog->type = $data['type'];
        $dog->sex  = $data['sex'];
        $dog->birth = $data['birth'];
        $dog->number = $data['number'];
        $dog->first_ok = $data['first_ok'];
        $dog->start_date = $data['start_date'];
        $dog->end_date = $data['end_date'];
        return $dog;
    }

}
