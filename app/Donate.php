<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donate extends Model
{
    protected $table = 'donate';

    public $labelname = "捐款芳名錄";
    public $primaryKey = "id";

    public $incrementing = false;

    protected $fillable = [
        'id',
        'number',
        'name',
        'money',
        'donate_time'
    ];
    public function getHideName(){
        $Name = $this->name;
        $name_len = mb_strlen($Name);

        $newName = "";
        for($i=0;$i<$name_len;$i++){

            if($i == 0 || $i == $name_len-1){
                $newName .= mb_substr($Name, $i, 1, 'UTF-8');
            }else{
                $newName .= "＊";
            }
        }
        return $newName;
    }

    public $columns = array(
        "id"	=> array( "label" => "#", "listshow" => false, "edittype" => "uuid" ),
        "number"  => array("label"=>"收據編號", "listshow" => true, "edittype" => "input", "placeholder" => "請輸入收據編號", "required"=> true),
        "name"  => array("label"=>"捐款人姓名", "listshow" => true, "edittype" => "input", "placeholder" => "請輸入姓名", "required"=> true),
        "money"=> array("label"=>"捐款項目","edittype" => "input","listshow" => true, "placeholder" => "請輸入捐款項目", "required"=> true),
        "donate_time"=> array("label"=>"捐款時間","edittype" => "date","listshow" => true, "placeholder" => "請輸入時間", "required"=> true),
    );
}
