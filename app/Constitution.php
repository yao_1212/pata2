<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constitution extends Model
{
    protected $table = 'constitutions';

    protected $fillable = [
        'total'
    ];


    //只能用到十位數
    static public function getNum2speak($num = 11){
        $ar = array('', '一', '二', '三', '四', '五', '六', '七', '八', '九') ;
        $cName = array('','','十');
        $cUnit = $cName[strlen($num)];
        if(strlen($num) > 1){
            if(substr($num,0,1) != 1){
                $go = $ar[substr($num,0,1)] . $cUnit. $ar[substr($num,1,1)];
            }else{
                $go = $cUnit. $ar[substr($num,1,1)];
            }
        }else{
            $go = $ar[substr($num,0,1)];
        }
        return $go;
    }
}
